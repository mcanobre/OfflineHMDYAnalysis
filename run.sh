#!/bin/bash
echo $PWD
cd /afs/cern.ch/work/m/mcanobre/HMDY/OfflineHMDYAnalysis/
echo $PWD
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
export ALRB_rootVersion=6.20.06-x86_64-centos7-gcc8-opt
lsetup root
./HMDYRun2 $1 $2 $3 $4 $5 $6 $7 > Output_$1_$2_$3.txt

