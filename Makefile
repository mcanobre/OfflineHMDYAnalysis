LIBS    =`root-config --libs`
INCS    =`root-config --cflags`
CCOPTS  =-g $(LIBS) $(INCS)
F_FLAGS =-g -fno-globals -fno-automatic -finit-local-zero -fno-second-underscore -fno-backslash -fno-globals -ff90 -fpic
C_FLAGS = -O2 -Wall -fPIC $(INCS)

# this makefile takes into account ntuples with slightly differnt
# structures. The source code is the same - in DY2012.C but this
# compiles using a dummy header called MyHeader.h which is then copied
# from the correct ntuple header file by this makefile



RecoHMDY: MainDY.C Comparison.h Comparison.C
	g++ $(C_FLAGS) -c MainDY.C -o MainDY.o
	g++ $(C_FLAGS) -c Comparison.C -o Comparison.o
	g++ MainDY.o  Comparison.o -o HMDYRun2 $(CCOPTS) 	

.C.o:
	g++ $(C_FLAGS) -c $< -o $*.o


clean:
	rm -f *.o
	rm -f HMDYRun2 
