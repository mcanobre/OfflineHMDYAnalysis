////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Dec 31 11:35:58 2017 by ROOT version 5.34/36
// from TTree nominal/tree
// found on file: /eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Ztautau.root
//////////////////////////////////////////////////////////

#ifndef Comparison_h
#define Comparison_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

//#include <unordered_map>
#include <map>
#include <memory>

#include <TROOT.h>
#include <TChain.h>
#include <TRandom.h>
#include <TFile.h>
#include "TLorentzVector.h"
#include "TF1.h"
#include <TH1.h>
#include "TProfile.h"
#include <TGraph.h>
#include <fstream>

// Header file for the classes stored in the TTree if any.
// Header file for the classes stored in the TTree if any.
#include <math.h>

#include <string>
#include <iostream>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
using std::vector;
using namespace std;
using std::endl;

class Comparison {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   TTree  *hmdyTree;   //!
   //OutputTree Variables
   double MCChannelNumber; //!

   bool TruthDilRapidity_00_08_Born_ee;  //!
   bool TruthDilRapidity_08_16_Born_ee;  //!
   bool TruthDilRapidity_00_16_Born_ee;  //!
   bool TruthDilRapidity_16_24_Born_ee;  //!
   bool TruthDilRapidity_24_Born_ee;  //!

   bool TruthDilRapidity_00_08_Bare_ee;  //!
   bool TruthDilRapidity_08_16_Bare_ee;  //!
   bool TruthDilRapidity_00_16_Bare_ee;  //!
   bool TruthDilRapidity_16_24_Bare_ee;  //!
   bool TruthDilRapidity_24_Bare_ee;  //!

   bool TruthDilRapidity_00_08_Dressed_ee;  //!
   bool TruthDilRapidity_08_16_Dressed_ee;  //!
   bool TruthDilRapidity_00_16_Dressed_ee;  //!
   bool TruthDilRapidity_16_24_Dressed_ee;  //!
   bool TruthDilRapidity_24_Dressed_ee;  //!

   bool TruthDilRapidity_00_08_Born_mumu;  //!
   bool TruthDilRapidity_08_16_Born_mumu;  //!
   bool TruthDilRapidity_00_16_Born_mumu;  //!
   bool TruthDilRapidity_16_24_Born_mumu;  //!
   bool TruthDilRapidity_24_Born_mumu;  //!

   bool TruthDilRapidity_00_08_Bare_mumu;  //!
   bool TruthDilRapidity_08_16_Bare_mumu;  //!
   bool TruthDilRapidity_00_16_Bare_mumu;  //!
   bool TruthDilRapidity_16_24_Bare_mumu;  //!
   bool TruthDilRapidity_24_Bare_mumu;  //!

   bool TruthDilRapidity_00_08_Dressed_mumu;  //!
   bool TruthDilRapidity_08_16_Dressed_mumu;  //!
   bool TruthDilRapidity_00_16_Dressed_mumu;  //!
   bool TruthDilRapidity_16_24_Dressed_mumu;  //!
   bool TruthDilRapidity_24_Dressed_mumu;  //!

   bool RecoDilRapidity_00_08_ee;  //!
   bool RecoDilRapidity_08_16_ee;  //!
   bool RecoDilRapidity_16_24_ee;  //!
   bool RecoDilRapidity_00_16_ee;  //!
   bool RecoDilRapidity_24_ee;  //!

   bool RecoDilRapidity_00_08_Fwdee;  //!
   bool RecoDilRapidity_08_16_Fwdee;  //!
   bool RecoDilRapidity_16_24_Fwdee;  //!
   bool RecoDilRapidity_00_16_Fwdee;  //!
   bool RecoDilRapidity_24_Fwdee;  //!

   bool RecoDilRapidity_00_08_mumu;  //!
   bool RecoDilRapidity_08_16_mumu;  //!
   bool RecoDilRapidity_16_24_mumu;  //!
   bool RecoDilRapidity_00_16_mumu;  //!
   bool RecoDilRapidity_24_mumu;  //!

   bool eeChannel;  //!
   bool mumuChannel;  //!
   bool emuChannel;  //!
   bool FwdeeChannel;  //!
   bool RecoSelectionPassed;  //! 
   bool BornTruthSelectionPassed; //!
   bool BareTruthSelectionPassed; //!
   bool DressedTruthSelectionPassed; //!
   bool BornTruthSelectionPassed_CF; //!
   bool BareTruthSelectionPassed_CF; //!
   bool DressedTruthSelectionPassed_CF; //!
   bool pass_ee_TT; //!
   bool pass_ee_TL; //!
   bool pass_ee_LT; //!
   bool pass_ee_LL; //!
   vector<int>    ele_isLHMedium; //!
   vector<int>    ele_isLHLoose; //!
   vector<int>    ele_isFixedCutTightIso; //!
   int RunNumber; //!
   double Mu; //!
   double MuActual; //!

   double pTllReweight; //!

   double LeptonSFWeight; //!
   double PileupWeight; //!
   double ChargeMisIDSFWeight; //!

   double PhiRF;//!

   double Weight_TopQuarkError_PDF4LHC_Total_Up; //!                                                                                                                                                                 
   double Weight_TopQuarkError_PDF4LHC_Total_Down; //!                                                                                                                                                               

   double  Weight_TopQuarkError_ISR_Var3c_Up; //!                                                                                                                                                                     
   double  Weight_TopQuarkError_FSR_RenFact_Up; //!                                                                                                                                                                   
   double  Weight_TopQuarkError_ISR_Up;//!                                                                                                                                                                            
   double  Weight_TopQuarkError_HardScatter_Up; //!                                                                                                                                                                   
   double  Weight_TopQuarkError_FragHadModel_Up;//!                                                                                                                                                                   
   double  Weight_TopQuarkError_TopMass_Up; //!                                                                                                                                                                       
   double  Weight_TopQuarkError_ISR_RenFact_Up; //!                                                                                                                                                                   

   double  Weight_TopQuarkError_ISR_Var3c_Down; //!                                                                                                                                                                   
   double  Weight_TopQuarkError_FSR_RenFact_Down; //!                                                                                                                                                                 
   double  Weight_TopQuarkError_ISR_Down; //!                                                                                                                                                                         
   double  Weight_TopQuarkError_HardScatter_Down; //!                                                                                                                                                                 
   double  Weight_TopQuarkError_FragHadModel_Down; //!                                                                                                                                                                
   double  Weight_TopQuarkError_TopMass_Down; //!                                                                                                                                                               
   double  Weight_TopQuarkError_ISR_RenFact_Down; //!  

   double Weight_TopQuarkError_DibosonSubtraction_UP; //!
   double Weight_TopQuarkError_DibosonXsection_UP; //!
   double Weight_TopQuarkError_DibosonSubtraction_DOWN; //!
   double Weight_TopQuarkError_DibosonXsection_DOWN; //!

   double Weight_TopQuarkError_LepSF_EL_ChargeMisID_STAT_UP;//!
   double Weight_TopQuarkError_LepSF_EL_ChargeMisID_STAT_DOWN;//!
   double Weight_TopQuarkError_LepSF_EL_ChargeMisID_SYST_UP;//!
   double Weight_TopQuarkError_LepSF_EL_ChargeMisID_SYST_DOWN;//!

   double Weight_TopQuarkError_LepSF_MU_ID_STAT_UP;//!
   double Weight_TopQuarkError_LepSF_MU_ID_STAT_DOWN;//!
   double Weight_TopQuarkError_LepSF_MU_ID_STAT_LOWPT_UP;//!
   double Weight_TopQuarkError_LepSF_MU_ID_STAT_LOWPT_DOWN;//!
   double Weight_TopQuarkError_LepSF_MU_ID_SYST_LOWPT_UP;//!
   double Weight_TopQuarkError_LepSF_MU_ID_SYST_LOWPT_DOWN;//!
   double Weight_TopQuarkError_LepSF_MU_TTVA_STAT_UP;//!
   double Weight_TopQuarkError_LepSF_MU_TTVA_STAT_DOWN;//!
   double Weight_TopQuarkError_LepSF_MU_Trigger_STAT_UP;//!
   double Weight_TopQuarkError_LepSF_MU_Trigger_STAT_DOWN;//!
   double Weight_TopQuarkError_LepSF_MU_Isol_STAT_UP;//!
   double Weight_TopQuarkError_LepSF_MU_Isol_STAT_DOWN;//!

   double Weight_TopQuarkError_Stat_emu_Up; //!
   double Weight_TopQuarkError_Stat_emu_Down; //!
   double Weight_TopQuarkError_Stat_ee_Up; //!
   double Weight_TopQuarkError_Stat_ee_Down; //!
   double Weight_TopQuarkError_Stat_mumu_Up; //!
   double Weight_TopQuarkError_Stat_mumu_Down; //!
   double Weight_TopQuarkError_Theory_Up_ee; //!
   double Weight_TopQuarkError_Theory_Down_ee; //!
   double Weight_TopQuarkError_Theory_Up_mumu; //!
   double Weight_TopQuarkError_Theory_Down_mumu; //!
   double Weight_TopQuarkError_LeptonSys_Up; //!
   double Weight_TopQuarkError_LeptonSys_Down; //!

   vector<float> Weight_TopQuarkError_LepSF_EL_ID_Up; //!
   vector<float> Weight_TopQuarkError_LepSF_EL_ID_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_1_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_2_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_3_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_4_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_5_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_6_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_7_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_8_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_9_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_10_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_11_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_12_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_13_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_14_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_15_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_16_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_17_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_18_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_19_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_20_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_21_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_22_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_23_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_24_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_25_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_26_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_27_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_28_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_29_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_30_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_31_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_32_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_33_Up; //!
   double Weight_TopQuarkError_LepSF_EL_ID_34_Up; //!
   double Weight_TopQuarkError_MUID_Up; //!
   double Weight_TopQuarkError_MUMS_Up; //!
   double Weight_TopQuarkError_MUSCALE_Up; //!
   double Weight_TopQuarkError_MUSAGITTARES_Up; //!
   double Weight_TopQuarkError_MUSAGITTARHO_Up; //!
   double Weight_TopQuarkError_ELRES_MATERIALCALO_Up; //!
   double Weight_TopQuarkError_ELRES_MATERIALCRYO_Up; //!
   double Weight_TopQuarkError_ELRES_MATERIALGAP_Up; //!
   double Weight_TopQuarkError_ELRES_MATERIALIBL_Up; //!
   double Weight_TopQuarkError_ELRES_MATERIALPP0_Up; //!
   double Weight_TopQuarkError_ELRES_PILEUP_Up; //!
   double Weight_TopQuarkError_ELRES_SAMPLINGTERM_Up; //!
   double Weight_TopQuarkError_ELRES_ZSMEARING_Up; //!
   double Weight_TopQuarkError_ELSCALE_E4SCINTILLATOR_Up; //!
   double Weight_TopQuarkError_ELSCALE_G4_Up; //!
   double Weight_TopQuarkError_ELSCALE_L1GAIN_Up; //!
   double Weight_TopQuarkError_ELSCALE_L2GAIN_Up; //!
   double Weight_TopQuarkError_ELSCALE_LARCALIB_Up; //!
   double Weight_TopQuarkError_ELSCALE_LARELECALIB_Up; //!
   double Weight_TopQuarkError_ELSCALE_LARELECUNCONV_Up; //!
   double Weight_TopQuarkError_ELSCALE_LARUNCONVCALIB_Up; //!
   double Weight_TopQuarkError_ELSCALE_LARUNCONVCALIB_Down; //!
   double Weight_TopQuarkError_ELSCALE_MATCALO_Up; //!
   double Weight_TopQuarkError_ELSCALE_MATCRYO_Up; //!
   double Weight_TopQuarkError_ELSCALE_MATID_Up; //!
   double Weight_TopQuarkError_ELSCALE_MATPP0_Up; //!
   double Weight_TopQuarkError_ELSCALE_PEDESTAL_Up; //!
   double Weight_TopQuarkError_ELSCALE_PS_BARREL_Up; //!
   double Weight_TopQuarkError_ELSCALE_PS_Up; //!
   double Weight_TopQuarkError_ELSCALE_S12_Up; //!
   double Weight_TopQuarkError_ELSCALE_TOPOCLUSTER_THRES_Up; //!
   double Weight_TopQuarkError_ELSCALE_WTOTS1_THRES_Up; //!
   double Weight_TopQuarkError_ELSCALE_ZEESYST_Up; //!

   double Weight_TopQuarkError_LepSF_EL_ID_1_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_2_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_3_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_4_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_5_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_6_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_7_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_8_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_9_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_10_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_11_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_12_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_13_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_14_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_15_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_16_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_17_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_18_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_19_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_20_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_21_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_22_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_23_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_24_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_25_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_26_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_27_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_28_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_29_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_30_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_31_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_32_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_33_Down; //!
   double Weight_TopQuarkError_LepSF_EL_ID_34_Down; //!

   double Weight_TopQuarkError_LepSF_EL_Reco_Up; //!
   double Weight_TopQuarkError_LepSF_EL_Iso_Up; //!
   double Weight_TopQuarkError_LepSF_EL_Trigger_Up; //!

   double Weight_TopQuarkError_LepSF_EL_Reco_Down; //!
   double Weight_TopQuarkError_LepSF_EL_Iso_Down; //!
   double Weight_TopQuarkError_LepSF_EL_Trigger_Down; //!

   double Weight_TopQuarkError_LepSF_MU_ID_Up_emu; //!
   double Weight_TopQuarkError_LepSF_MU_ID_Down_emu; //!

   double Weight_TopQuarkError_LepSF_MU_TTVA_Up_emu; //! 
   double Weight_TopQuarkError_LepSF_MU_Trigger_Up_emu; //!
   double Weight_TopQuarkError_LepSF_MU_Iso_Up_emu; //!

   double Weight_TopQuarkError_LepSF_MU_TTVA_Down_emu; //! 
   double Weight_TopQuarkError_LepSF_MU_Trigger_Down_emu; //!
   double Weight_TopQuarkError_LepSF_MU_Iso_Down_emu; //!

   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_1; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_1; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_2; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_2; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_3; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_3; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_4; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_4; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_5; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_5; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_6; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_6; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_7; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_7; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_8; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_8; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_9; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_9; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_10; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_10; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_11; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_11; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_12; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_12; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_13; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_13; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_14; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_14; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_15; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_15; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_16; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_16; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_17; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_17; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_18; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_18; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_19; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_19; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_20; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_20; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_21; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_21; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_22; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_22; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_23; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_23; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_24; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_24; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_25; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_25; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_26; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_26; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_27; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_27; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_28; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_28; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_29; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_29; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_30; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_30; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_31; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_31; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_32; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_32; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_33; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_33; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_34; //!
   double Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_34; //!
   double Weight_TopQuarkError_MUID_Down; //!
   double Weight_TopQuarkError_MUMS_Down; //!
   double Weight_TopQuarkError_MUSCALE_Down; //!
   double Weight_TopQuarkError_MUSAGITTARES_Down; //!
   double Weight_TopQuarkError_MUSAGITTARHO_Down; //!
   double Weight_TopQuarkError_ELRES_MATERIALCALO_Down; //!
   double Weight_TopQuarkError_ELRES_MATERIALCRYO_Down; //!
   double Weight_TopQuarkError_ELRES_MATERIALGAP_Down; //!
   double Weight_TopQuarkError_ELRES_MATERIALIBL_Down; //!
   double Weight_TopQuarkError_ELRES_MATERIALPP0_Down; //!
   double Weight_TopQuarkError_ELRES_PILEUP_Down; //!
   double Weight_TopQuarkError_ELRES_SAMPLINGTERM_Down; //!
   double Weight_TopQuarkError_ELRES_ZSMEARING_Down; //!
   double Weight_TopQuarkError_ELSCALE_E4SCINTILLATOR_Down; //!
   double Weight_TopQuarkError_ELSCALE_G4_Down; //!
   double Weight_TopQuarkError_ELSCALE_L1GAIN_Down; //!
   double Weight_TopQuarkError_ELSCALE_L2GAIN_Down; //!
   double Weight_TopQuarkError_ELSCALE_LARCALIB_Down; //!
   double Weight_TopQuarkError_ELSCALE_LARELECALIB_Down; //!
   double Weight_TopQuarkError_ELSCALE_LARELECUNCONV_Down; //!
   double Weight_TopQuarkError_ELSCALE_MATCALO_Down; //!
   double Weight_TopQuarkError_ELSCALE_MATCRYO_Down; //!
   double Weight_TopQuarkError_ELSCALE_MATID_Down; //!
   double Weight_TopQuarkError_ELSCALE_MATPP0_Down; //!
   double Weight_TopQuarkError_ELSCALE_PEDESTAL_Down; //!
   double Weight_TopQuarkError_ELSCALE_PS_BARREL_Down; //!
   double Weight_TopQuarkError_ELSCALE_PS_Down; //!
   double Weight_TopQuarkError_ELSCALE_S12_Down; //!
   double Weight_TopQuarkError_ELSCALE_TOPOCLUSTER_THRES_Down; //!
   double Weight_TopQuarkError_ELSCALE_WTOTS1_THRES_Down; //!
   double Weight_TopQuarkError_ELSCALE_ZEESYST_Down; //!
  
   double Weight_indiv_SF_MU_ID_STAT_UP; //!
   double Weight_indiv_SF_MU_ID_STAT_DOWN; //!
   double Weight_indiv_SF_MU_ID_STAT_LOWPT_UP;//! 
   double Weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN; //!
   double Weight_indiv_SF_MU_ID_SYST_LOWPT_UP; //!
   double Weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN; //!
   double Weight_indiv_SF_MU_TTVA_STAT_UP; //!
   double Weight_indiv_SF_MU_TTVA_STAT_DOWN; //!
   double Weight_indiv_SF_MU_Trigger_STAT_UP; //!
   double Weight_indiv_SF_MU_Trigger_STAT_DOWN; //!
   double Weight_indiv_SF_MU_Isol_STAT_UP; //!
   double Weight_indiv_SF_MU_Isol_STAT_DOWN; //!

   double Weight_indiv_SF_EL_ChargeMisID_STAT_UP; //!
   double Weight_indiv_SF_EL_ChargeMisID_STAT_DOWN; //!
   double Weight_indiv_SF_EL_ChargeMisID_SYST_UP; //!
   double Weight_indiv_SF_EL_ChargeMisID_SYST_DOWN; //!

   double Weight_Lepton1_SF; //!
   double Weight_Lepton2_SF; //!

   double Weight_kFactor_Old; //!
   double Weight_kFactor_QCDEW; //!
   double Weight_kFactor_New_CT18NNLO; //!
   double Weight_kFactor_New_CT18ANNLO; //!
   double Weight_kFactor_PowhegtoSherpa; //!

   double Weight_PDF_UP; //!
   double Weight_PDF_DOWN; //!

   double Weight_kFactor_HERAPDF20; //!
   double Weight_kFactor_NNPDF30; //!
   double Weight_kFactor_ct14nnlo_pdf_up; //!
   double Weight_kFactor_ct14nnlo_pdf_down; //!
   double Weight_kFactor_pi_mem0_spline; //!
   double Weight_kFactor_pi_mem1_spline; //!
   double Weight_kFactor_z_scaleup; //!
   double Weight_kFactor_z_scaledown; //!
   double Weight_kFactor_CT14nnlo_as_0118_alpha_s_up_fit; //!
   double Weight_kFactor_CT14nnlo_as_0118_alpha_s_down_fit; //!
   double Weight_kFactor_Zgamma_beamUp_spline; //!
   double Weight_kFactor_Zgamma_beamDown_spline; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_1; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_2; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_3; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_4; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_5; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_6; //!
   double Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_7; //!
   double Weight_kFactor_PDF_EW__1up; //!
   double Weight_kFactor_PDF_EW__1down; //!
   double Weight_kFactor_PDF_REDCHOICE_NNPDF30; //!

   double Weight_Top_FSR_RenFact_Up; //!
   double Weight_Top_FSR_RenFact_Down; //!
   double Weight_Top_ISR_RenFact_Up; //!
   double Weight_Top_ISR_RenFact_Down; //!
   double Weight_Top_ISR_Var3c_Up; //!
   double Weight_Top_ISR_Var3c_Down; //!

   double Weight_Top_PDF4LHC_1; //!
   double Weight_Top_PDF4LHC_2; //!
   double Weight_Top_PDF4LHC_3; //!
   double Weight_Top_PDF4LHC_4; //!
   double Weight_Top_PDF4LHC_5; //!
   double Weight_Top_PDF4LHC_6; //!
   double Weight_Top_PDF4LHC_7; //!
   double Weight_Top_PDF4LHC_8; //!
   double Weight_Top_PDF4LHC_9; //!
   double Weight_Top_PDF4LHC_10; //!
   double Weight_Top_PDF4LHC_11; //!
   double Weight_Top_PDF4LHC_12; //!
   double Weight_Top_PDF4LHC_13; //!
   double Weight_Top_PDF4LHC_14; //!
   double Weight_Top_PDF4LHC_15; //!
   double Weight_Top_PDF4LHC_16; //!
   double Weight_Top_PDF4LHC_17; //!
   double Weight_Top_PDF4LHC_18; //!
   double Weight_Top_PDF4LHC_19; //!
   double Weight_Top_PDF4LHC_20; //!
   double Weight_Top_PDF4LHC_21; //!
   double Weight_Top_PDF4LHC_22; //!
   double Weight_Top_PDF4LHC_23; //!
   double Weight_Top_PDF4LHC_24; //!
   double Weight_Top_PDF4LHC_25; //!
   double Weight_Top_PDF4LHC_26; //!
   double Weight_Top_PDF4LHC_27; //!
   double Weight_Top_PDF4LHC_28; //!
   double Weight_Top_PDF4LHC_29; //!
   double Weight_Top_PDF4LHC_30; //!

   ULong64_t EventNumber; //!
   int n_Bjets;//!
   double HT; //!
   double MET_Et;  //!
   double MET_Phi;  //!
   double DeltaPhi_ll; //!
   double DeltaPhi_DilMET; //!

   double FEle_Pt;  //! 
   double FEle_EtCone20;  //! 
   double FEle_EtCone30;  //! 
   double FEle_EtCone40;  //! 

   double Lepton1_z0;  //!
   double Lepton2_z0;  //!
   double Lepton1_d0;  //!
   double Lepton2_d0;  //!
   double Lepton1_d0sig;  //!
   double Lepton2_d0sig;  //!

   double Lepton1_Pt;  //!
   double Lepton2_Pt;  //!
   double Lepton1_Eta;  //!
   double Lepton2_Eta;  //!
   double Lepton1_Phi;  //!
   double Lepton2_Phi;  //!

   double Lepton1_Pt_Born;  //!
   double Lepton2_Pt_Born;  //!
   double Lepton1_Pt_Bare;  //!
   double Lepton2_Pt_Bare;  //!
   double Lepton1_Pt_Dressed; //!
   double Lepton2_Pt_Dressed;  //!

   double Lepton1_Eta_Born;  //!
   double Lepton2_Eta_Born;  //!
   double Lepton1_Eta_Bare;  //!
   double Lepton2_Eta_Bare;  //!
   double Lepton1_Eta_Dressed;  //!
   double Lepton2_Eta_Dressed; //!

   double Lepton1_Phi_Born;  //!
   double Lepton2_Phi_Born;  //!
   double Lepton1_Phi_Bare;  //!
   double Lepton2_Phi_Bare;  //!
   double Lepton1_Phi_Dressed;  //!
   double Lepton2_Phi_Dressed; //!

   double RecoDilPhi; //!
   double RecoDilRapidity; //!
   double TruthDilRapidity_Born; //!
   double TruthDilRapidity_Bare; //!
   double TruthDilRapidity_Dressed; //!

   double RecoDilMass; //!
   double RecoPseudoDilMass; //!
   double RecoDilMass_MS; //!
   double RecoDilMass_ID; //!
   double TruthDilMass_Born; //!
   double TruthDilMass_Bare; //!
   double TruthDilMass_Dressed; //!

   double RecoDilPt; //!
   double TruthDilPt_Born; //!
   double TruthDilPt_Bare; //!
   double TruthDilPt_Dressed; //!

   double RecoCosThetaStar; //!
   double TruthCosThetaStar_Born; //!
   double TruthCosThetaStar_Bare; //!
   double TruthCosThetaStar_Dressed; //!

   double RecoPhiStar; //!
   double TruthPhiStar_Born; //!
   double TruthPhiStar_Bare; //!
   double TruthPhiStar_Dressed; //!

   double RecoWeight; //!
   double weight_MM; //!
   double TruthWeight; //!

   double Weight_indiv_SF_EL_Trigger; //!
   double Weight_indiv_SF_EL_Trigger_UP; //!
   double Weight_indiv_SF_EL_Trigger_DOWN; //!
   double Weight_indiv_SF_EL_Reco; //!
   double Weight_indiv_SF_EL_Reco_UP; //!
   double Weight_indiv_SF_EL_Reco_DOWN; //!
   double Weight_indiv_SF_EL_ID; //!
   double Weight_indiv_SF_EL_ID_UP; //!
   double Weight_indiv_SF_EL_ID_DOWN; //!
   double Weight_indiv_SF_EL_Isol; //!
   double Weight_indiv_SF_EL_Isol_UP; //!
   double Weight_indiv_SF_EL_Isol_DOWN; //!

   double Weight_indiv_SF_MU_Trigger; //!
   double Weight_indiv_SF_MU_Trigger_UP; //!
   double Weight_indiv_SF_MU_Trigger_DOWN; //!
   double Weight_indiv_SF_MU_TTVA; //!
   double Weight_indiv_SF_MU_TTVA_UP; //!
   double Weight_indiv_SF_MU_TTVA_DOWN; //!
   double Weight_indiv_SF_MU_ID; //!
   double Weight_indiv_SF_MU_ID_UP; //!
   double Weight_indiv_SF_MU_ID_DOWN; //!
   double Weight_indiv_SF_MU_Isol; //!
   double Weight_indiv_SF_MU_Isol_UP; //!
   double Weight_indiv_SF_MU_Isol_DOWN; //!

   double Weight_ME; //!
   double Weight_Normalisation; //!
   double Weight_NTrials; //!
   double Weight_MUR05_MUF05; //!
   double Weight_MUR05_MUF1; //!
   double Weight_MUR1_MUF05; //!
   double Weight_MUR1_MUF1; //!
   double Weight_MUR05_MUF2; //!
   double Weight_MUR2_MUF1; //!
   double Weight_MUR2_MUF2; //!
   
   std::string    nameOfTree; 
   std::string    nameOfSample;
   std::string    nameOfWeight; 
   bool Unfolding; 
   bool doElectronChannel; 
   bool doMuonChannel; 
   std::string    truthLevel;

   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_globalLeptonTriggerSF;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   vector<float>   *weight_leptonSF_EL_SF_SIMPLIFIED_Reco_UP;
   vector<float>   *weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN;
   vector<float>   *weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP;
   vector<float>   *weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN;
   vector<float>   *weight_leptonSF_EL_SF_SIMPLIFIED_Iso_UP;
   vector<float>   *weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_UP;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeID;
   Float_t         weight_indiv_SF_EL_ChargeID_UP;
   Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_jvt;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           mumuSelection;
   Int_t           emu;
   Int_t           eeSelection;
   Int_t           eeSelection_withmuon;
   Bool_t          HLT_mu50;
   Bool_t          HLT_mu26_ivarmedium;
   Bool_t          HLT_2mu14;
   Bool_t          HLT_2e24_lhvloose_nod0;
   Bool_t          HLT_e140_lhloose_nod0;
   Bool_t          HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          HLT_e60_lhmedium_nod0;
   Bool_t          HLT_mu22_mu8noL1;
   Bool_t          HLT_2e17_lhvloose_nod0;
   Bool_t          HLT_2e12_lhloose_L12EM10VH;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu22_mu8noL1;
   vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   Double_t        weight_KFactor;
   vector<bool>    *mu_isMedium;
   vector<bool>    *mu_isHighPt;
   vector<float>   *mu_PtID;
   vector<float>   *mu_PtMS;
   vector<bool>    *mu_isolation_FixedCutTight;
   vector<bool>    *mu_isolation_FixedCutLoose;
   vector<bool>    *mu_isolation_FixedCutPflowTight;
   vector<bool>    *mu_isolation_FixedCutPflowLoose;
   vector<bool>    *mu_isolation_FCTightTrackOnly_FixedRad;
   vector<bool>    *el_isolation_FixedCutLoose;
   vector<bool>    *el_isolation_FixedCutTight;
   vector<bool>    *el_isolation_PflowTight;
   vector<bool>    *el_isolation_FCHighPtCaloOnly;
   vector<bool>    *el_isolation_TightTrackOnly;
   vector<bool>    *el_isolation_TightTrackOnly_FixedRad;
   vector<bool>    *el_isElTight;
   vector<bool>    *fwdel_isTight;
   vector<bool>    *fwdel_isMedium;
   vector<bool>    *fwdel_isLoose;
   vector<bool>    *el_isElMedium;
   vector<bool>    *el_isElLoose;
   vector<bool>    *el_DFCommonElectronsECIDS;
   vector<double>  *el_DFCommonElectronsECIDSResult;
   vector<float>   *el_isPromptLepton;
   Float_t         Quark2_Px;
   Float_t         Quark2_Py;
   Float_t         Quark2_Pz;
   Float_t         Quark2_E;
   Float_t         Quark2_PDGID;
   Float_t         Quark1_Px;
   Float_t         Quark1_Py;
   Float_t         Quark1_Pz;
   Float_t         Quark1_E;
   Float_t         Quark1_PDGID;
   Float_t         Dilepton_Pt_born;
   Float_t         Dilepton_Rapidity_born;
   Float_t         Dilepton_Eta_born;
   Float_t         Dilepton_Phi_born;
   Float_t         Dilepton_Mass_born;
   Float_t         Dilepton_m_born;
   Float_t         Lepton_Pt_born;
   Float_t         AntiLepton_Pt_born;
   Float_t         Lepton_Eta_born;
   Float_t         AntiLepton_Eta_born;
   Float_t         Lepton_Phi_born;
   Float_t         AntiLepton_Phi_born;
   Float_t         Lepton_m_born;
   Float_t         AntiLepton_m_born;
   Float_t         Lepton_Pt_bare;
   Float_t         AntiLepton_Pt_bare;
   Float_t         Lepton_Eta_bare;
   Float_t         AntiLepton_Eta_bare;
   Float_t         Lepton_Phi_bare;
   Float_t         AntiLepton_Phi_bare;
   Float_t         Lepton_Pt_dressed;
   Float_t         Lepton_Eta_dressed;
   Float_t         Lepton_Phi_dressed;
   Float_t         Lepton_Charge_dressed;
   Float_t         AntiLepton_Pt_dressed;
   Float_t         AntiLepton_Eta_dressed;
   Float_t         AntiLepton_Phi_dressed;
   Float_t         AntiLepton_Charge_dressed;
   vector<double>  *elTruth_Pt_dressed;
   vector<double>  *elTruth_Eta_dressed;
   vector<double>  *elTruth_Phi_dressed;
   vector<double>  *elTruth_Charge;
   vector<double>  *muTruth_Pt_dressed;
   vector<double>  *muTruth_Eta_dressed;
   vector<double>  *muTruth_Phi_dressed;
   vector<double>  *muTruth_Charge;
   
   TH1F* Top_Stat_emu; //!
   TH1F* Top_Stat_ee; //!
   TH1F* Top_Sys_Theory_Up_ee; //!	
   TH1F* Top_Sys_Theory_Down_ee; //!	

   TH1F* SysRatio_PDF4LHC_Total_Up_mumu; //!
   TH1F* SysRatio_PDF4LHC_Total_Down_mumu; //!

   TH1F* SysRatio_ISR_Var3c_Up_mumu; //!
   TH1F* SysRatio_FSR_RenFact_Up_mumu; //!
   TH1F* SysRatio_ISR_Up_mumu;  //!
   TH1F* SysRatio_HardScatter_Up_mumu; //!
   TH1F* SysRatio_FragHadModel_Up_mumu; //!
   TH1F* SysRatio_TopMass_Up_mumu; //!
   TH1F* SysRatio_ISR_RenFact_Up_mumu; //!

   TH1F* SysRatio_ISR_Var3c_Down_mumu; //!
   TH1F* SysRatio_FSR_RenFact_Down_mumu; //!
   TH1F* SysRatio_ISR_Down_mumu; //!
   TH1F* SysRatio_HardScatter_Down_mumu; //!
   TH1F* SysRatio_FragHadModel_Down_mumu; //!
   TH1F* SysRatio_TopMass_Down_mumu; //!
   TH1F* SysRatio_ISR_RenFact_Down_mumu; //!

   TH1F* SysRatio_PDF4LHC_Total_Up_ee; //!                                                                                                                                                              
   TH1F* SysRatio_PDF4LHC_Total_Down_ee; //!
                                                                                                                                                             
   TH1F* SysRatio_ISR_Var3c_Up_ee; //!                                                                                                                                                                  
   TH1F* SysRatio_FSR_RenFact_Up_ee; //!                                                                                                                                                                
   TH1F* SysRatio_ISR_Up_ee;//!                                                                                                                                                                        
   TH1F* SysRatio_HardScatter_Up_ee; //!                                                                                                                                                                
   TH1F* SysRatio_FragHadModel_Up_ee;//!                                                                                                                                                                
   TH1F* SysRatio_TopMass_Up_ee; //!                                                                                                                                                                    
   TH1F* SysRatio_ISR_RenFact_Up_ee; //!                                                                                                                                                                 

   TH1F* SysRatio_ISR_Var3c_Down_ee; //!                                                                                                                                                                
   TH1F* SysRatio_FSR_RenFact_Down_ee; //!                                                                                                                                                              
   TH1F* SysRatio_ISR_Down_ee; //!                                                                                                                                                                      
   TH1F* SysRatio_HardScatter_Down_ee; //!                                                                                                                                                              
   TH1F* SysRatio_FragHadModel_Down_ee; //!                                                                                                                                                             
   TH1F* SysRatio_TopMass_Down_ee; //!                                                                                                                                                                 
   TH1F* SysRatio_ISR_RenFact_Down_ee; //!    


   TH1F* SysRatioTop_ELRES_MATERIALCALO_Up_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALCRYO_Up_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALGAP_Up_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALIBL_Up_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALPP0_Up_ee; //!
   TH1F* SysRatioTop_ELRES_PILEUP_Up_ee; //!
   TH1F* SysRatioTop_ELRES_SAMPLINGTERM_Up_ee; //!
   TH1F* SysRatioTop_ELRES_ZSMEARING_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_E4SCINTILLATOR_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_G4_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_L1GAIN_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_L2GAIN_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARCALIB_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARELECALIB_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARELECUNCONV_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATCALO_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATCRYO_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATID_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATPP0_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_PEDESTAL_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_PS_BARREL_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_ELSCALE_PS_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_S12_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_WTOTS1_THRES_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_ZEESYST_Up_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARUNCONVCALIB_Up_ee; //!
        	
   TH1F* SysRatioTop_ELRES_MATERIALCALO_Down_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALCRYO_Down_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALGAP_Down_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALIBL_Down_ee; //!
   TH1F* SysRatioTop_ELRES_MATERIALPP0_Down_ee; //!
   TH1F* SysRatioTop_ELRES_PILEUP_Down_ee; //!
   TH1F* SysRatioTop_ELRES_SAMPLINGTERM_Down_ee; //!
   TH1F* SysRatioTop_ELRES_ZSMEARING_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_E4SCINTILLATOR_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_G4_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_L1GAIN_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_L2GAIN_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARCALIB_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARELECALIB_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARELECUNCONV_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATCALO_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATCRYO_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATID_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_MATPP0_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_PEDESTAL_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_PS_BARREL_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_ELSCALE_PS_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_S12_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_WTOTS1_THRES_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_ZEESYST_Down_ee; //!
   TH1F* SysRatioTop_ELSCALE_LARUNCONVCALIB_Down_ee; //!       	
        	
   TH1F* SysRatioTop_MU_ID_Up_ee; //!
   TH1F* SysRatioTop_MU_ID_Down_ee; //!
   TH1F* SysRatioTop_MU_MS_Up_ee; //!
   TH1F* SysRatioTop_MU_MS_Down_ee; //!
   TH1F* SysRatioTop_MU_SCALE_Up_ee; //!
   TH1F* SysRatioTop_MU_SCALE_Down_ee; //!
   TH1F* SysRatioTop_MU_SAGHITTARES_Up_ee; //!
   TH1F* SysRatioTop_MU_SAGHITTARES_Down_ee; //!
   TH1F* SysRatioTop_MU_SAGHITTARHO_Up_ee; //!
   TH1F* SysRatioTop_MU_SAGHITTARHO_Down_ee; //!
        	
   TH1F* SysRatioTop_LepSF_EL_ID_1_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_2_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_3_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_4_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_5_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_6_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_7_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_8_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_9_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_10_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_11_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_12_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_13_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_14_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_15_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_16_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_17_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_18_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_19_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_20_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_21_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_22_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_23_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_24_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_25_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_26_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_27_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_28_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_29_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_30_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_31_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_32_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_33_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_34_Up_ee; //!

   TH1F* SysRatioTop_LepSF_EL_ID_1_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_2_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_3_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_4_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_5_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_6_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_7_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_8_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_9_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_10_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_11_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_12_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_13_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_14_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_15_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_16_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_17_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_18_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_19_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_20_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_21_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_22_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_23_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_24_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_25_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_26_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_27_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_28_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_29_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_30_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_31_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_32_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_33_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_ID_34_Down_ee; //!

   TH1F* SysRatioTop_LepSF_EL_Reco_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_Iso_Up_ee; //!
   TH1F* SysRatioTop_LepSF_EL_Trigger_Up_ee; //!

   TH1F* SysRatioTop_LepSF_EL_Reco_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_Iso_Down_ee; //!
   TH1F* SysRatioTop_LepSF_EL_Trigger_Down_ee; //!

   TH1F* SysRatioTop_LepSF_MU_Reco_Up_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Reco_Down_ee; //!

   TH1F* SysRatioTop_LepSF_MU_TTVA_Up_ee; //! 
   TH1F* SysRatioTop_LepSF_MU_Trigger_Up_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_Up_ee; //!

   TH1F* SysRatioTop_LepSF_MU_TTVA_Down_ee; //! 
   TH1F* SysRatioTop_LepSF_MU_Trigger_Down_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_Down_ee; //!
        	
   TH1F* SysRatioTop_LepSF_MU_TTVA_STAT_Up_ee; //!
   TH1F* SysRatioTop_LepSF_MU_TTVA_STAT_Down_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_STAT_Up_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_STAT_Down_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Trigger_STAT_Up_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Trigger_STAT_Down_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Reco_STAT_Up_ee; //!
   TH1F* SysRatioTop_LepSF_MU_Reco_STAT_Down_ee; //!        
		
   TH1F* Top_Stat_mumu; //!
   TH1F* Top_Sys_Theory_Up_mumu; //!	
   TH1F* Top_Sys_Theory_Down_mumu; //!

   TH1F* SysRatioTop_ELRES_MATERIALCALO_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALCRYO_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALGAP_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALIBL_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALPP0_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_PILEUP_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_SAMPLINGTERM_Up_mumu; //!
   TH1F* SysRatioTop_ELRES_ZSMEARING_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_E4SCINTILLATOR_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_G4_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_L1GAIN_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_L2GAIN_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARCALIB_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARELECALIB_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARELECUNCONV_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATCALO_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATCRYO_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATID_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATPP0_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_PEDESTAL_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_PS_BARREL_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_ELSCALE_PS_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_S12_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_WTOTS1_THRES_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_ZEESYST_Up_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARUNCONVCALIB_Up_mumu; //!
        	
   TH1F* SysRatioTop_ELRES_MATERIALCALO_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALCRYO_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALGAP_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALIBL_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_MATERIALPP0_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_PILEUP_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_SAMPLINGTERM_Down_mumu; //!
   TH1F* SysRatioTop_ELRES_ZSMEARING_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_E4SCINTILLATOR_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_G4_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_L1GAIN_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_L2GAIN_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARCALIB_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARELECALIB_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARELECUNCONV_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATCALO_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATCRYO_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATID_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_MATPP0_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_PEDESTAL_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_PS_BARREL_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_ELSCALE_PS_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_S12_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_WTOTS1_THRES_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_ZEESYST_Down_mumu; //!
   TH1F* SysRatioTop_ELSCALE_LARUNCONVCALIB_Down_mumu; //!       	
        	
   TH1F* SysRatioTop_MU_ID_Up_mumu; //!
   TH1F* SysRatioTop_MU_ID_Down_mumu; //!
   TH1F* SysRatioTop_MU_MS_Up_mumu; //!
   TH1F* SysRatioTop_MU_MS_Down_mumu; //!
   TH1F* SysRatioTop_MU_SCALE_Up_mumu; //!
   TH1F* SysRatioTop_MU_SCALE_Down_mumu; //!
   TH1F* SysRatioTop_MU_SAGHITTARES_Up_mumu; //!
   TH1F* SysRatioTop_MU_SAGHITTARES_Down_mumu; //!
   TH1F* SysRatioTop_MU_SAGHITTARHO_Up_mumu; //!
   TH1F* SysRatioTop_MU_SAGHITTARHO_Down_mumu; //!
        	
   TH1F* SysRatioTop_LepSF_EL_ID_1_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_2_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_3_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_4_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_5_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_6_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_7_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_8_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_9_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_10_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_11_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_12_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_13_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_14_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_15_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_16_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_17_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_18_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_19_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_20_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_21_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_22_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_23_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_24_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_25_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_26_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_27_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_28_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_29_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_30_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_31_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_32_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_33_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_34_Up_mumu; //!

   TH1F* SysRatioTop_LepSF_EL_ID_1_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_2_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_3_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_4_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_5_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_6_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_7_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_8_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_9_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_10_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_11_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_12_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_13_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_14_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_15_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_16_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_17_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_18_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_19_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_20_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_21_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_22_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_23_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_24_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_25_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_26_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_27_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_28_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_29_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_30_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_31_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_32_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_33_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_ID_34_Down_mumu; //!

   TH1F* SysRatioTop_LepSF_EL_Reco_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_Iso_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_Trigger_Up_mumu; //!

   TH1F* SysRatioTop_LepSF_EL_Reco_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_Iso_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_EL_Trigger_Down_mumu; //!

   TH1F* SysRatioTop_LepSF_MU_Reco_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Reco_Down_mumu; //!

   TH1F* SysRatioTop_LepSF_MU_TTVA_Up_mumu; //! 
   TH1F* SysRatioTop_LepSF_MU_Trigger_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_Up_mumu; //!

   TH1F* SysRatioTop_LepSF_MU_TTVA_Down_mumu; //! 
   TH1F* SysRatioTop_LepSF_MU_Trigger_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_Down_mumu; //!
        	
   TH1F* SysRatioTop_LepSF_MU_TTVA_STAT_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_TTVA_STAT_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_STAT_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Iso_STAT_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Trigger_STAT_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Trigger_STAT_Down_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Reco_STAT_Up_mumu; //!
   TH1F* SysRatioTop_LepSF_MU_Reco_STAT_Down_mumu; //!      

   TFile* TopSys_ee ; //!
   TFile* TopSys_mumu ; //!
   
   TFile* TopSys_ee_116_150; //!
   TFile* TopSys_mumu_116_150; //!
   TFile* TopSys_ee_150_200; //!
   TFile* TopSys_mumu_150_200; //!
   TFile* TopSys_ee_200_300; //!
   TFile* TopSys_mumu_200_300; //!
   TFile* TopSys_ee_300_1500; //!
   TFile* TopSys_mumu_300_1500; //!

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_globalLeptonTriggerSF;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_Iso_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_mumuSelection;   //!
   TBranch        *b_emu;   //!
   TBranch        *b_eeSelection;   //!
   TBranch        *b_eeSelection_withmuon;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_2mu14;   //!
   TBranch        *b_HLT_2e24_lhvloose_nod0;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_mu22_mu8noL1;   //!
   TBranch        *b_HLT_2e17_lhvloose_nod0;   //!
   TBranch        *b_HLT_2e12_lhloose_L12EM10VH;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu22_mu8noL1;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_weight_KFactor;   //!
   TBranch        *b_mu_isMedium;   //!
   TBranch        *b_mu_isHighPt;   //!
   TBranch        *b_mu_PtID;   //!
   TBranch        *b_mu_PtMS;   //!
   TBranch        *b_mu_isolation_FixedCutTight;   //!
   TBranch        *b_mu_isolation_FixedCutLoose;   //!
   TBranch        *b_mu_isolation_FixedCutPflowTight;   //!
   TBranch        *b_mu_isolation_FixedCutPflowLoose;   //!
   TBranch        *b_mu_isolation_FCTightTrackOnly_FixedRad;   //!
   TBranch        *b_el_isolation_FixedCutLoose;   //!
   TBranch        *b_el_isolation_FixedCutTight;   //!
   TBranch        *b_el_isolation_PflowTight;   //!
   TBranch        *b_el_isolation_TightTrackOnly;   //!
   TBranch        *b_el_isolation_TightTrackOnly_FixedRad;   //!
   TBranch        *b_el_isolation_FCHighPtCaloOnly;   //!
   TBranch        *b_el_isElTight;   //!
   TBranch        *b_fwdel_isTight;   //!
   TBranch        *b_fwdel_isMedium;   //!
   TBranch        *b_fwdel_isLoose;   //!
   TBranch        *b_el_isElMedium;   //!
   TBranch        *b_el_isElLoose;   //!
   TBranch        *b_el_DFCommonElectronsECIDS;   //!
   TBranch        *b_el_DFCommonElectronsECIDSResult;   //!
   TBranch        *b_el_isPromptLepton;   //!
   TBranch        *b_Quark2_Px;   //!
   TBranch        *b_Quark2_Py;   //!
   TBranch        *b_Quark2_Pz;   //!
   TBranch        *b_Quark2_E;   //!
   TBranch        *b_Quark2_PDGID;   //!
   TBranch        *b_Quark1_Px;   //!
   TBranch        *b_Quark1_Py;   //!
   TBranch        *b_Quark1_Pz;   //!
   TBranch        *b_Quark1_E;   //!
   TBranch        *b_Quark1_PDGID;   //!
   TBranch        *b_Dilepton_Pt_born;   //!
   TBranch        *b_Dilepton_Rapidity_born;   //!
   TBranch        *b_Dilepton_Eta_born;   //!
   TBranch        *b_Dilepton_Phi_born;   //!
   TBranch        *b_Dilepton_Mass_born;   //!
   TBranch        *b_Dilepton_m_born;   //!
   TBranch        *b_Lepton_Pt_born;   //!
   TBranch        *b_AntiLepton_Pt_born;   //!
   TBranch        *b_Lepton_Eta_born;   //!
   TBranch        *b_AntiLepton_Eta_born;   //!
   TBranch        *b_Lepton_Phi_born;   //!
   TBranch        *b_AntiLepton_Phi_born;   //!
   TBranch        *b_Lepton_m_born;   //!
   TBranch        *b_AntiLepton_m_born;   //!
   TBranch        *b_Lepton_Pt_bare;   //!
   TBranch        *b_AntiLepton_Pt_bare;   //!
   TBranch        *b_Lepton_Eta_bare;   //!
   TBranch        *b_AntiLepton_Eta_bare;   //!
   TBranch        *b_Lepton_Phi_bare;   //!
   TBranch        *b_AntiLepton_Phi_bare;   //!
   TBranch        *b_Lepton_Pt_dressed;   //!
   TBranch        *b_Lepton_Eta_dressed;   //!
   TBranch        *b_Lepton_Phi_dressed;   //!
   TBranch        *b_Lepton_Charge_dressed;   //!
   TBranch        *b_AntiLepton_Pt_dressed;   //!
   TBranch        *b_AntiLepton_Eta_dressed;   //!
   TBranch        *b_AntiLepton_Phi_dressed;   //!
   TBranch        *b_AntiLepton_Charge_dressed;   //!
   TBranch        *b_elTruth_Pt_dressed;   //!
   TBranch        *b_elTruth_Eta_dressed;   //!
   TBranch        *b_elTruth_Phi_dressed;   //!
   TBranch        *b_elTruth_Charge;   //!
   TBranch        *b_muTruth_Pt_dressed;   //!
   TBranch        *b_muTruth_Eta_dressed;   //!
   TBranch        *b_muTruth_Phi_dressed;   //!
   TBranch        *b_muTruth_Charge;   //!

   // Declaration of leaf types
   Int_t           dsid;
   Int_t           isAFII;
   string          *generators;
   string          *AMITag;
   Float_t         totalEventsWeighted;
   ULong64_t       totalEvents;
   
   // List of branches
   TBranch        *b_dsid;   //!
   TBranch        *b_isAFII;   //!
   TBranch        *b_generators;   //!
   TBranch        *b_AMITag;   //!
   TBranch        *b_totalEventsWeighted;   //!
   TBranch        *b_totalEvents;   //!

   //Comparison(TTree *tree=0);
   Comparison( std::string systematics, std::string sample, std::string weights, bool doUnfolding, bool doElectrons, bool doMuons, std::string LevelTruth );
   virtual ~Comparison(void){};
   virtual int    Cut(Long64_t entry);
   virtual int    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual bool   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual double   Normalisation( int mcChannelNumber, int runNumber, double MCWeight );
   virtual double   PhiStarEta( double phi_1, double eta_1, double phi_2, double eta_2 );
   virtual double   cosThetaStar( TLorentzVector LeptonPlus, TLorentzVector LeptonMinus, TLorentzVector Propagator );
   virtual double   CalculateDeltaPhi( double Phi1, double Phi2 );
   virtual void     SetTreeBranches();
   virtual void     ResetVariables();

   virtual double   Zgamma_CT14nnlo_CT10(double x);
   virtual double   ew_z_spline(double x);
   virtual double   Zgamma_CT10nnlo_CT10nnlo(double x);
   virtual double   z_all_pi_ave_spline(double x);

   virtual double Zgamma_CT14nnlo_CT10_New(double x);
   virtual double Zgamma_CT18NNLO_CT14nnlo_Central_spline(double x);
   virtual double Zgamma_CT18ANNLO_CT14nnlo_Central_spline(double x);
   virtual double Zgamma_CT18ANNLO_CT14nnlo_Up_spline(double x);
   virtual double Zgamma_CT18ANNLO_CT14nnlo_Down_spline(double x);

   virtual double Zgamma_HERAPDF20_CT14nnlo(double x);
   virtual double Zgamma_NNPDF30_CT14nnlo(double x);
   virtual double Zgamma_ct14nnlo_pdf_up(double x);
   virtual double Zgamma_ct14nnlo_pdf_down(double x);
   virtual double z_scaleup(double x);
   virtual double z_scalelow(double x);

   virtual double Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(double x);
   virtual double Zgamma_CT14nnlo_as_0118_alpha_s_down_fit(double x);
   virtual double Zgamma_beamUp_spline(double x);
   virtual double Zgamma_beamDown_spline(double x);

   virtual double z_all_pi_mem0_spline(double x);
   virtual double z_all_pi_mem1_spline(double x);

   virtual double Zgamma_CT14nnlo_EWbun_EV_1(double x);
   virtual double Zgamma_CT14nnlo_EWbun_EV_2(double x);
   virtual double Zgamma_CT14nnlo_EWbun_EV_3(double x);
   virtual double Zgamma_CT14nnlo_EWbun_EV_4(double x);
   virtual double Zgamma_CT14nnlo_EWbun_EV_5(double x);
   virtual double Zgamma_CT14nnlo_EWbun_EV_6(double x);
   virtual double Zgamma_CT14nnlo_EWbun_EV_7(double x);

   virtual double MuonSmearing(double mu_pt, double mu_eta);

   virtual bool RFrame(TLorentzVector Z, TVector3 &RFAxis, TVector3 &xAxis, TVector3 &yAxis,string frame);
   virtual double GetCosThetaRF(TLorentzVector lepM,TLorentzVector lepP,    string frame);
   virtual double GetPhiRF(TLorentzVector lepM,TLorentzVector lepP,       string frame);
   virtual void GetHistograms( double Mass );

}; // end of function

#endif

#ifdef Comparison_cxx

Comparison::Comparison( std::string systematics, std::string sample, std::string weights, bool doUnfolding, bool doElectrons, bool doMuons, std::string LevelTruth ) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   TTree* tree;

   nameOfTree=systematics;
   nameOfSample=sample;
   nameOfWeight=weights;
   Unfolding=doUnfolding;
   doElectronChannel=doElectrons;
   doMuonChannel=doMuons;
   truthLevel=LevelTruth;

   cout<<"Running on "<<nameOfTree<<" nameOfSample "<<nameOfSample<<" nameOfWeight "<<nameOfWeight<<endl;
   cout<<"doElectronChannel "<<doElectronChannel<<" doMuonChannel "<<doMuonChannel<<" Unfolding "<<Unfolding<<" truthLevel "<<truthLevel<<endl;

   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("nominal",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain( nameOfTree.c_str() ,""); 

      if( nameOfSample == "Data15_TOPQ1" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.AllYear.physics_Main.DAOD_TOPQ1.grp15_v01_p4173.126_output_root/*root");
      } //end of function
      if( nameOfSample == "Data16_TOPQ1" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.AllYear.physics_Main.DAOD_TOPQ1.grp16_v01_p4173.126_output_root/*root");
      } //end of function
      if( nameOfSample == "Data17_TOPQ1" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.AllYear.physics_Main.DAOD_TOPQ1.grp17_v01_p4173.21_2_126_output_root/*root");
      } //end of function
      if( nameOfSample == "Data18_TOPQ1" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.AllYear.physics_Main.DAOD_TOPQ1.grp18_v01_p4173.21_2_126_output_root/*root");
      } //end of function
      if( nameOfSample == "Data15" ){ 
	   //Data15
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodD.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data15_output_root/*root");
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodE.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data15_output_root/*root");
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodF.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data15_output_root/*root");
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodG.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data15_output_root/*root");
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodH.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data15_output_root/*root");
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodJ.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data15_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_1" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodA.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_2" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodB.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_3" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodC.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_4" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodD.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_5" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodE.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of if 
     if( nameOfSample == "Data16_6" ){  
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodF.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_7" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodG.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_8" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodI.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_9" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodK.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
     } // end of file
     if( nameOfSample == "Data16_10" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodL.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data16_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data17_1" ){ 
	//Data17
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodB.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data17_2" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodC.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data17_3" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodD.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");  
      } // end of loop
      if( nameOfSample == "Data17_4" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodE.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data17_5" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodF.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data17_6" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodH.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root"); 
      } // end of loop
      if( nameOfSample == "Data17_7" ){
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodI.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data17_8" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodK.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data17_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_1" ){ 
	//Data18
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodB.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_2" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodC.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_3" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodD.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of if statement
      if( nameOfSample == "Data18_4" ){
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodF.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_5" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodI.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_6" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodK.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of if statement
      if( nameOfSample == "Data18_7" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodL.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_8" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodM.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of if statement
      if( nameOfSample == "Data18_9" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodO.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of loop
      if( nameOfSample == "Data18_10" ){ 
           chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/Data/STDM4/user.mcanobre.periodQ.physics_Main.DAOD_STDM4.grp23_v01_p4238.151_Data18_output_root/*root");
      } // end of if statement
      if( nameOfSample == "DrellYan_ee_mc16a" ){
        //************************** MC16a ******************************** //
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301000.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301001.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301002.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301003.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301004.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301005.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301006.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301007.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301008.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301009.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301010.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301011.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301012.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301013.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301014.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301015.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301016.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301017.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301018.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt2_output_root/*root");
      } // end of if statement
      if( nameOfSample == "DrellYan_ee_mc16d" ){
	// ************************ MC16d ****************************** //
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301000.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301001.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301002.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301003.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301004.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301005.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301006.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301007.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301008.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301009.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301010.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301011.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301012.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301013.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301014.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301015.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301016.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301017.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301018.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt2_output_root/*root");
      } // end of if statement
      if( nameOfSample == "DrellYan_ee_mc16e" ){
	// ************************ MC16e ****************************** //
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301000.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301001.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301002.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301003.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301004.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301005.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301006.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301007.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301008.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301009.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301010.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301011.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301012.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301013.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301014.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301015.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301016.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301017.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301018.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt2_output_root/*root");
      } // end of if DrellYan
      if( nameOfSample == "DrellYan_mumu_mc16a" ){
        //************************** MC16a ******************************** //
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301020.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301021.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301022.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301023.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301024.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301025.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301026.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301027.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301028.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301029.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301030.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301031.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301032.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301033.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301034.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301035.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301036.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301037.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.301038.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r9364_p4239.151_HighPt3_output_root/*root");
      } // end of if statement
      if( nameOfSample == "DrellYan_mumu_mc16d" ){
	// ************************ MC16d ****************************** //
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301020.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt8_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301021.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301022.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301023.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301024.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301025.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301026.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301027.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301028.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt7_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301029.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301030.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301031.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301032.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301033.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301034.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301035.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301037.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.301038.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10201_p4239.151_HighPt3_output_root/*root");
      } // end of if statement
      if( nameOfSample == "DrellYan_mumu_mc16e" ){
	// ************************ MC16e ****************************** //
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301020.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301021.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301022.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301023.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301024.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301025.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301026.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301027.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301028.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301029.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301030.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301031.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301032.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301033.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301036.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301037.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.301038.PowhegPythia8EvtGen.DAOD_STDM4.e3649_s3126_r10724_p4239.151_HighPt3_output_root/*root");
     } // end of if DrellYan
      if( nameOfSample == "PI_ee_mc16a" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364834.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364835.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r9364_r9315_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364836.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r9364_r9315_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364837.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364838.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364839.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.163_DilTriggerRet_output_root/*root");
      } // end of if statement
      if( nameOfSample == "PI_ee_mc16d" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364834.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364835.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364836.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364837.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364838.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364839.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364840.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.163_DilTriggerRet_output_root/*root");
      } // end of if statement
      if( nameOfSample == "PI_ee_mc16e" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364834.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10724_r10726_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364835.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364836.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364837.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364838.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364839.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.163_DilTriggerRet_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364840.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.163_DilTriggerRet_output_root/*root");
      } // end of if statement
      if( nameOfSample == "PI_mumu_mc16a" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364841.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364842.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364843.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364844.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r9364_r9315_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364845.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364846.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r9364_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364847.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r9364_r9315_p4252.151_output_root/*root");
      } // end of if statement
      if( nameOfSample == "PI_mumu_mc16d" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364841.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364842.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364843.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364844.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10201_r10210_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364845.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364846.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364847.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10201_p4252.151_output_root/*root");
      } // end of if statement
      if( nameOfSample == "PI_mumu_mc16e" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364841.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10724_r10726_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364842.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10724_r10726_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364843.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10724_r10726_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364844.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364845.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364846.Pythia8EvtGen.DAOD_STDM3.e7621_s3126_r10724_p4252.151_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364847.Pythia8EvtGen.DAOD_STDM3.e7621_e5984_s3126_r10724_r10726_p4252.151_output_root/*root");

      } // end of if statement
     if( nameOfSample == "Diboson_mc16a" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.363356.Sherpa.DAOD_STDM4.e5525_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.363358.Sherpa.DAOD_STDM4.e5525_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.363360.Sherpa.DAOD_STDM4.e5983_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.364250.Sherpa.DAOD_STDM4.e5894_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.364253.Sherpa.DAOD_STDM4.e5916_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.364254.Sherpa.DAOD_STDM4.e5916_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Diboson/user.mcanobre.364255.Sherpa.DAOD_STDM4.e5916_s3126_r9364_p4237.151_HighPt10_output_root/*root");
     } // end of if Diboson
     if( nameOfSample == "Diboson_mc16d" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.363356.Sherpa.DAOD_STDM4.e5525_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.363355.Sherpa.DAOD_STDM4.e5525_s3126_r10201_p3975.151_HighPt_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.363357.Sherpa.DAOD_STDM4.e5525_e5984_s3126_r10201_r10210_p3975.151_HighPt_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.363358.Sherpa.DAOD_STDM4.e5525_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.363359.Sherpa.DAOD_STDM4.e5583_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.363360.Sherpa.DAOD_STDM4.e5983_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.364250.Sherpa.DAOD_STDM4.e5894_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.364253.Sherpa.DAOD_STDM4.e5916_e5984_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.364254.Sherpa.DAOD_STDM4.e5916_e5984_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Diboson/user.mcanobre.364255.Sherpa.DAOD_STDM4.e5916_s3126_r10201_p4237.151_HighPt10_output_root/*root");
     } // end of if Diboson
     if( nameOfSample == "Diboson_mc16e" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.363356.Sherpa.DAOD_STDM4.e5525_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.363358.Sherpa.DAOD_STDM4.e5525_s3126_r10724_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.363359.Sherpa.DAOD_STDM4.e5583_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.363360.Sherpa.DAOD_STDM4.e5983_s3126_r10724_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.364250.Sherpa.DAOD_STDM4.e5894_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.364253.Sherpa.DAOD_STDM4.e5916_s3126_r10724_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.364254.Sherpa.DAOD_STDM4.e5916_s3126_r10724_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Diboson/user.mcanobre.364255.Sherpa.DAOD_STDM4.e5916_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
     } // end of Dibosn
     if( nameOfSample == "Ztautau_mc16a" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Ztautau/user.mcanobre.361108.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
     } // end of Ztautau
     if( nameOfSample == "Ztautau_mc16d" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Ztautau/user.mcanobre.361108.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*.root");
     } // end of if Ztautau
     if( nameOfSample == "Ztautau_mc16e" ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Ztautau/user.mcanobre.361108.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_s3136_r10724_r10726_p4237.151_HighPt10_output_root/*.root");
     } // end of if Ztautau
     if( nameOfSample == "TopQuark_mc16a" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410472.PhPy8EG.DAOD_STDM4.e6348_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        //chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410470.PhPy8EG.DAOD_STDM4.e6337_s3126_r9364_p4237.151_Retry_output_root/*root");
     } // end of if ttbar
     if( nameOfSample == "singleTop_mc16a" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410644.PowhegPythia8EvtGen.DAOD_STDM4.e6527_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410645.PowhegPythia8EvtGen.DAOD_STDM4.e6527_e5984_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410646.PowhegPythia8EvtGen.DAOD_STDM4.e6552_e5984_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410647.PowhegPythia8EvtGen.DAOD_STDM4.e6552_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410658.PhPy8EG.DAOD_STDM4.e6671_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/TopQuark/user.mcanobre.410659.PhPy8EG.DAOD_STDM4.e6671_s3126_r9364_p4237.151_HighPt10_output_root/*root");
     } // end of if ttbar
     if( nameOfSample == "TopQuark_mc16d" ){
      // ttbar mc16d
        //chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410470.PhPy8EG.DAOD_STDM4.e6337_s3126_r10201_p4237.151_Retry_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410472.PhPy8EG.DAOD_STDM4.e6348_s3126_r10201_p4237.151_HighPt10_output_root/*root");
     } // end of if TopQuark
     if( nameOfSample == "singleTop_mc16d" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410644.PowhegPythia8EvtGen.DAOD_STDM4.e6527_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410645.PowhegPythia8EvtGen.DAOD_STDM4.e6527_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410646.PowhegPythia8EvtGen.DAOD_STDM4.e6552_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410647.PowhegPythia8EvtGen.DAOD_STDM4.e6552_e5984_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410658.PhPy8EG.DAOD_STDM4.e6671_s3126_r10201_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/TopQuark/user.mcanobre.410659.PhPy8EG.DAOD_STDM4.e6671_s3126_r10201_p4237.151_HighPt10_output_root/*root");
    } // end of if TopQuark
    if( nameOfSample == "TopQuark_mc16e" ){
      //ttbar mc16e
        //chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410470.PhPy8EG.DAOD_STDM4.e6337_s3126_r10724_p4237.151_Retry_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410472.PhPy8EG.DAOD_STDM4.e6348_s3126_r10724_p4237.151_HighPt11_output_root/*root");
    } // end of if TopQuark
    if( nameOfSample == "singleTop_mc16e" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410644.PowhegPythia8EvtGen.DAOD_STDM4.e6527_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410645.PowhegPythia8EvtGen.DAOD_STDM4.e6527_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410646.PowhegPythia8EvtGen.DAOD_STDM4.e6552_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410647.PowhegPythia8EvtGen.DAOD_STDM4.e6552_s3126_r10724_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410658.PhPy8EG.DAOD_STDM4.e6671_s3126_r10724_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410659.PhPy8EG.DAOD_STDM4.e6671_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
    } // end of if TopQuark
    if( nameOfSample == "Wjets_mc16a" ){
      //mc16a
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Wjets/user.mcanobre.361100.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_r9315_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Wjets/user.mcanobre.361101.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_r9315_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Wjets/user.mcanobre.361102.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Wjets/user.mcanobre.361103.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Wjets/user.mcanobre.361104.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Wjets/user.mcanobre.361105.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_r9315_p4237.151_HighPt10_output_root/*root");
    } // end of if Wjets
    if( nameOfSample == "Wjets_mc16d" ){
      //mc16d
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Wjets/user.mcanobre.361100.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10201_r10210_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Wjets/user.mcanobre.361101.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_r10201_r10210_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Wjets/user.mcanobre.361102.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Wjets/user.mcanobre.361103.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10201_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Wjets/user.mcanobre.361104.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10201_r10210_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Wjets/user.mcanobre.361105.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_r10201_r10210_p4237.151_HighPt10_output_root/*root");
    } // end of Wjets
    if( nameOfSample == "Wjets_mc16e" ){
      //mc16e
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Wjets/user.mcanobre.361100.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10724_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Wjets/user.mcanobre.361101.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_r10724_r10726_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Wjets/user.mcanobre.361102.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_r10724_r10726_p4237.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Wjets/user.mcanobre.361103.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10724_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Wjets/user.mcanobre.361104.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10724_p4239.151_HighPt10_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Wjets/user.mcanobre.361105.PowhegPythia8EvtGen.DAOD_STDM4.e3601_e5984_s3126_s3136_r10724_r10726_p4237.151_HighPt10_output_root/*root");
    } // end of Wjets
    if( nameOfSample == "Ztoee_mc16e" ){
      // Signal Dielectron                                                                                                                                                                                 
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Ztoll/user.mcanobre.361106.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10724_p4239.151_HighPt2_output_root/*.root");                                                                                                                                                                  
    } //end of if 
    if( nameOfSample == "Ztomumu_mc16e" ){
      // Signal Dimuon 
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/Ztoll/user.mcanobre.361107.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10724_p4239.151_HighPt10_output_root/*.root");
    } // end of if statement  
     if( nameOfSample == "Ztoee_mc16d" ){
        // Signal Dielectron mc16a
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Ztoll/user.mcanobre.361106.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10201_p4239.151_HighPt2_output_root/*.root");
     } //end of if statement
     if( nameOfSample == "Ztomumu_mc16d" ){
        // Signal Dimuon mc16a
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/Ztoll/user.mcanobre.361107.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r10201_p4097.151_HighPt10_output_root/*.root");
     } // end of if statement
     if( nameOfSample == "Ztoee_mc16a" ){
        // Signal Dielectron mc16a 
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Ztoll/user.mcanobre.361106.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_p4239.151_HighPt2_output_root/*.root");
     } //end of loop
     if( nameOfSample == "Ztomumu_mc16a" ){
        // Signal Dimuon mc16d
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/Ztoll/user.mcanobre.361107.PowhegPythia8EvtGen.DAOD_STDM4.e3601_s3126_r9364_p4239.151_HighPt10_output_root/*.root");  
     } // end of if Ztoll
     if( nameOfSample == "TopQuark_mc16e_Sys" ){
	 chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410472.PhPy8EG.DAOD_STDM4.e6348_s3126_r10724_p4237.189_Ret_output_root/*.root");
     }
     if( nameOfSample == "TopQuark_mc16e_Sys_HardScatter_MCAdNlo" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410465.aMcAtNloPy8EvtGen.DAOD_TOPQ1.e6762_a875_r10724_p4346.151_HighPt10_output_root/*.root");
     }
     if( nameOfSample == "TopQuark_mc16e_Sys_Fragmentation_Herwig7" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410558.PowhegHerwig7EvtGen.DAOD_TOPQ1.e6366_a875_r10724_p4346.151_HighPt10_output_root/*.root");
     }
     if( nameOfSample == "TopQuark_mc16e_Sys_ISR" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.410482.PhPy8EG.DAOD_TOPQ1.e6454_a875_r10724_p4346.151_HighPt10_output_root/*.root");
     }
     if( nameOfSample == "TopQuark_mc16e_Sys_MassDown" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.411054.PowhegPythia8EvtGen.DAOD_TOPQ1.e6696_a875_r10724_p4346.151_HighPt10_output_root/*.root");
     }
     if( nameOfSample == "TopQuark_mc16e_Sys_MassUP" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/TopQuark/user.mcanobre.411057.PowhegPythia8EvtGen.DAOD_TOPQ1.e6696_a875_r10724_p4346.151_HighPt10_output_root/*.root");
     }
     if( nameOfSample == "Sherpa_mumu_mc16a_0_70_LowMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364100.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364101.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364102.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364103.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364104.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364105.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364106.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364107.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364108.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364109.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364110.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364111.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364112.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364113.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4237.151_Re\
try_output_root/*root");
     }
     if( nameOfSample == "Sherpa_mumu_mc16a_70_140_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364103.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364104.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_4_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364105.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
     }
     if( nameOfSample == "Sherpa_mumu_mc16a_140_280_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364106.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364107.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364108.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
     }
     if( nameOfSample == "Sherpa_mumu_mc16a_280_500_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364109.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364110.Sherpa.DAOD_STDM4.e5271_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364111.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
     }
     if( nameOfSample == "Sherpa_mumu_mc16a_500_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364112.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364113.Sherpa.DAOD_STDM4.e5271_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
    } // end of if Sherpa mumu   
    if( nameOfSample == "Sherpa_ee_mc16a_0_70_LowMass" ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364114.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364115.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\
try_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364116.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\
try_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364117.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\
try_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364118.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\
try_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364119.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\
try_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364120.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364121.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364122.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364123.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\
try_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364124.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364125.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r9364_r9315_\
p4237.151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364126.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4237.\
151_Retry_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364127.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4237.151_Re\try_output_root/*root");
    } // end of if Sherpa ee  
     if( nameOfSample == "Sherpa_ee_mc16a_70_140_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364117.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364118.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364119.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
    } // end of if Sherpa ee  
     if( nameOfSample == "Sherpa_ee_mc16a_140_280_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364120.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364121.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364122.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
    } // end of if Sherpa ee  
     if( nameOfSample == "Sherpa_ee_mc16a_280_500_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364123.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364124.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364125.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
    } // end of if Sherpa ee  
     if( nameOfSample == "Sherpa_ee_mc16a_500_LowMass" ){
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364126.Sherpa.DAOD_STDM4.e5299_s3126_r9364_r9315_p4097.126_mc16a_2_output_root/*root");
             chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.364127.Sherpa.DAOD_STDM4.e5299_s3126_r9364_p4097.126_mc16a_2_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16a_0_70_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366300.Sh.DAOD_EXOT0.e7410_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366301.Sh.DAOD_EXOT0.e7410_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366302.Sh.DAOD_EXOT0.e7729_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu 
     if( nameOfSample == "Sherpa_mumu_mc16a_70_140_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366303.Sh.DAOD_EXOT0.e7410_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366304.Sh.DAOD_EXOT0.e7410_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366305.Sh.DAOD_EXOT0.e7729_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu 
     if( nameOfSample == "Sherpa_mumu_mc16a_140_280_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366306.Sh.DAOD_EXOT0.e7410_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366307.Sh.DAOD_EXOT0.e7729_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366308.Sh.DAOD_EXOT0.e7729_s3126_r9364_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16a_0_70_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366309.Sh.DAOD_EXOT0.e7411_a875_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366310.Sh.DAOD_EXOT0.e7411_a875_r9364_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_ee_mc16a_70_140_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366312.Sh.DAOD_EXOT0.e7411_a875_r9364_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366313.Sh.DAOD_EXOT0.e7411_a875_r9364_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16a_140_280_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.366315.Sh.DAOD_EXOT0.e7411_a875_r9364_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_mumu_mc16d_0_70_LowMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364100.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364101.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364102.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364103.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364104.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364105.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364106.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364107.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364108.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364109.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364110.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364111.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364112.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10201_r10210_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364113.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4237.151_Retry_\
output_root/*root");
     } // end of if Sherpa mumu 
     if( nameOfSample == "Sherpa_mumu_mc16d_70_140_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364103.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364104.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364105.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_mumu_mc16d_140_280_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364106.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364107.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364108.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_mumu_mc16d_280_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364109.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364110.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364111.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_mumu_mc16d_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364112.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364113.Sherpa.DAOD_STDM4.e5271_s3126_r10201_p4097.126_mc16d_output_root/*root");       
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_0_70_LowMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364114.Sherpa.DAOD_STDM4.e5299_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364115.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364116.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364117.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364118.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364119.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364120.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364121.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364122.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364123.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364124.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364125.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364126.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364127.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_ee_mc16d_70_140_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364117.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364118.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364119.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_140_280_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364120.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364121.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364122.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_280_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364123.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364124.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364125.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364126.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10201_r10210_p4097.126_mc16d_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364127.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16d_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16d_0_70_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366300.Sh.DAOD_EXOT0.e7410_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366301.Sh.DAOD_EXOT0.e7410_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366302.Sh.DAOD_EXOT0.e7729_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16d_70_140_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366303.Sh.DAOD_EXOT0.e7410_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366304.Sh.DAOD_EXOT0.e7410_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366305.Sh.DAOD_EXOT0.e7729_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16d_140_280_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366306.Sh.DAOD_EXOT0.e7410_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366307.Sh.DAOD_EXOT0.e7729_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366308.Sh.DAOD_EXOT0.e7729_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_0_70_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366309.Sh.DAOD_EXOT0.e7411_a875_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366310.Sh.DAOD_EXOT0.e7411_a875_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_70_140_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366312.Sh.DAOD_EXOT0.e7411_a875_r10201_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366313.Sh.DAOD_EXOT0.e7411_a875_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16d_140_280_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366315.Sh.DAOD_EXOT0.e7411_a875_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_mumu_mc16e_0_70_LowMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364100.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364101.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364102.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364103.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364104.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364105.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364106.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364107.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364108.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364109.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364110.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364111.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364112.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10201_r10210_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364113.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_mumu_mc16e_70_140_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364103.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364104.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364105.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_mumu_mc16e_140_280_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364106.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364107.Sherpa.DAOD_STDM4.e5271_s3126_r10201_r10210_p4097.126_mc16e_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364108.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_mumu_mc16e_280_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364109.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364110.Sherpa.DAOD_STDM4.e5271_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364111.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_mumu_mc16e_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364112.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10201_r10210_p4097.126_mc16e_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364113.Sherpa.DAOD_STDM4.e5271_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_0_70_LowMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364114.Sherpa.DAOD_STDM4.e5299_s3126_r10201_r10210_p4237.151\
_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364115.Sherpa.DAOD_STDM4.e5299_e5984_s3126_s3136_r10724_r107\
26_p4237.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364116.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364117.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364118.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364119.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364120.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364121.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364122.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364123.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p42\
37.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364124.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364125.Sherpa.DAOD_STDM4.e5299_e5984_s3126_s3136_r10724_r107\
26_p4237.151_Retry_output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364126.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4237.151_Retry_\
output_root/*root");
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364127.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4237.151_Retry_\
output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_70_140_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364117.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364118.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364119.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_140_280_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364120.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364121.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364122.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_280_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364123.Sherpa.DAOD_STDM4.e5299_e5984_s3126_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364124.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364125.Sherpa.DAOD_STDM4.e5299_e5984_s3126_s3136_r10724_r10726_p4097.126_mc16e_2_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_500_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.364126.Sherpa.DAOD_STDM4.e5299_s3126_r10724_p4097.126_mc16e_2_output_root/*root");
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.364127.Sherpa.DAOD_STDM4.e5299_s3126_r10201_p4097.126_mc16e_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16e_0_70_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366300.Sh.DAOD_EXOT0.e7410_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366301.Sh.DAOD_EXOT0.e7410_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.366302.Sh.DAOD_EXOT0.e7729_s3126_r10201_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16e_70_140_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366303.Sh.DAOD_EXOT0.e7410_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366304.Sh.DAOD_EXOT0.e7410_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366305.Sh.DAOD_EXOT0.e7729_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16e_140_280_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366306.Sh.DAOD_EXOT0.e7410_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366307.Sh.DAOD_EXOT0.e7729_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366308.Sh.DAOD_EXOT0.e7729_s3126_r10724_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_0_70_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366309.Sh.DAOD_EXOT0.e7411_a875_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366310.Sh.DAOD_EXOT0.e7411_a875_r10724_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_70_140_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366312.Sh.DAOD_EXOT0.e7411_a875_r10724_p4180.21_126_NoSys_output_root/*root");
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366313.Sh.DAOD_EXOT0.e7411_a875_r10724_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa mumu   
     if( nameOfSample == "Sherpa_ee_mc16e_140_280_HighMass" ){
	chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.366315.Sh.DAOD_EXOT0.e7411_a875_r10724_p4180.21_126_NoSys_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700102_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700102.Sh.DAOD_STDM3.e8118_s3126_r9364_p4252.Base_142_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700103_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700103.Sh.DAOD_STDM3.e8118_s3126_r9364_p4252.Base_142_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700104_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700104.Sh.DAOD_STDM3.e8118_s3126_r9364_p4252.Base_142_output_root/*root");
    } // end of if Sherpa ee   
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700105_LowMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700105.Sh.DAOD_STDM3.e8118_s3126_r9364_p4252.Base_142_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700125_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700125.Sh.DAOD_STDM3.e8118_s3126_r9364_r9315_p4434.189_Ret2_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700126_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700126.Sh.DAOD_STDM3.e8118_s3126_r9364_r9315_p4434.189_Ret2_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16a_2210_700127_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700127.Sh.DAOD_STDM3.e8118_s3126_r9364_r9315_p4252.189_Ret2_output_root/*root");
    } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16a_2210_700128_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700128.Sh.DAOD_STDM3.e8118_s3126_r9364_r9315_p4434.189_Ret2_output_root/*root");
    } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16a_2210_700129_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700129.Sh.DAOD_STDM3.e8118_s3126_r9364_r9315_p4434.189_Ret2_output_root/*root");
    } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16a_2210_700130_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700130.Sh.DAOD_STDM3.e8118_s3126_r9364_p4252.21_2_173_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16d_2210_700125_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700125.Sh.DAOD_STDM3.e8118_s3126_r10201_r10210_p4434.189_Ret2_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16d_2210_700126_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700126.Sh.DAOD_STDM3.e8118_s3126_r10201_r10210_p4434.189_Ret2_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16d_2210_700127_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700127.Sh.DAOD_STDM3.e8118_s3126_r10201_p4252.21_2_173_output_root/*root");
    } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16d_2210_700128_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700128.Sh.DAOD_STDM3.e8118_s3126_r10201_p4252.21_2_173_output_root/*root");
    } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16d_2210_700129_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700129.Sh.DAOD_STDM3.e8118_s3126_r10201_r10210_p4434.189_Ret2_output_root/*root");
    } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16d_2210_700130_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700130.Sh.DAOD_STDM3.e8118_s3126_r10201_r10210_p4434.189_Ret2_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16e_2210_700125_HighMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700125.Sh.DAOD_STDM3.e8118_s3126_r10724_r10726_p4434.189_Ret2_output_root/*root");
     } // end of if Sherpa ee
     if( nameOfSample == "Sherpa_ee_mc16e_2210_700126_HighMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700126.Sh.DAOD_STDM3.e8118_s3126_r10724_r10726_p4434.189_Ret2_output_root/*root");
     } // end of if Sherpa ee
     if( nameOfSample == "Sherpa_ee_mc16e_2210_700127_HighMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700127.Sh.DAOD_STDM3.e8118_s3126_r10724_r10726_p4434.189_Ret2_output_root/*root");
     } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16e_2210_700128_HighMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700128.Sh.DAOD_STDM3.e8118_s3126_r10724_r10726_p4434.189_Ret2_output_root/*root");
     } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16e_2210_700129_HighMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700129.Sh.DAOD_STDM3.e8118_s3126_r10724_r10726_p4434.189_Ret2_output_root/*root");
     } // end of Sherpa ee
     if( nameOfSample == "Sherpa_mumu_mc16e_2210_700130_HighMass" ){
       chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700130.Sh.DAOD_STDM3.e8118_s3126_r10724_r10726_p4434.189_Ret2_output_root/*root");
     } // end of if Sherpa ee
     if( nameOfSample == "Sherpa_ee_mc16e_2210_700028_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700028.Sh.DAOD_STDM3.e8118_s3126_r10724_p4252.142Truth_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16e_2210_700029_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700029.Sh.DAOD_STDM3.e8118_s3126_r10724_p4252.142Truth_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_ee_mc16e_2210_700029_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700029.Sh.DAOD_STDM3.e8118_s3126_r10724_p4252.142Truth_output_root/*root");
    } // end of if Sherpa ee 
     if( nameOfSample == "Sherpa_mumu_mc16e_2210_700028_HighMass" ){
        chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700028.Sh.DAOD_STDM3.e8118_s3126_r10724_p4252.142Truth_output_root/*root");
    } // end of if Sherpa ee 

    if(  nameOfSample == "Sherpa_mumu_mc16a_2211"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700455.Sh.DAOD_STDM3.e8351_e7400_s3126_s3136_r9364_r9315_p4434.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700456.Sh.DAOD_STDM3.e8351_s3126_r9364_p4252.189_Ret5_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700457.Sh.DAOD_STDM3.e8351_s3126_r9364_p4434.189_Ret4_output_root/*root");     
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_ee_mc16a_2211"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700452.Sh.DAOD_STDM3.e8351_e7400_s3126_r9364_r9315_p4434.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700453.Sh.DAOD_STDM3.e8351_e7400_s3126_r9364_r9315_p4252.189_Ret6_output_root//*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700454.Sh.DAOD_STDM3.e8351_s3126_r9364_p4434.189_Ret4_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_mumu_mc16d_2211"  ){        
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700455.Sh.DAOD_STDM3.e8351_e7400_s3126_r10201_r10210_p4434.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700456.Sh.DAOD_STDM3.e8351_e7400_s3126_r10201_r10210_p4252.189_Ret5_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700457.Sh.DAOD_STDM3.e8351_e7400_s3126_r10201_r10210_p4434.189_Ret4_output_root/*root");      
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_ee_mc16d_2211"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700452.Sh.DAOD_STDM3.e8351_e7400_s3126_r10201_r10210_p4434.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700453.Sh.DAOD_STDM3.e8351_e7400_s3126_r10201_r10210_p4252.189_Ret5_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700454.Sh.DAOD_STDM3.e8351_s3126_r10201_p4434.189_Ret4_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_mumu_mc16e_2211"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700455.Sh.DAOD_STDM3.e8351_e7400_s3126_s3136_r10724_r10726_p4434.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700456.Sh.DAOD_STDM3.e8351_e7400_s3126_s3136_r10724_r10726_p4252.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700457.Sh.DAOD_STDM3.e8351_e7400_s3126_s3136_r10724_r10726_p4434.189_Ret4_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_ee_mc16e_2211"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700452.Sh.DAOD_STDM3.e8351_s3126_r10724_p4434.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700453.Sh.DAOD_STDM3.e8351_e7400_s3126_s3136_r10724_r10726_p4252_tid27746940_00.189_Ret4_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700454.Sh.DAOD_STDM3.e8351_s3126_r10724_p4434.189_Ret4_output_root/*root");
    } // end of Sherpa 2211

    if(  nameOfSample == "Sherpa_mumu_mc16a_2210_New"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700323.Sh.DAOD_STDM3.e8351_e7400_s3126_r9364_r9315_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700324.Sh.DAOD_STDM3.e8351_s3126_r9364_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/.mcanobre.700325.Sh.DAOD_STDM3.e8351_s3126_r9364_p4252.189_Ret6_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_ee_mc16a_2210_New"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700320.Sh.DAOD_STDM3.e8351_s3126_r9364_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700321.Sh.DAOD_STDM3.e8351_s3126_r9364_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16a/DrellYan/user.mcanobre.700322.Sh.DAOD_STDM3.e8351_s3126_r9364_p4252.189_Ret6_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_mumu_mc16d_2210_New"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700323.Sh.DAOD_STDM3.e8351_s3126_r10201_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700324.Sh.DAOD_STDM3.e8351_s3126_r10201_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700325.Sh.DAOD_STDM3.e8351_s3126_r10201_r10210_p4252.189_Ret6_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_ee_mc16d_2210_New"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700320.Sh.DAOD_STDM3.e8351_s3126_r10201_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700321.Sh.DAOD_STDM3.e8351_s3126_r10201_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16d/DrellYan/user.mcanobre.700322.Sh.DAOD_STDM3.e8351_s3126_r10201_r10210_p4252.189_Ret6_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_mumu_mc16e_2210_New"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700323.Sh.DAOD_STDM3.e8351_s3126_r10724_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700324.Sh.DAOD_STDM3.e8351_s3126_r10724_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700325.Sh.DAOD_STDM3.e8351_s3126_r10724_p4252.189_Ret6_output_root/*root");
    } // end of Sherpa 2211
    if(  nameOfSample == "Sherpa_ee_mc16e_2210_New"  ){
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700320.Sh.DAOD_STDM3.e8351_s3126_r10724_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700321.Sh.DAOD_STDM3.e8351_s3126_r10724_p4252.189_Ret6_output_root/*root");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-sm/hmdy/MC/mc16e/DrellYan/user.mcanobre.700322.Sh.DAOD_STDM3.e8351_s3126_r10724_p4252.189_Ret6_output_root/*root");
    } // end of Sherpa 2211    

    tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
} // end of function

double Comparison::Normalisation( int mcChannelNumber, int runNumber, double MCWeight ){

   double Weight = 1.0;

if( runNumber == 284500.0 ){

if( mcChannelNumber == 301000.0 ) Weight =  17.477*1E+03/(2.61461e+07);
if( mcChannelNumber == 301001.0 ) Weight =  2.9215*1E+03/(2.62907e+06);
if( mcChannelNumber == 301002.0 ) Weight =  1.0819*1E+03/(432753);
if( mcChannelNumber == 301003.0 ) Weight =  0.19551*1E+03/(173999);
if( mcChannelNumber == 301004.0 ) Weight =  0.037403*1E+03/(748.075);
if( mcChannelNumber == 301005.0 ) Weight =  0.010607*1E+03/(2651.78);
if( mcChannelNumber == 301006.0 ) Weight =  0.0042586*1E+03/(425.84);
if( mcChannelNumber == 301007.0 ) Weight =  0.001422*1E+03/(71.0944);
if( mcChannelNumber == 301008.0 ) Weight =  0.00054526*1E+03/(27.2606);
if( mcChannelNumber == 301009.0 ) Weight =  0.00022993*1E+03/(22.9906);
if( mcChannelNumber == 301010.0 ) Weight =  0.00010386*1E+03/(5.19259);
if( mcChannelNumber == 301011.0 ) Weight =  0.000049403*1E+03/(2.46996);
if( mcChannelNumber == 301012.0 ) Weight =  0.000024454*1E+03/(1.22263);
if( mcChannelNumber == 301013.0 ) Weight =  0.00001249*1E+03/(0.624437);
if( mcChannelNumber == 301014.0 ) Weight =  0.000010031*1E+03/(0.501549);
if( mcChannelNumber == 301015.0 ) Weight =  0.0000029343*1E+03/(0.146709);
if( mcChannelNumber == 301016.0 ) Weight =  0.00000089767*1E+03/(0.0448815);
if( mcChannelNumber == 301017.0 ) Weight =  0.00000028071*1E+03/(0.014035);
if( mcChannelNumber == 301018.0 ) Weight =  0.00000012648*1E+03/(0.00189746);
if( mcChannelNumber == 301020.0 ) Weight =  17.477*1E+03/(3.49556e+07);
if( mcChannelNumber == 301021.0 ) Weight =  2.9215*1E+03/(2.62914e+06);
if( mcChannelNumber == 301022.0 ) Weight =  1.0819*1E+03/(1.45522e+06);
if( mcChannelNumber == 301023.0 ) Weight =  0.19551*1E+03/(195503);
if( mcChannelNumber == 301024.0 ) Weight =  0.037403*1E+03/(22440.7);
if( mcChannelNumber == 301025.0 ) Weight =  0.010607*1E+03/(3182.11);
if( mcChannelNumber == 301026.0 ) Weight =  0.0042586*1E+03/(638.779);
if( mcChannelNumber == 301027.0 ) Weight =  0.001422*1E+03/(142.189);
if( mcChannelNumber == 301028.0 ) Weight =  0.00054526*1E+03/(27.2606);
if( mcChannelNumber == 301029.0 ) Weight =  0.00022993*1E+03/(11.4952);
if( mcChannelNumber == 301030.0 ) Weight =  0.00010386*1E+03/(5.1926);
if( mcChannelNumber == 301031.0 ) Weight =  0.000049403*1E+03/(4.93991);
if( mcChannelNumber == 301032.0 ) Weight =  0.000024454*1E+03/(2.44526);
if( mcChannelNumber == 301033.0 ) Weight =  0.00001249*1E+03/(1.24887);
if( mcChannelNumber == 301034.0 ) Weight =  0.000010031*1E+03/(0.501532);
if( mcChannelNumber == 301035.0 ) Weight =  0.0000029343*1E+03/(0.146709);
if( mcChannelNumber == 301036.0 ) Weight =  0.00000089767*1E+03/(0.089763);
if( mcChannelNumber == 301037.0 ) Weight =  0.00000028071*1E+03/(0.02807);
if( mcChannelNumber == 301038.0 ) Weight =  0.00000012648*1E+03/(0.1);
if( mcChannelNumber == 364834.0 ) Weight =  0.52176*45.585*1E+03/(200000);
if( mcChannelNumber == 364835.0 ) Weight =  0.72212*4.144*1E+03/(200000);
if( mcChannelNumber == 364836.0 ) Weight =  0.81193*0.17591*1E+03/(200000);
if( mcChannelNumber == 364837.0 ) Weight =  0.85386*0.005382*1E+03/(200000);
if( mcChannelNumber == 364838.0 ) Weight =  0.87275*0.000127710*1E+03/(200000);
if( mcChannelNumber == 364839.0 ) Weight =  0.87788*0.000009444*1E+03/(199000);
if( mcChannelNumber == 364840.0 ) Weight =  0.87551*0.00000004797*1E+03/(0.1);
 if( mcChannelNumber == 364841.0 ) Weight =  0.53668*45.475*1E+03/(200000);
 if( mcChannelNumber == 364842.0 ) Weight =  0.73306*4.137*1E+03/(0.1);
 if( mcChannelNumber == 364843.0 ) Weight =  0.82016*0.1776*1E+03/(200000);
 if( mcChannelNumber == 364844.0 ) Weight =  0.85936*0.0053803*1E+03/(200000);
 if( mcChannelNumber == 364845.0 ) Weight =  0.8765*0.00012756*1E+03/(200000);
 if( mcChannelNumber == 364846.0 ) Weight =  0.88225*0.0000094349*1E+03/(200000);
 if( mcChannelNumber == 364847.0 ) Weight =  0.87872*0.000000047971*1E+03/(200000);
if( mcChannelNumber == 361100.0 ) Weight = 11.5E+06/(4.68247e+11);
if( mcChannelNumber == 361101.0 ) Weight = 11.5E+06/(4.51808e+11);
if( mcChannelNumber == 361102.0 ) Weight = 11.5E+06/(3.38976e+11);
if( mcChannelNumber == 361103.0 ) Weight = 8.283E+06/(4.13338e+11);
if( mcChannelNumber == 361104.0 ) Weight = 8.283E+06/(2.64817e+11);
if( mcChannelNumber == 361105.0 ) Weight = 8.283E+06/(1.65275e+11);
if( mcChannelNumber == 361106.0 ) Weight = 1.901E+06/(3.41799e+10);
if( mcChannelNumber == 361107.0 ) Weight = 1.901E+06/(1.06068e+11);
if( mcChannelNumber == 361108.0 ) Weight = 1.901E+06/(7.50837e+10);
if( mcChannelNumber == 410470.0 ) Weight = 0.54383*1.1398*729.9*1E+03/(8.72058e+10);
if( mcChannelNumber == 410471.0 ) Weight = 0.45627*1.1398*729.9*1E+03/(0.1);
if( mcChannelNumber == 410472.0 ) Weight = 0.10546*1.1398*729.9*1E+03/(5.81482e+10);
if( mcChannelNumber == 410644.0 ) Weight = 2.0267e+03/(4.05637e+06);
if( mcChannelNumber == 410645.0 ) Weight = 1.2674e+03/(2.53797e+06);
if( mcChannelNumber == 410646.0 ) Weight = 3.7936E+04/(3.79094e+08);
if( mcChannelNumber == 410647.0 ) Weight = 3.7905E+04/(3.79068e+08);
if( mcChannelNumber == 410658.0 ) Weight = 3.6996E+04/(9.16561e+08);
if( mcChannelNumber == 410659.0 ) Weight = 2.2175E+04/(5.48748e+08);
if( mcChannelNumber == 364250.0 ) Weight =   1252.3 /(7.51881e+06);
if( mcChannelNumber == 364253.0 ) Weight =   4579./(5.35645e+06);
if( mcChannelNumber == 364254.0 ) Weight =   12501./(5.1148e+06);
if( mcChannelNumber == 364255.0 ) Weight =   3234.4/(1.75046e+06);
if( mcChannelNumber == 363355.0 ) Weight =   0.27686*15560./(0.1);
if( mcChannelNumber == 363356.0 ) Weight =   0.14158*15564./(6.98404e+06);
if( mcChannelNumber == 363357.0 ) Weight =   6795./1/(0.1);
if( mcChannelNumber == 363358.0 ) Weight =   3432.8/(254404);
if( mcChannelNumber == 363359.0 ) Weight =   24708./(0.1);
if( mcChannelNumber == 363360.0 ) Weight =   24724./(1.08043e+06);

 if( mcChannelNumber == 364100.0 ) Weight = 1.9825E+06*8.2142E-01/(3.68713e+06);
 if( mcChannelNumber == 364101.0 ) Weight = 1.9822E+06*1.1319E-01/(2.87199e+06);
 if( mcChannelNumber == 364102.0 ) Weight = 1.9817E+06*6.5069E-02/(4.12099e+06);
 if( mcChannelNumber == 364103.0 ) Weight = 1.0914E+05*6.8992E-01/(2.16649e+06);
 if( mcChannelNumber == 364104.0 ) Weight = 1.0898E+05*1.9027E-01/(732466);
 if( mcChannelNumber == 364105.0 ) Weight = 1.0903E+05*1.1735E-01/(2.08226e+06);
 if( mcChannelNumber == 364106.0 ) Weight = 3.9870E+04*6.1125E-01/(2.97401e+06);
 if( mcChannelNumber == 364107.0 ) Weight = 3.9857E+04*2.3359E-01/(1.99109e+06);
 if( mcChannelNumber == 364108.0 ) Weight = 3.9888E+04*1.5570E-01/(7.51523e+06);
 if( mcChannelNumber == 364109.0 ) Weight = 8.5251E+03*5.6023E-01/(1.72841e+06);
 if( mcChannelNumber == 364110.0 ) Weight = 8.5259E+03*2.6577E-01/(918653);
 if( mcChannelNumber == 364111.0 ) Weight = 8.5296E+03*1.7458E-01/(3.74946e+06);
 if( mcChannelNumber == 364112.0 ) Weight = 1.7870E+03*1.0000E+00/(2.98051e+06);
 if( mcChannelNumber == 364113.0 ) Weight = 1.4761E+02*1.0000E+00/(1.01705e+06);
 if( mcChannelNumber == 364114.0 ) Weight = 1.9817E+06*8.2118E-01/(5.2599e+06);
 if( mcChannelNumber == 364115.0 ) Weight = 1.9816E+06*1.1347E-01/(2.87288e+06);
 if( mcChannelNumber == 364116.0 ) Weight = 1.9820E+06*6.5194E-02/(4.10509e+06);
 if( mcChannelNumber == 364117.0 ) Weight = 1.2064E+05*6.9275E-01/(2.17538e+06);
 if( mcChannelNumber == 364118.0 ) Weight = 1.1050E+05*1.8933E-01/(724682);
 if( mcChannelNumber == 364119.0 ) Weight = 1.1046E+05*1.1547E-01/(2.08382e+06);
 if( mcChannelNumber == 364120.0 ) Weight = 4.0645E+04*6.1125E-01/(2.99657e+06);
 if( mcChannelNumber == 364121.0 ) Weight = 4.0671E+04*2.3209E-01/(2.0014e+06);
 if( mcChannelNumber == 364122.0 ) Weight = 4.0675E+04*1.5238E-01/(8.62671e+06);
 if( mcChannelNumber == 364123.0 ) Weight = 8.6703E+03*5.6349E-01/(1.7234e+06);
 if( mcChannelNumber == 364124.0 ) Weight = 8.6668E+03*2.6531E-01/(918310);
 if( mcChannelNumber == 364125.0 ) Weight = 8.6799E+03*1.7578E-01/(1.87061e+06);
 if( mcChannelNumber == 364126.0 ) Weight = 1.8092E+03*1.0000E+00/(2.96847e+06);
 if( mcChannelNumber == 364127.0 ) Weight = 1.4875E+02*1.0000E+00/(1.01652e+06);

 if(   mcChannelNumber == 700452. ) Weight = 23.1e+3*0.024195/(1.66037e+10);
 if(   mcChannelNumber == 700453. ) Weight = 23.1e+3*1.3553E-01/(4.22516e+11);
 if(   mcChannelNumber == 700454. ) Weight = 23.1e+3*0.83987/(1.21352e+12);
 if(   mcChannelNumber == 700455. ) Weight = 23.1e+3*2.4072E-02/(1.65556e+10);
 if(   mcChannelNumber == 700456. ) Weight = 23.1e+3*1.3586E-01/(4.25301e+11);
 if(   mcChannelNumber == 700457. ) Weight = 23.1e+3*0.83988/(6.07545e+11);

 if(   mcChannelNumber == 700320. ) Weight = 2.22E+06*0.024945/(5.75717e+13);
 if(   mcChannelNumber == 700321. ) Weight = 2.22E+06*0.1290/(2.18168e+14);
 if(   mcChannelNumber == 700322. ) Weight = 2.22E+06*0.84607/(4.26435e+14);
 if(   mcChannelNumber == 700323. ) Weight = 2.22E+06*0.024427/(5.66396e+13);
 if(   mcChannelNumber == 700324. ) Weight = 2.22E+06*0.12937/(2.18557e+14);
 if(   mcChannelNumber == 700325. ) Weight = 2.22E+06*0.84635/(4.28376e+14);

if( nameOfSample == "Sherpa_ee_mc16a_2210_700102_LowMass" ) Weight = 2.14E+06/(9.94671e+13);
if( nameOfSample == "Sherpa_ee_mc16a_2210_700103_LowMass" ) Weight = 2.15E+06*0.84858/(2.74732e+13);
if( nameOfSample == "Sherpa_ee_mc16a_2210_700104_LowMass" ) Weight = 2.16E+06*0.15128/(2.10342e+13);
if( nameOfSample == "Sherpa_ee_mc16a_2210_700105_LowMass" ) Weight = 0.045E+06/(6.9251e+10);
if( nameOfSample == "Sherpa_ee_mc16a_2210_700125_HighMass" ) Weight = 0.975*2.14E+06*0.024195/(8.88541e+12);
if( nameOfSample == "Sherpa_ee_mc16a_2210_700126_HighMass" ) Weight = 0.975*2.14E+06*0.1266/(2.27364e+13);
if( nameOfSample == "Sherpa_ee_mc16a_2210_700127_HighMass" ) Weight = 0.975*2.14E+06*0.84902/(3.25148e+14); //3.57506e+14
if( nameOfSample == "Sherpa_mumu_mc16a_2210_700128_HighMass" ) Weight = 0.975*2.14E+06*0.023631/(2.6275e+12);
if( nameOfSample == "Sherpa_mumu_mc16a_2210_700129_HighMass" ) Weight = 0.975*2.14E+06*0.12695/(2.95438e+12);
if( nameOfSample == "Sherpa_mumu_mc16a_2210_700130_HighMass" ) Weight = 0.975*2.14E+06*0.84946/(2.52275e+14);

} // end of if run == 284500

if( runNumber == 300000.0 ){

if( mcChannelNumber == 301000.0 ) Weight =  17.477*1E+03/(2.82229e+07);
if( mcChannelNumber == 301001.0 ) Weight =  2.9215*1E+03/(1.71497e+06);
if( mcChannelNumber == 301002.0 ) Weight =  1.0819*1E+03/(1.3416e+06);
if( mcChannelNumber == 301003.0 ) Weight =  0.19551*1E+03/(180840);
if( mcChannelNumber == 301004.0 ) Weight =  0.037403*1E+03/(20047.9);
if( mcChannelNumber == 301005.0 ) Weight =  0.010607*1E+03/(2779.07);
if( mcChannelNumber == 301006.0 ) Weight =  0.0042586*1E+03/(476.954);
if( mcChannelNumber == 301007.0 ) Weight =  0.001422*1E+03/(88.1567);
if( mcChannelNumber == 301008.0 ) Weight =  0.00054526*1E+03/(33.803);
if( mcChannelNumber == 301009.0 ) Weight =  0.00022993*1E+03/(28.7425);
if( mcChannelNumber == 301010.0 ) Weight =  0.00010386*1E+03/(6.43877);
if( mcChannelNumber == 301011.0 ) Weight =  0.000049403*1E+03/(3.06273);
if( mcChannelNumber == 301012.0 ) Weight =  0.000024454*1E+03/(1.51605);
if( mcChannelNumber == 301013.0 ) Weight =  0.00001249*1E+03/(0.774294);
if( mcChannelNumber == 301014.0 ) Weight =  0.000010031*1E+03/(0.501535);
if( mcChannelNumber == 301015.0 ) Weight =  0.0000029343*1E+03/(0.164317);
if( mcChannelNumber == 301016.0 ) Weight =  0.00000089767*1E+03/(0.0107703);
if( mcChannelNumber == 301017.0 ) Weight =  0.00000028071*1E+03/(0.0171225);
if( mcChannelNumber == 301018.0 ) Weight =  0.00000012648*1E+03/(0.00784263);
if( mcChannelNumber == 301020.0 ) Weight =  17.477*1E+03/(8.73713e+06);
if( mcChannelNumber == 301021.0 ) Weight =  2.9215*1E+03/(1.89308e+06);
if( mcChannelNumber == 301022.0 ) Weight =  1.0819*1E+03/(1.29291e+06);
if( mcChannelNumber == 301023.0 ) Weight =  0.19551*1E+03/(175952);
if( mcChannelNumber == 301024.0 ) Weight =  0.037403*1E+03/(18514.3);
if( mcChannelNumber == 301025.0 ) Weight =  0.010607*1E+03/(2651.82);
if( mcChannelNumber == 301026.0 ) Weight =  0.0042586*1E+03/(425.846);
if( mcChannelNumber == 301027.0 ) Weight =  0.001422*1E+03/(71.0955);
if( mcChannelNumber == 301028.0 ) Weight =  0.00054526*1E+03/(27.261);
if( mcChannelNumber == 301029.0 ) Weight =  0.00022993*1E+03/(11.4954);
if( mcChannelNumber == 301030.0 ) Weight =  0.00010386*1E+03/(5.19266);
if( mcChannelNumber == 301031.0 ) Weight =  0.000049403*1E+03/(2.46999);
if( mcChannelNumber == 301032.0 ) Weight =  0.000024454*1E+03/(1.22265);
if( mcChannelNumber == 301033.0 ) Weight =  0.00001249*1E+03/(0.624444);
if( mcChannelNumber == 301034.0 ) Weight =  0.000010031*1E+03/(0.501535);
if( mcChannelNumber == 301035.0 ) Weight =  0.0000029343*1E+03/(0.14671);
if( mcChannelNumber == 301036.0 ) Weight =  0.00000089767*1E+03/(0.1);
if( mcChannelNumber == 301037.0 ) Weight =  0.00000028071*1E+03/(0.0140351);
if( mcChannelNumber == 301038.0 ) Weight =  0.00000012648*1E+03/(0.00632481);
if( mcChannelNumber == 364834.0 ) Weight =  0.52176*45.585*1E+03/(250000);
if( mcChannelNumber == 364835.0 ) Weight =  0.72212*4.144*1E+03/(250000);
if( mcChannelNumber == 364836.0 ) Weight =  0.81193*0.17591*1E+03/(250000);
if( mcChannelNumber == 364837.0 ) Weight =  0.85386*0.005382*1E+03/(250000);
if( mcChannelNumber == 364838.0 ) Weight =  0.87275*0.000127710*1E+03/(249000);
if( mcChannelNumber == 364839.0 ) Weight =  0.87788*0.000009444*1E+03/(250000);
if( mcChannelNumber == 364840.0 ) Weight =  0.87551*0.00000004797*1E+03/(249000);
 if( mcChannelNumber == 364841.0 ) Weight =  0.53668*45.475*1E+03/(250000);
 if( mcChannelNumber == 364842.0 ) Weight =  0.73306*4.137*1E+03/(250000);
 if( mcChannelNumber == 364843.0 ) Weight =  0.82016*0.1776*1E+03/(250000);
 if( mcChannelNumber == 364844.0 ) Weight =  0.85936*0.0053803*1E+03/(250000);
 if( mcChannelNumber == 364845.0 ) Weight =  0.8765*0.00012756*1E+03/(250000);
 if( mcChannelNumber == 364846.0 ) Weight =  0.88225*0.0000094349*1E+03/(339000);
 if( mcChannelNumber == 364847.0 ) Weight =  0.87872*0.000000047971*1E+03/(250000);
if( mcChannelNumber == 361100.0 ) Weight = 11.5E+06/(3.86289e+11);
if( mcChannelNumber == 361101.0 ) Weight = 11.5E+06/(1.12927e+11);
if( mcChannelNumber == 361102.0 ) Weight = 11.5E+06/(8.4667e+10);
if( mcChannelNumber == 361103.0 ) Weight = 8.283E+06/(5.00865e+11);
if( mcChannelNumber == 361104.0 ) Weight = 8.283E+06/(2.64877e+11);
if( mcChannelNumber == 361105.0 ) Weight = 8.283E+06/(4.13612e+10);
if( mcChannelNumber == 361106.0 ) Weight = 1.901E+06/(4.02401e+10);
if( mcChannelNumber == 361107.0 ) Weight = 1.901E+06/(9.35282e+10);
if( mcChannelNumber == 361108.0 ) Weight = 1.901E+06/(7.05515e+10);
if( mcChannelNumber == 410470.0 ) Weight = 0.54383*1.1398*729.9*1E+03/(9.81207e+10);
if( mcChannelNumber == 410471.0 ) Weight = 0.45627*1.1398*729.9*1E+03/(0.1);
if( mcChannelNumber == 410472.0 ) Weight = 0.10546*1.1398*729.9*1E+03/(7.24757e+10);
if( mcChannelNumber == 410644.0 ) Weight = 2.0267e+03/(5.0656e+06);
if( mcChannelNumber == 410645.0 ) Weight = 1.2674e+03/(3.17182e+06);
if( mcChannelNumber == 410646.0 ) Weight = 3.7936E+04/(4.73547e+08);
if( mcChannelNumber == 410647.0 ) Weight = 3.7905E+04/(4.7245e+08);
if( mcChannelNumber == 410658.0 ) Weight = 3.6996E+04/(1.15403e+09);
if( mcChannelNumber == 410659.0 ) Weight = 2.2175E+04/(6.86026e+08);
if( mcChannelNumber == 364250.0 ) Weight =   1252.3 /(1.51851e+07);
if( mcChannelNumber == 364253.0 ) Weight =   4579./(1.08423e+07);
if( mcChannelNumber == 364254.0 ) Weight =   12501./(1.0238e+07);
if( mcChannelNumber == 364255.0 ) Weight =   3234.4/(3.50595e+06);
if( mcChannelNumber == 363355.0 ) Weight =   0.27686*15560./(3.49036e+06);
if( mcChannelNumber == 363356.0 ) Weight =   0.14158*15564./(3.49307e+06);
if( mcChannelNumber == 363357.0 ) Weight =   6795./1/(555940);
if( mcChannelNumber == 363358.0 ) Weight =   3432.8/(1.09715e+06);
if( mcChannelNumber == 363359.0 ) Weight =   24708./(1.07583e+06);
if( mcChannelNumber == 363360.0 ) Weight =   24724./(2.12909e+06);

 if( mcChannelNumber == 364100.0 ) Weight = 1.9825E+06*8.2142E-01/(6.69374e+06);
 if( mcChannelNumber == 364101.0 ) Weight = 1.9822E+06*1.1319E-01/(2.85947e+06);
 if( mcChannelNumber == 364102.0 ) Weight = 1.9817E+06*6.5069E-02/(4.931e+06);
 if( mcChannelNumber == 364103.0 ) Weight = 1.0914E+05*6.8992E-01/(2.53917e+06);
 if( mcChannelNumber == 364104.0 ) Weight = 1.0898E+05*1.9027E-01/(902414);
 if( mcChannelNumber == 364105.0 ) Weight = 1.0903E+05*1.1735E-01/(517321);
 if( mcChannelNumber == 364106.0 ) Weight = 3.9870E+04*6.1125E-01/(3.71602e+06);
 if( mcChannelNumber == 364107.0 ) Weight = 3.9857E+04*2.3359E-01/(2.49178e+06);
 if( mcChannelNumber == 364108.0 ) Weight = 3.9888E+04*1.5570E-01/(3.46372e+06);
 if( mcChannelNumber == 364109.0 ) Weight = 8.5251E+03*5.6023E-01/(1.68615e+06);
 if( mcChannelNumber == 364110.0 ) Weight = 8.5259E+03*2.6577E-01/(0.1);
 if( mcChannelNumber == 364111.0 ) Weight = 8.5296E+03*1.7458E-01/(4.68323e+06);
 if( mcChannelNumber == 364112.0 ) Weight = 1.7870E+03*1.0000E+00/(744568);
 if( mcChannelNumber == 364113.0 ) Weight = 1.4761E+02*1.0000E+00/(1.27407e+06);
 if( mcChannelNumber == 364114.0 ) Weight = 1.9817E+06*8.2118E-01/(6.71901e+06);
 if( mcChannelNumber == 364115.0 ) Weight = 1.9816E+06*1.1347E-01/(3.5837e+06);
 if( mcChannelNumber == 364116.0 ) Weight = 1.9820E+06*6.5194E-02/(5.1392e+06);
 if( mcChannelNumber == 364117.0 ) Weight = 1.2064E+05*6.9275E-01/(2.70266e+06);
 if( mcChannelNumber == 364118.0 ) Weight = 1.1050E+05*1.8933E-01/(907295);
 if( mcChannelNumber == 364119.0 ) Weight = 1.1046E+05*1.1547E-01/(1.22538e+06);
 if( mcChannelNumber == 364120.0 ) Weight = 4.0645E+04*6.1125E-01/(3.74416e+06);
 if( mcChannelNumber == 364121.0 ) Weight = 4.0671E+04*2.3209E-01/(2.49978e+06);
 if( mcChannelNumber == 364122.0 ) Weight = 4.0675E+04*1.5238E-01/(8.48747e+06);
 if( mcChannelNumber == 364123.0 ) Weight = 8.6703E+03*5.6349E-01/(2.13549e+06);
 if( mcChannelNumber == 364124.0 ) Weight = 8.6668E+03*2.6531E-01/(1.14707e+06);
 if( mcChannelNumber == 364125.0 ) Weight = 8.6799E+03*1.7578E-01/(4.68178e+06);
 if( mcChannelNumber == 364126.0 ) Weight = 1.8092E+03*1.0000E+00/(3.67771e+06);
 if( mcChannelNumber == 364127.0 ) Weight = 1.4875E+02*1.0000E+00/(1.27033e+06);

 if(   mcChannelNumber == 700452. ) Weight = 23.1e+3*0.024195/(2.1377e+10);
 if(   mcChannelNumber == 700453. ) Weight = 23.1e+3*1.3553E-01/(2.54694e+11);
 if(   mcChannelNumber == 700454. ) Weight = 23.1e+3*0.83987/(7.58684e+11);
 if(   mcChannelNumber == 700455. ) Weight = 23.1e+3*2.4072E-02/(2.1073e+10);
 if(   mcChannelNumber == 700456. ) Weight = 23.1e+3*1.3586E-01/(5.30789e+11);
 if(   mcChannelNumber == 700457. ) Weight = 23.1e+3*0.83988/(7.60419e+11);

 if(   mcChannelNumber == 700320. ) Weight = 2.22E+06*0.024945/(6.67517e+13);
 if(   mcChannelNumber == 700321. ) Weight = 2.22E+06*0.1290/(2.80908e+14);
 if(   mcChannelNumber == 700322. ) Weight = 2.22E+06*0.84607/(5.34929e+14);
 if(   mcChannelNumber == 700323. ) Weight = 2.22E+06*0.024427/(7.31814e+13);
 if(   mcChannelNumber == 700324. ) Weight = 2.22E+06*0.12937/(2.81145e+14);
 if(   mcChannelNumber == 700325. ) Weight = 2.22E+06*0.84635/(5.35332e+14);

if( nameOfSample == "Sherpa_ee_mc16d_2210_700125_HighMass" ) Weight = 0.975*2.14E+06*0.024195/(1.1145e+13);
if( nameOfSample == "Sherpa_ee_mc16d_2210_700126_HighMass" ) Weight = 0.975*2.14E+06*0.1266/(2.84749e+13);
if( nameOfSample == "Sherpa_ee_mc16d_2210_700127_HighMass" ) Weight = 0.975*2.14E+06*0.84902/(4.43989e+14);
if( nameOfSample == "Sherpa_mumu_mc16d_2210_700128_HighMass" ) Weight = 0.975*2.14E+06*0.023631/(7.71021e+12);
if( nameOfSample == "Sherpa_mumu_mc16d_2210_700129_HighMass" ) Weight = 0.975*2.14E+06*0.12695/(1.99983e+13);
 if( nameOfSample == "Sherpa_mumu_mc16d_2210_700130_HighMass" ) Weight = 0.975*2.14E+06*0.84946/(3.14842e+14);

   } // end of if run == 300000
   if( runNumber == 310000.0 ){

if( mcChannelNumber == 301000.0 ) Weight =  17.477*1E+03/(3.86588e+07);
if( mcChannelNumber == 301001.0 ) Weight =  2.9215*1E+03/(4.38234e+06);
if( mcChannelNumber == 301002.0 ) Weight =  1.0819*1E+03/(2.16177e+06);
if( mcChannelNumber == 301003.0 ) Weight =  0.19551*1E+03/(118296);
if( mcChannelNumber == 301004.0 ) Weight =  0.037403*1E+03/(31794.8);
if( mcChannelNumber == 301005.0 ) Weight =  0.010607*1E+03/(4454.72);
if( mcChannelNumber == 301006.0 ) Weight =  0.0042586*1E+03/(425.928);
if( mcChannelNumber == 301007.0 ) Weight =  0.001422*1E+03/(127.97);
if( mcChannelNumber == 301008.0 ) Weight =  0.00054526*1E+03/(49.0692);
if( mcChannelNumber == 301009.0 ) Weight =  0.00022993*1E+03/(39.0897);
if( mcChannelNumber == 301010.0 ) Weight =  0.00010386*1E+03/(9.34725);
if( mcChannelNumber == 301011.0 ) Weight =  0.000049403*1E+03/(4.44619);
if( mcChannelNumber == 301012.0 ) Weight =  0.000024454*1E+03/(2.20086);
if( mcChannelNumber == 301013.0 ) Weight =  0.00001249*1E+03/(0.1);
if( mcChannelNumber == 301014.0 ) Weight =  0.000010031*1E+03/(0.902775);
if( mcChannelNumber == 301015.0 ) Weight =  0.0000029343*1E+03/(0.264082);
if( mcChannelNumber == 301016.0 ) Weight =  0.00000089767*1E+03/(0.0807883);
if( mcChannelNumber == 301017.0 ) Weight =  0.00000028071*1E+03/(0.0252636);
if( mcChannelNumber == 301018.0 ) Weight =  0.00000012648*1E+03/(0.0113824);

 if( mcChannelNumber == 301020.0 ) Weight =  17.477*1E+03/(5.79876e+07);
 if( mcChannelNumber == 301021.0 ) Weight =  2.9215*1E+03/(5.54784e+06);
 if( mcChannelNumber == 301022.0 ) Weight =  1.0819*1E+03/(605910);
 if( mcChannelNumber == 301023.0 ) Weight =  0.19551*1E+03/(324553);
 if( mcChannelNumber == 301024.0 ) Weight =  0.037403*1E+03/(37405.2);
 if( mcChannelNumber == 301025.0 ) Weight =  0.010607*1E+03/(5250.28);
 if( mcChannelNumber == 301026.0 ) Weight =  0.0042586*1E+03/(1064.71);
 if( mcChannelNumber == 301027.0 ) Weight =  0.001422*1E+03/(227.52);
 if( mcChannelNumber == 301028.0 ) Weight =  0.00054526*1E+03/(92.6935);
 if( mcChannelNumber == 301029.0 ) Weight =  0.00022993*1E+03/(39.0868);
 if( mcChannelNumber == 301030.0 ) Weight =  0.00010386*1E+03/(17.6566);
 if( mcChannelNumber == 301031.0 ) Weight =  0.000049403*1E+03/(8.39866);
 if( mcChannelNumber == 301032.0 ) Weight =  0.000024454*1E+03/(4.15733);
 if( mcChannelNumber == 301033.0 ) Weight =  0.00001249*1E+03/(2.12328);
 if( mcChannelNumber == 301034.0 ) Weight =  0.000010031*1E+03/(0.1);
 if( mcChannelNumber == 301035.0 ) Weight =  0.0000029343*1E+03/(0.1);
 if( mcChannelNumber == 301036.0 ) Weight =  0.00000089767*1E+03/(0.152608);
 if( mcChannelNumber == 301037.0 ) Weight =  0.00000028071*1E+03/(0.1);
 if( mcChannelNumber == 301038.0 ) Weight =  0.00000012648*1E+03/(0.1);
if( mcChannelNumber == 364834.0 ) Weight =  0.52176*45.585*1E+03/(340000);
if( mcChannelNumber == 364835.0 ) Weight =  0.72212*4.144*1E+03/(340000);
if( mcChannelNumber == 364836.0 ) Weight =  0.81193*0.17591*1E+03/(250000);
if( mcChannelNumber == 364837.0 ) Weight =  0.85386*0.005382*1E+03/(340000);
if( mcChannelNumber == 364838.0 ) Weight =  0.87275*0.000127710*1E+03/(340000);
if( mcChannelNumber == 364839.0 ) Weight =  0.87788*0.000009444*1E+03/(336000);
if( mcChannelNumber == 364840.0 ) Weight =  0.87551*0.00000004797*1E+03/(338000);
 if( mcChannelNumber == 364841.0 ) Weight =  0.53668*45.475*1E+03/(340000);
 if( mcChannelNumber == 364842.0 ) Weight =  0.73306*4.137*1E+03/(338000);
 if( mcChannelNumber == 364843.0 ) Weight =  0.82016*0.1776*1E+03/(340000);
 if( mcChannelNumber == 364844.0 ) Weight =  0.85936*0.0053803*1E+03/(340000);
 if( mcChannelNumber == 364845.0 ) Weight =  0.8765*0.00012756*1E+03/(340000);
 if( mcChannelNumber == 364846.0 ) Weight =  0.88225*0.0000094349*1E+03/(339000);
 if( mcChannelNumber == 364847.0 ) Weight =  0.87872*0.000000047971*1E+03/(340000);
if( mcChannelNumber == 361100.0 ) Weight = 11.5E+06/(6.39087e+11);
if( mcChannelNumber == 361101.0 ) Weight = 11.5E+06/(2.11998e+11);
if( mcChannelNumber == 361102.0 ) Weight = 11.5E+06/(5.59662e+11);
if( mcChannelNumber == 361103.0 ) Weight = 8.283E+06/(6.82237e+11);
if( mcChannelNumber == 361104.0 ) Weight = 8.283E+06/(4.39486e+11);
if( mcChannelNumber == 361105.0 ) Weight = 8.283E+06/(1.09019e+11);
if( mcChannelNumber == 361106.0 ) Weight = 1.901E+06/(2.70511e+10);
if( mcChannelNumber == 361107.0 ) Weight = 1.901E+06/(1.07989e+11);
if( mcChannelNumber == 361108.0 ) Weight = 1.901E+06/(4.89112e+10);
if( mcChannelNumber == 410470.0 ) Weight = 0.54383*1.1398*729.9*1E+03/(1.45369e+11);
if( mcChannelNumber == 410471.0 ) Weight = 0.45627*1.1398*729.9*1E+03/(0.1);
if( mcChannelNumber == 410472.0 ) Weight = 0.10546*1.1398*729.9*1E+03/(1.01641e+11);
if( mcChannelNumber == 410644.0 ) Weight = 2.0267e+03/(6.69946e+06);
if( mcChannelNumber == 410645.0 ) Weight = 1.2674e+03/(4.20759e+06);
if( mcChannelNumber == 410646.0 ) Weight = 3.7936E+04/(4.85628e+08);
if( mcChannelNumber == 410647.0 ) Weight = 3.7905E+04/(6.27923e+08);
if( mcChannelNumber == 410658.0 ) Weight = 3.6996E+04/(1.54136e+09);
if( mcChannelNumber == 410659.0 ) Weight = 2.2175E+04/(7.34985e+08);
if( mcChannelNumber == 364250.0 ) Weight =   1252.3 /(1.08084e+07);
if( mcChannelNumber == 364253.0 ) Weight =   4579./(9.2335e+06);
if( mcChannelNumber == 364254.0 ) Weight =   12501./(8.41172e+06);
if( mcChannelNumber == 364255.0 ) Weight =   3234.4/(2.62332e+06);
if( mcChannelNumber == 363355.0 ) Weight =   0.27686*15560./(0.1);
if( mcChannelNumber == 363356.0 ) Weight =   0.14158*15564./(5.7981e+06);
if( mcChannelNumber == 363357.0 ) Weight =   6795./1/(0.1);
if( mcChannelNumber == 363358.0 ) Weight =   3432.8/(421819);
if( mcChannelNumber == 363359.0 ) Weight =   24708./(1.0473e+06);
if( mcChannelNumber == 363360.0 ) Weight =   24724./(1.78713e+06);

 if( mcChannelNumber == 364100.0 ) Weight = 1.9825E+06*8.2142E-01/(8.84177e+06);
 if( mcChannelNumber == 364101.0 ) Weight = 1.9822E+06*1.1319E-01/(3.83409e+06);
 if( mcChannelNumber == 364102.0 ) Weight = 1.9817E+06*6.5069E-02/(6.7108e+06);
 if( mcChannelNumber == 364103.0 ) Weight = 1.0914E+05*6.8992E-01/(3.61556e+06);
 if( mcChannelNumber == 364104.0 ) Weight = 1.0898E+05*1.9027E-01/(295394);
 if( mcChannelNumber == 364105.0 ) Weight = 1.0903E+05*1.1735E-01/(3.46726e+06);
 if( mcChannelNumber == 364106.0 ) Weight = 3.9870E+04*6.1125E-01/(0.1);
 if( mcChannelNumber == 364107.0 ) Weight = 3.9857E+04*2.3359E-01/(0.1);
 if( mcChannelNumber == 364108.0 ) Weight = 3.9888E+04*1.5570E-01/(1.43671e+07);
 if( mcChannelNumber == 364109.0 ) Weight = 8.5251E+03*5.6023E-01/(2.86554e+06);
 if( mcChannelNumber == 364110.0 ) Weight = 8.5259E+03*2.6577E-01/(1.53185e+06);
 if( mcChannelNumber == 364111.0 ) Weight = 8.5296E+03*1.7458E-01/(3.8614e+06);
 if( mcChannelNumber == 364112.0 ) Weight = 1.7870E+03*1.0000E+00/(0.1);
 if( mcChannelNumber == 364113.0 ) Weight = 1.4761E+02*1.0000E+00/(428685);
 if( mcChannelNumber == 364114.0 ) Weight = 1.9817E+06*8.2118E-01/(0.1);
 if( mcChannelNumber == 364115.0 ) Weight = 1.9816E+06*1.1347E-01/(3.57173e+06);
 if( mcChannelNumber == 364116.0 ) Weight = 1.9820E+06*6.5194E-02/(6.83013e+06);
 if( mcChannelNumber == 364117.0 ) Weight = 1.2064E+05*6.9275E-01/(898448);
 if( mcChannelNumber == 364118.0 ) Weight = 1.1050E+05*1.8933E-01/(300455);
 if( mcChannelNumber == 364119.0 ) Weight = 1.1046E+05*1.1547E-01/(3.47115e+06);
 if( mcChannelNumber == 364120.0 ) Weight = 4.0645E+04*6.1125E-01/(5.01136e+06);
 if( mcChannelNumber == 364121.0 ) Weight = 4.0671E+04*2.3209E-01/(2.79074e+06);
 if( mcChannelNumber == 364122.0 ) Weight = 4.0675E+04*1.5238E-01/(1.43178e+06);
 if( mcChannelNumber == 364123.0 ) Weight = 8.6703E+03*5.6349E-01/(2.95158e+06);
 if( mcChannelNumber == 364124.0 ) Weight = 8.6668E+03*2.6531E-01/(1.59351e+06);
 if( mcChannelNumber == 364125.0 ) Weight = 8.6799E+03*1.7578E-01/(2.34331e+06);
 if( mcChannelNumber == 364126.0 ) Weight = 1.8092E+03*1.0000E+00/(4.93026e+06);

if( mcChannelNumber == 410465.0 ) Weight = 0.10717*712.02*1.1398*1E+03/(8.87614e+10);
if( mcChannelNumber == 410482.0 ) Weight = 0.10546*729.74*1.1398*1E+03/(6.79772e+10);
if( mcChannelNumber == 410558.0 ) Weight = 0.10537*730.15*1.1398*1E+03/(8.40355e+10);
if( mcChannelNumber == 411054.0 ) Weight = 77.946*1.1398*1E+03/(7.36546e+09);
if( mcChannelNumber == 411057.0 ) Weight = 75.856*1.1398*1E+03/(7.43724e+09);

 if(   mcChannelNumber == 700452. ) Weight = 23.1e+3*0.024195/(2.65008e+10);
 if(   mcChannelNumber == 700453. ) Weight = 23.1e+3*1.3553E-01/(3.83538e+11);
 if(   mcChannelNumber == 700454. ) Weight = 23.1e+3*0.83987/(1.01495e+12);
 if(   mcChannelNumber == 700455. ) Weight = 23.1e+3*2.4072E-02/(2.66037e+10);
 if(   mcChannelNumber == 700456. ) Weight = 23.1e+3*1.3586E-01/(7.09818e+11);
 if(   mcChannelNumber == 700457. ) Weight = 23.1e+3*0.83988/(1.01667e+12);

 if(   mcChannelNumber == 700320. ) Weight = 2.22E+06*0.024945/(9.78265e+13);
 if(   mcChannelNumber == 700321. ) Weight = 2.22E+06*0.1290/(3.51089e+14);
 if(   mcChannelNumber == 700322. ) Weight = 2.22E+06*0.84607/(7.14578e+14);
 if(   mcChannelNumber == 700323. ) Weight = 2.22E+06*0.024427/(9.61361e+13);
 if(   mcChannelNumber == 700324. ) Weight = 2.22E+06*0.12937/(3.69063e+14);
 if(   mcChannelNumber == 700325. ) Weight = 2.22E+06*0.84635/(7.14958e+14);

if( nameOfSample == "TopQuark_mc16e_Sys" )  0.10546*1.1398*729.9*1E+03/(5.70209e+10);

if( nameOfSample == "Sherpa_ee_mc16e_2210_700125_HighMass" ) Weight = 0.975*2.14E+06*0.024195/(5.31262e+12);
if( nameOfSample == "Sherpa_ee_mc16e_2210_700126_HighMass" ) Weight = 0.975*2.14E+06*0.1266/(1.8602e+13);
if( nameOfSample == "Sherpa_ee_mc16e_2210_700127_HighMass" ) Weight = 0.975*2.14E+06*0.84902/(3.80235e+14);
if( nameOfSample == "Sherpa_mumu_mc16e_2210_700128_HighMass" ) Weight = 0.975*2.14E+06*0.023631/(4.16976e+12);
if( nameOfSample == "Sherpa_mumu_mc16e_2210_700129_HighMass" ) Weight = 0.975*2.14E+06*0.12695/(2.65235e+13);
if( nameOfSample == "Sherpa_mumu_mc16e_2210_700130_HighMass" ) Weight = 0.975*2.14E+06*0.84946/(4.14943e+14);
if( nameOfSample == "Sherpa_ee_mc16e_2210_700029_HighMass" ) Weight = 0.975*2.3084E+06/(1.26402e+14);
if( nameOfSample == "Sherpa_mumu_mc16e_2210_700028_HighMass" ) Weight = 0.975*2.2946E+06/(1.05683e+14);


    } // end of if run == 310000

    return Weight;
} // end of function 

void Comparison :: SetTreeBranches(){

  hmdyTree->Branch( "Weight_Lepton1_SF" , &Weight_Lepton1_SF );
  hmdyTree->Branch( "Weight_Lepton2_SF" , &Weight_Lepton2_SF );
  hmdyTree->Branch( "RunNumber" , &RunNumber );
  hmdyTree->Branch( "EventNumber" , &EventNumber );
  hmdyTree->Branch( "mcChannelNumber" , &MCChannelNumber );
  hmdyTree->Branch( "RecoDilRapidity"         ,  &RecoDilRapidity         );
  hmdyTree->Branch( "RecoDilPhi"         ,  &RecoDilPhi         );
  hmdyTree->Branch( "RecoDilMass"         ,  &RecoDilMass         );
  hmdyTree->Branch( "RecoPseudoDilMass"         ,  &RecoPseudoDilMass         );
  hmdyTree->Branch( "RecoDilPt"         ,  &RecoDilPt         );
  hmdyTree->Branch( "RecoPhiStar"         ,  &RecoPhiStar         );
  hmdyTree->Branch( "RecoCosThetaStar"         ,  &RecoCosThetaStar         );
  hmdyTree->Branch( "RecoWeight"         ,  &RecoWeight         );
  hmdyTree->Branch( "eeChannel"         ,  &eeChannel         );
  hmdyTree->Branch( "mumuChannel"         ,  &mumuChannel         );
  hmdyTree->Branch( "emuChannel"         ,  &emuChannel         );
  //hmdyTree->Branch( "FwdeeChannel"         ,  &FwdeeChannel         );
  hmdyTree->Branch( "TruthDilMass_Born"         ,  &TruthDilMass_Born         );
  hmdyTree->Branch( "TruthDilRapidity_Born"         ,  &TruthDilRapidity_Born         );	

  if( nameOfSample == "TopQuark_mc16a" || nameOfSample == "singleTop_mc16a" || nameOfSample == "TopQuark_mc16d" || nameOfSample == "singleTop_mc16d" || nameOfSample == "TopQuark_mc16e" || nameOfSample == "singleTop_mc16e" || nameOfSample == "Diboson_mc16e" || nameOfSample == "Diboson_mc16d" || nameOfSample == "Diboson_mc16a" || nameOfSample == "Ztautau_mc16e" || nameOfSample == "Ztautau_mc16d" || nameOfSample == "Ztautau_mc16a" ){

    hmdyTree->Branch( "Weight_TopQuarkError_DibosonSubtraction_UP"         ,  &Weight_TopQuarkError_DibosonSubtraction_UP         );
    hmdyTree->Branch( "Weight_TopQuarkError_DibosonXsection_UP"         ,  &Weight_TopQuarkError_DibosonXsection_UP         );
    hmdyTree->Branch( "Weight_TopQuarkError_DibosonSubtraction_DOWN"         ,  &Weight_TopQuarkError_DibosonSubtraction_DOWN         );
    hmdyTree->Branch( "Weight_TopQuarkError_DibosonXsection_DOWN"         ,  &Weight_TopQuarkError_DibosonXsection_DOWN         );
    hmdyTree->Branch( "Weight_TopQuarkError_PDF4LHC_Total_Up", &Weight_TopQuarkError_PDF4LHC_Total_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_PDF4LHC_Total_Down", &Weight_TopQuarkError_PDF4LHC_Total_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_ISR_Var3c_Up" , &Weight_TopQuarkError_ISR_Var3c_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_FSR_RenFact_Up" , &Weight_TopQuarkError_FSR_RenFact_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_ISR_Up" , &Weight_TopQuarkError_ISR_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_HardScatter_Up" , &Weight_TopQuarkError_HardScatter_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_FragHadModel_Up" , &Weight_TopQuarkError_FragHadModel_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_TopMass_Up" , &Weight_TopQuarkError_TopMass_Up );
    hmdyTree->Branch( "Weight_TopQuarkError_ISR_RenFact_Up" , &Weight_TopQuarkError_ISR_RenFact_Up );
                
    hmdyTree->Branch( "Weight_TopQuarkError_ISR_Var3c_Down" , &Weight_TopQuarkError_ISR_Var3c_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_FSR_RenFact_Down" , &Weight_TopQuarkError_FSR_RenFact_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_ISR_Down" , &Weight_TopQuarkError_ISR_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_HardScatter_Down" , &Weight_TopQuarkError_HardScatter_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_FragHadModel_Down" , &Weight_TopQuarkError_FragHadModel_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_TopMass_Down" , &Weight_TopQuarkError_TopMass_Down );
    hmdyTree->Branch( "Weight_TopQuarkError_ISR_RenFact_Down" , &Weight_TopQuarkError_ISR_RenFact_Down );

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_Up", &Weight_TopQuarkError_LepSF_EL_ID_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_Down", &Weight_TopQuarkError_LepSF_EL_ID_Down );
	        hmdyTree->Branch( "Weight_TopQuarkError_Stat_emu_Up" , &Weight_TopQuarkError_Stat_emu_Up );
	        hmdyTree->Branch( "Weight_TopQuarkError_Stat_emu_Down" , &Weight_TopQuarkError_Stat_emu_Down );
	        hmdyTree->Branch( "Weight_TopQuarkError_Stat_ee_Up" , &Weight_TopQuarkError_Stat_ee_Up );
	        hmdyTree->Branch( "Weight_TopQuarkError_Stat_ee_Down" , &Weight_TopQuarkError_Stat_ee_Down );
	        hmdyTree->Branch( "Weight_TopQuarkError_Stat_mumu_Up" , &Weight_TopQuarkError_Stat_mumu_Up );
	        hmdyTree->Branch( "Weight_TopQuarkError_Stat_mumu_Down" , &Weight_TopQuarkError_Stat_mumu_Down );
	        hmdyTree->Branch( "Weight_TopQuarkError_Theory_Up_ee" , &Weight_TopQuarkError_Theory_Up_ee );
	        hmdyTree->Branch( "Weight_TopQuarkError_Theory_Down_ee" , &Weight_TopQuarkError_Theory_Down_ee );
	        hmdyTree->Branch( "Weight_TopQuarkError_Theory_Up_mumu" , &Weight_TopQuarkError_Theory_Up_mumu );
	        hmdyTree->Branch( "Weight_TopQuarkError_Theory_Down_mumu" , &Weight_TopQuarkError_Theory_Down_mumu );
	        hmdyTree->Branch( "Weight_TopQuarkError_LeptonSys_Up" , &Weight_TopQuarkError_LeptonSys_Up );
	        hmdyTree->Branch( "Weight_TopQuarkError_LeptonSys_Down" , &Weight_TopQuarkError_LeptonSys_Down );
		/*hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_1_Up" , &Weight_TopQuarkError_LepSF_EL_ID_1_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_2_Up" , &Weight_TopQuarkError_LepSF_EL_ID_2_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_3_Up" , &Weight_TopQuarkError_LepSF_EL_ID_3_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_4_Up" , &Weight_TopQuarkError_LepSF_EL_ID_4_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_5_Up" , &Weight_TopQuarkError_LepSF_EL_ID_5_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_6_Up" , &Weight_TopQuarkError_LepSF_EL_ID_6_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_7_Up" , &Weight_TopQuarkError_LepSF_EL_ID_7_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_8_Up" , &Weight_TopQuarkError_LepSF_EL_ID_8_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_9_Up" , &Weight_TopQuarkError_LepSF_EL_ID_9_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_10_Up" , &Weight_TopQuarkError_LepSF_EL_ID_10_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_11_Up" , &Weight_TopQuarkError_LepSF_EL_ID_11_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_12_Up" , &Weight_TopQuarkError_LepSF_EL_ID_12_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_13_Up" , &Weight_TopQuarkError_LepSF_EL_ID_13_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_14_Up" , &Weight_TopQuarkError_LepSF_EL_ID_14_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_15_Up" , &Weight_TopQuarkError_LepSF_EL_ID_15_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_16_Up" , &Weight_TopQuarkError_LepSF_EL_ID_16_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_17_Up" , &Weight_TopQuarkError_LepSF_EL_ID_17_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_18_Up" , &Weight_TopQuarkError_LepSF_EL_ID_18_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_19_Up" , &Weight_TopQuarkError_LepSF_EL_ID_19_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_20_Up" , &Weight_TopQuarkError_LepSF_EL_ID_20_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_21_Up" , &Weight_TopQuarkError_LepSF_EL_ID_21_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_22_Up" , &Weight_TopQuarkError_LepSF_EL_ID_22_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_23_Up" , &Weight_TopQuarkError_LepSF_EL_ID_23_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_24_Up" , &Weight_TopQuarkError_LepSF_EL_ID_24_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_25_Up" , &Weight_TopQuarkError_LepSF_EL_ID_25_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_26_Up" , &Weight_TopQuarkError_LepSF_EL_ID_26_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_27_Up" , &Weight_TopQuarkError_LepSF_EL_ID_27_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_28_Up" , &Weight_TopQuarkError_LepSF_EL_ID_28_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_29_Up" , &Weight_TopQuarkError_LepSF_EL_ID_29_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_30_Up" , &Weight_TopQuarkError_LepSF_EL_ID_30_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_31_Up" , &Weight_TopQuarkError_LepSF_EL_ID_31_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_32_Up" , &Weight_TopQuarkError_LepSF_EL_ID_32_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_33_Up" , &Weight_TopQuarkError_LepSF_EL_ID_33_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_34_Up" , &Weight_TopQuarkError_LepSF_EL_ID_34_Up );

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_1_Down" , &Weight_TopQuarkError_LepSF_EL_ID_1_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_2_Down" , &Weight_TopQuarkError_LepSF_EL_ID_2_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_3_Down" , &Weight_TopQuarkError_LepSF_EL_ID_3_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_4_Down" , &Weight_TopQuarkError_LepSF_EL_ID_4_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_5_Down" , &Weight_TopQuarkError_LepSF_EL_ID_5_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_6_Down" , &Weight_TopQuarkError_LepSF_EL_ID_6_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_7_Down" , &Weight_TopQuarkError_LepSF_EL_ID_7_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_8_Down" , &Weight_TopQuarkError_LepSF_EL_ID_8_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_9_Down" , &Weight_TopQuarkError_LepSF_EL_ID_9_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_10_Down" , &Weight_TopQuarkError_LepSF_EL_ID_10_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_11_Down" , &Weight_TopQuarkError_LepSF_EL_ID_11_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_12_Down" , &Weight_TopQuarkError_LepSF_EL_ID_12_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_13_Down" , &Weight_TopQuarkError_LepSF_EL_ID_13_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_14_Down" , &Weight_TopQuarkError_LepSF_EL_ID_14_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_15_Down" , &Weight_TopQuarkError_LepSF_EL_ID_15_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_16_Down" , &Weight_TopQuarkError_LepSF_EL_ID_16_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_17_Down" , &Weight_TopQuarkError_LepSF_EL_ID_17_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_18_Down" , &Weight_TopQuarkError_LepSF_EL_ID_18_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_19_Down" , &Weight_TopQuarkError_LepSF_EL_ID_19_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_20_Down" , &Weight_TopQuarkError_LepSF_EL_ID_20_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_21_Down" , &Weight_TopQuarkError_LepSF_EL_ID_21_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_22_Down" , &Weight_TopQuarkError_LepSF_EL_ID_22_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_23_Down" , &Weight_TopQuarkError_LepSF_EL_ID_23_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_24_Down" , &Weight_TopQuarkError_LepSF_EL_ID_24_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_25_Down" , &Weight_TopQuarkError_LepSF_EL_ID_25_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_26_Down" , &Weight_TopQuarkError_LepSF_EL_ID_26_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_27_Down" , &Weight_TopQuarkError_LepSF_EL_ID_27_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_28_Down" , &Weight_TopQuarkError_LepSF_EL_ID_28_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_29_Down" , &Weight_TopQuarkError_LepSF_EL_ID_29_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_30_Down" , &Weight_TopQuarkError_LepSF_EL_ID_30_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_31_Down" , &Weight_TopQuarkError_LepSF_EL_ID_31_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_32_Down" , &Weight_TopQuarkError_LepSF_EL_ID_32_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_33_Down" , &Weight_TopQuarkError_LepSF_EL_ID_33_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ID_34_Down" , &Weight_TopQuarkError_LepSF_EL_ID_34_Down ); */

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_Reco_Up" , &Weight_TopQuarkError_LepSF_EL_Reco_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_Iso_Up" , &Weight_TopQuarkError_LepSF_EL_Iso_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_Trigger_Up" , &Weight_TopQuarkError_LepSF_EL_Trigger_Up );

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_Reco_Down" , &Weight_TopQuarkError_LepSF_EL_Reco_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_Iso_Down" , &Weight_TopQuarkError_LepSF_EL_Iso_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_Trigger_Down" , &Weight_TopQuarkError_LepSF_EL_Trigger_Down );

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_Up_emu" , &Weight_TopQuarkError_LepSF_MU_ID_Up_emu );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_Down_emu" , &Weight_TopQuarkError_LepSF_MU_ID_Down_emu );

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_TTVA_Up_emu" , &Weight_TopQuarkError_LepSF_MU_TTVA_Up_emu ); 
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Trigger_Up_emu" , &Weight_TopQuarkError_LepSF_MU_Trigger_Up_emu );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Iso_Up_emu" , &Weight_TopQuarkError_LepSF_MU_Iso_Up_emu );

		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_TTVA_Down_emu" , &Weight_TopQuarkError_LepSF_MU_TTVA_Down_emu ); 
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Trigger_Down_emu" , &Weight_TopQuarkError_LepSF_MU_Trigger_Down_emu );
		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Iso_Down_emu" , &Weight_TopQuarkError_LepSF_MU_Iso_Down_emu );

		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALCALO_Up" , &Weight_TopQuarkError_ELRES_MATERIALCALO_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALCRYO_Up" , &Weight_TopQuarkError_ELRES_MATERIALCRYO_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALGAP_Up" , &Weight_TopQuarkError_ELRES_MATERIALGAP_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALIBL_Up" , &Weight_TopQuarkError_ELRES_MATERIALIBL_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALPP0_Up" , &Weight_TopQuarkError_ELRES_MATERIALPP0_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_PILEUP_Up" , &Weight_TopQuarkError_ELRES_PILEUP_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_SAMPLINGTERM_Up" , &Weight_TopQuarkError_ELRES_SAMPLINGTERM_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_ZSMEARING_Up" , &Weight_TopQuarkError_ELRES_ZSMEARING_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_E4SCINTILLATOR_Up" , &Weight_TopQuarkError_ELSCALE_E4SCINTILLATOR_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_G4_Up" , &Weight_TopQuarkError_ELSCALE_G4_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_L1GAIN_Up" , &Weight_TopQuarkError_ELSCALE_L1GAIN_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_L2GAIN_Up" , &Weight_TopQuarkError_ELSCALE_L2GAIN_Up );
                hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARUNCONVCALIB_Up" , &Weight_TopQuarkError_ELSCALE_LARUNCONVCALIB_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARCALIB_Up" , &Weight_TopQuarkError_ELSCALE_LARCALIB_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARELECALIB_Up" , &Weight_TopQuarkError_ELSCALE_LARELECALIB_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARELECUNCONV_Up" , &Weight_TopQuarkError_ELSCALE_LARELECUNCONV_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATCALO_Up" , &Weight_TopQuarkError_ELSCALE_MATCALO_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATCRYO_Up" , &Weight_TopQuarkError_ELSCALE_MATCRYO_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATID_Up" , &Weight_TopQuarkError_ELSCALE_MATID_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATPP0_Up" , &Weight_TopQuarkError_ELSCALE_MATPP0_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_PEDESTAL_Up" , &Weight_TopQuarkError_ELSCALE_PEDESTAL_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_PS_BARREL_Up" , &Weight_TopQuarkError_ELSCALE_PS_BARREL_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_PS_Up" , &Weight_TopQuarkError_ELSCALE_PS_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_S12_Up" , &Weight_TopQuarkError_ELRES_MATERIALCRYO_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_TOPOCLUSTER_THRES_Up" , &Weight_TopQuarkError_ELSCALE_TOPOCLUSTER_THRES_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_WTOTS1_THRES_Up" , &Weight_TopQuarkError_ELSCALE_WTOTS1_THRES_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_ZEESYST_Up" , &Weight_TopQuarkError_ELSCALE_ZEESYST_Up );

		hmdyTree->Branch( "Weight_TopQuarkError_MUID_Up" , &Weight_TopQuarkError_MUID_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_MUMS_Up" , &Weight_TopQuarkError_MUMS_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_MUSCALE_Up" , &Weight_TopQuarkError_MUSCALE_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_MUSAGITTARES_Up" , &Weight_TopQuarkError_MUSAGITTARES_Up );
		hmdyTree->Branch( "Weight_TopQuarkError_MUSAGITTARHO_Up" , &Weight_TopQuarkError_MUSAGITTARHO_Up );

		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALCALO_Down" , &Weight_TopQuarkError_ELRES_MATERIALCALO_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALCRYO_Down" , &Weight_TopQuarkError_ELRES_MATERIALCRYO_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALGAP_Down" , &Weight_TopQuarkError_ELRES_MATERIALGAP_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALIBL_Down" , &Weight_TopQuarkError_ELRES_MATERIALIBL_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_MATERIALPP0_Down" , &Weight_TopQuarkError_ELRES_MATERIALPP0_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_PILEUP_Down" , &Weight_TopQuarkError_ELRES_PILEUP_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_SAMPLINGTERM_Down" , &Weight_TopQuarkError_ELRES_SAMPLINGTERM_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELRES_ZSMEARING_Down" , &Weight_TopQuarkError_ELRES_ZSMEARING_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_E4SCINTILLATOR_Down" , &Weight_TopQuarkError_ELSCALE_E4SCINTILLATOR_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_G4_Down" , &Weight_TopQuarkError_ELSCALE_G4_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_L1GAIN_Down" , &Weight_TopQuarkError_ELSCALE_L1GAIN_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_L2GAIN_Down" , &Weight_TopQuarkError_ELSCALE_L2GAIN_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARCALIB_Down" , &Weight_TopQuarkError_ELSCALE_LARCALIB_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARELECALIB_Down" , &Weight_TopQuarkError_ELSCALE_LARELECALIB_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARELECUNCONV_Down" , &Weight_TopQuarkError_ELSCALE_LARELECUNCONV_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATCALO_Down" , &Weight_TopQuarkError_ELSCALE_MATCALO_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATCRYO_Down" , &Weight_TopQuarkError_ELSCALE_MATCRYO_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATID_Down" , &Weight_TopQuarkError_ELSCALE_MATID_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_MATPP0_Down" , &Weight_TopQuarkError_ELSCALE_MATPP0_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_PEDESTAL_Down" , &Weight_TopQuarkError_ELSCALE_PEDESTAL_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_PS_BARREL_Down" , &Weight_TopQuarkError_ELSCALE_PS_BARREL_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_PS_Down" , &Weight_TopQuarkError_ELSCALE_PS_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_S12_Down" , &Weight_TopQuarkError_ELRES_MATERIALCRYO_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_TOPOCLUSTER_THRES_Down" , &Weight_TopQuarkError_ELSCALE_TOPOCLUSTER_THRES_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_WTOTS1_THRES_Down" , &Weight_TopQuarkError_ELSCALE_WTOTS1_THRES_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_ZEESYST_Down" , &Weight_TopQuarkError_ELSCALE_ZEESYST_Down );
                hmdyTree->Branch( "Weight_TopQuarkError_ELSCALE_LARUNCONVCALIB_Down" , &Weight_TopQuarkError_ELSCALE_LARUNCONVCALIB_Down );

		hmdyTree->Branch( "Weight_TopQuarkError_MUID_Down" , &Weight_TopQuarkError_MUID_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_MUMS_Down" , &Weight_TopQuarkError_MUMS_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_MUSCALE_Down" , &Weight_TopQuarkError_MUSCALE_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_MUSAGITTARES_Down" , &Weight_TopQuarkError_MUSAGITTARES_Down );
		hmdyTree->Branch( "Weight_TopQuarkError_MUSAGITTARHO_Down" , &Weight_TopQuarkError_MUSAGITTARHO_Down );

 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ChargeMisID_STAT_UP"         ,  &Weight_TopQuarkError_LepSF_EL_ChargeMisID_STAT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ChargeMisID_STAT_DOWN"         ,  &Weight_TopQuarkError_LepSF_EL_ChargeMisID_STAT_DOWN         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ChargeMisID_SYST_UP"         ,  &Weight_TopQuarkError_LepSF_EL_ChargeMisID_SYST_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_EL_ChargeMisID_SYST_DOWN"         ,  &Weight_TopQuarkError_LepSF_EL_ChargeMisID_SYST_DOWN         );

 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_STAT_UP"         ,  &Weight_TopQuarkError_LepSF_MU_ID_STAT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_STAT_DOWN"         ,  &Weight_TopQuarkError_LepSF_MU_ID_STAT_DOWN         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_STAT_LOWPT_UP"         ,  &Weight_TopQuarkError_LepSF_MU_ID_STAT_LOWPT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_STAT_LOWPT_DOWN"         ,  &Weight_TopQuarkError_LepSF_MU_ID_STAT_LOWPT_DOWN         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_SYST_LOWPT_UP"         ,  &Weight_TopQuarkError_LepSF_MU_ID_SYST_LOWPT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_ID_SYST_LOWPT_DOWN"         ,  &Weight_TopQuarkError_LepSF_MU_ID_SYST_LOWPT_DOWN         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_TTVA_STAT_UP"         ,  &Weight_TopQuarkError_LepSF_MU_TTVA_STAT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_TTVA_STAT_DOWN"         ,  &Weight_TopQuarkError_LepSF_MU_TTVA_STAT_DOWN         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Trigger_STAT_UP"         ,  &Weight_TopQuarkError_LepSF_MU_Trigger_STAT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Trigger_STAT_DOWN"         ,  &Weight_TopQuarkError_LepSF_MU_Trigger_STAT_DOWN         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Isol_STAT_UP"         ,  &Weight_TopQuarkError_LepSF_MU_Isol_STAT_UP         );
 		hmdyTree->Branch( "Weight_TopQuarkError_LepSF_MU_Isol_STAT_DOWN"         ,  &Weight_TopQuarkError_LepSF_MU_Isol_STAT_DOWN         );


  } //ed of if TopQuark Sample

  if( !Unfolding ){

	  hmdyTree->Branch( "pTllReweight" , &pTllReweight );
	  hmdyTree->Branch( "ChargeMisIDSFWeight"         ,  &ChargeMisIDSFWeight         );
	  hmdyTree->Branch( "LeptonSFWeight"         ,  &LeptonSFWeight         );
	  hmdyTree->Branch( "PileupWeight"         ,  &PileupWeight         );

	  hmdyTree->Branch( "Lepton1_Pt"         ,  &Lepton1_Pt         );
	  hmdyTree->Branch( "Lepton2_Pt"         ,  &Lepton2_Pt         );
	  hmdyTree->Branch( "Lepton1_Phi"         ,  &Lepton1_Phi         );
	  hmdyTree->Branch( "Lepton2_Phi"         ,  &Lepton2_Phi         );
	  hmdyTree->Branch( "Lepton1_Eta"         ,  &Lepton1_Eta         );
	  hmdyTree->Branch( "Lepton2_Eta"         ,  &Lepton2_Eta         );
	  hmdyTree->Branch( "n_Bjets"         ,  &n_Bjets         );
	  hmdyTree->Branch( "MET_Et"         ,  &MET_Et         );

	  hmdyTree->Branch( "Lepton1_z0"         ,  &Lepton1_z0         );
	  hmdyTree->Branch( "Lepton2_z0"         ,  &Lepton2_z0         );
	  hmdyTree->Branch( "Lepton1_d0sig"         ,  &Lepton1_d0sig         );
	  hmdyTree->Branch( "Lepton2_d0sig"         ,  &Lepton2_d0sig         );

	  hmdyTree->Branch( "Mu"         ,  &Mu         );
	  hmdyTree->Branch( "MuActual"         ,  &MuActual         );

	  hmdyTree->Branch( "PhiRF"         ,  &PhiRF         );

	  hmdyTree->Branch( "Weight_kFactor_Old"         ,  &Weight_kFactor_Old         );  
	  hmdyTree->Branch( "Weight_kFactor_QCDEW"         ,  &Weight_kFactor_QCDEW         );  
  	  hmdyTree->Branch( "Weight_kFactor_New_CT18NNLO"         ,  &Weight_kFactor_New_CT18NNLO         );  
  	  hmdyTree->Branch( "Weight_kFactor_New_CT18ANNLO"         ,  &Weight_kFactor_New_CT18ANNLO         );  
  	  hmdyTree->Branch( "Weight_kFactor_PowhegtoSherpa"         ,  &Weight_kFactor_PowhegtoSherpa         );  


	  if( ( nameOfSample == "Sherpa_ee_mc16a_LowMass" || nameOfSample == "Sherpa_ee_mc16a_HighMass" || nameOfSample == "Sherpa_mumu_mc16a_LowMass" || nameOfSample == "Sherpa_mumu_mc16a_HighMass" ) || ( nameOfSample == "Sherpa_ee_mc16d_LowMass" || nameOfSample == "Sherpa_ee_mc16d_HighMass" || nameOfSample == "Sherpa_mumu_mc16d_LowMass" || nameOfSample == "Sherpa_mumu_mc16d_HighMass" ) || ( nameOfSample == "Sherpa_ee_mc16e_LowMass" || nameOfSample == "Sherpa_ee_mc16e_HighMass" || nameOfSample == "Sherpa_mumu_mc16e_LowMass" || nameOfSample == "Sherpa_mumu_mc16e_HighMass" ) ){ 


	        hmdyTree->Branch( "Weight_ME" , &Weight_ME );
	        hmdyTree->Branch( "Weight_Normalisation" , &Weight_Normalisation );
	        hmdyTree->Branch( "Weight_NTrials" , &Weight_NTrials );
	        hmdyTree->Branch( "Weight_MUR05_MUF05" , &Weight_MUR05_MUF05 );
	        hmdyTree->Branch( "Weight_MUR05_MUF1" , &Weight_MUR05_MUF1 );
	        hmdyTree->Branch( "Weight_MUR1_MUF05" , &Weight_MUR1_MUF05 );
	        hmdyTree->Branch( "Weight_MUR1_MUF1" , &Weight_MUR1_MUF1 );
	        hmdyTree->Branch( "Weight_MUR05_MUF2" , &Weight_MUR05_MUF2 );
	        hmdyTree->Branch( "Weight_MUR2_MUF1" , &Weight_MUR2_MUF1 );
	        hmdyTree->Branch( "Weight_MUR2_MUF2" , &Weight_MUR2_MUF2 );		
	
  	 } // end of if size mc_generator exceeds 10

  	  if( (nameOfSample == "DrellYan_ee_mc16a" || nameOfSample == "DrellYan_ee_mc16d" || nameOfSample == "DrellYan_ee_mc16e" || nameOfSample == "Ztoee_mc16e" || nameOfSample == "Ztoee_mc16d" || nameOfSample == "Ztoee_mc16a" || nameOfSample == "DrellYan_mumu_mc16a" || nameOfSample == "DrellYan_mumu_mc16d" || nameOfSample == "DrellYan_mumu_mc16e" || nameOfSample == "Ztomumu_mc16e" || nameOfSample == "Ztomumu_mc16d" || nameOfSample == "Ztomumu_mc16a" || nameOfSample == "TopQuark_mc16e_Sys") && nameOfTree == "nominal" ){

		hmdyTree->Branch( "TruthCosThetaStar_Born"         ,  &TruthCosThetaStar_Born         );
   		hmdyTree->Branch( "Weight_PDF_DOWN"         ,  &Weight_PDF_DOWN         ); 
   		hmdyTree->Branch( "Weight_PDF_UP"         ,  &Weight_PDF_UP         ); 
   		hmdyTree->Branch( "Weight_kFactor_HERAPDF20"         ,  &Weight_kFactor_HERAPDF20         ); 
   		hmdyTree->Branch( "Weight_kFactor_NNPDF30"         ,  &Weight_kFactor_NNPDF30         ); 
   		hmdyTree->Branch( "Weight_kFactor_ct14nnlo_pdf_up"         ,  &Weight_kFactor_ct14nnlo_pdf_up         ); 
   		hmdyTree->Branch( "Weight_kFactor_ct14nnlo_pdf_down"         ,  &Weight_kFactor_ct14nnlo_pdf_down         ); 
   		hmdyTree->Branch( "Weight_kFactor_pi_mem0_spline"         ,  &Weight_kFactor_pi_mem0_spline         ); 
   		hmdyTree->Branch( "Weight_kFactor_pi_mem1_spline"         ,  &Weight_kFactor_pi_mem1_spline         ); 
   		hmdyTree->Branch( "Weight_kFactor_z_scaleup"         ,  &Weight_kFactor_z_scaleup         ); 
   		hmdyTree->Branch( "Weight_kFactor_z_scaledown"         ,  &Weight_kFactor_z_scaledown         ); 
   		hmdyTree->Branch( "Weight_kFactor_CT14nnlo_as_0118_alpha_s_up_fit"         ,  &Weight_kFactor_CT14nnlo_as_0118_alpha_s_up_fit         ); 
   		hmdyTree->Branch( "Weight_kFactor_CT14nnlo_as_0118_alpha_s_down_fit"         ,  &Weight_kFactor_CT14nnlo_as_0118_alpha_s_down_fit         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_beamUp_spline"         ,  &Weight_kFactor_Zgamma_beamUp_spline         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_beamDown_spline"         ,  &Weight_kFactor_Zgamma_beamDown_spline         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_1"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_1         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_2"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_2         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_3"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_3         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_4"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_4         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_5"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_5         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_6"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_6         ); 
   		hmdyTree->Branch( "Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_7"         ,  &Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_7         ); 
   		hmdyTree->Branch( "Weight_kFactor_PDF_EW__1up"         ,  &Weight_kFactor_PDF_EW__1up         ); 
   		hmdyTree->Branch( "Weight_kFactor_PDF_EW__1down"         ,  &Weight_kFactor_PDF_EW__1down         ); 
   		hmdyTree->Branch( "Weight_kFactor_PDF_REDCHOICE_NNPDF30"         ,  &Weight_kFactor_PDF_REDCHOICE_NNPDF30         ); 
		
		if( nameOfSample == "DrellYan_ee_mc16a" || nameOfSample == "DrellYan_ee_mc16d" || nameOfSample == "DrellYan_ee_mc16e" || nameOfSample == "Ztoee_mc16e" || nameOfSample == "Ztoee_mc16d" || nameOfSample == "Ztoee_mc16a"  || nameOfSample == "TopQuark_mc16e_Sys" ){

		  	hmdyTree->Branch( "Weight_indiv_SF_EL_Trigger_UP"         ,  &Weight_indiv_SF_EL_Trigger_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_Trigger_DOWN"         ,  &Weight_indiv_SF_EL_Trigger_DOWN         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_Reco_UP"         ,  &Weight_indiv_SF_EL_Reco_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_Reco_DOWN"         ,  &Weight_indiv_SF_EL_Reco_DOWN         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_ID_UP"         ,  &Weight_indiv_SF_EL_ID_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_ID_DOWN"         ,  &Weight_indiv_SF_EL_ID_DOWN         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_Isol_UP"         ,  &Weight_indiv_SF_EL_Isol_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_EL_Isol_DOWN"         ,  &Weight_indiv_SF_EL_Isol_DOWN         );
		 	hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_1"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_1         );
		 	hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_1"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_1         );
		 	hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_2"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_2         );
		 	hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_2"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_2         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_3"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_3         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_3"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_3         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_4"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_4        );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_4"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_4         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_5"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_5         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_5"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_5         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_6"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_6         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_6"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_6         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_7"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_7        );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_7"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_7         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_8"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_8         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_8"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_8         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_9"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_9         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_9"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_9         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_10"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_10         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_10"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_10         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_11"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_11         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_11"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_11         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_12"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_12         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_12"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_12         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_13"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_13         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_13"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_13         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_14"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_14        );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_14"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_14         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_15"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_15        );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_15"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_15         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_16"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_16         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_16"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_16         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_17"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_17         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_17"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_17         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_18"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_18         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_18"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_18         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_19"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_19         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_19"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_19         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_20"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_20         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_20"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_20         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_21"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_21         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_21"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_21         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_22"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_22         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_22"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_22         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_23"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_23         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_23"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_23         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_24"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_24         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_24"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_24         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_25"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_25         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_25"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_25         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_26"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_26         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_26"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_26         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_27"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_27         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_27"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_27         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_28"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_28         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_28"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_28         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_29"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_29         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_29"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_29         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_30"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_30         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_30"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_30         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_31"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_31         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_31"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_31         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_32"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_32         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_32"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_32         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_33"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_33         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_33"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_33         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_34"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_34         );
 			hmdyTree->Branch( "Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_34"         ,  &Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_34         );
 			hmdyTree->Branch( "Weight_indiv_SF_EL_ChargeMisID_STAT_UP"         ,  &Weight_indiv_SF_EL_ChargeMisID_STAT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_EL_ChargeMisID_STAT_DOWN"         ,  &Weight_indiv_SF_EL_ChargeMisID_STAT_DOWN         );
 			hmdyTree->Branch( "Weight_indiv_SF_EL_ChargeMisID_SYST_UP"         ,  &Weight_indiv_SF_EL_ChargeMisID_SYST_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_EL_ChargeMisID_SYST_DOWN"         ,  &Weight_indiv_SF_EL_ChargeMisID_SYST_DOWN         );

		} // end of if electron sample
	  	if( nameOfSample == "DrellYan_mumu_mc16a" || nameOfSample == "DrellYan_mumu_mc16d" || nameOfSample == "DrellYan_mumu_mc16e" || nameOfSample == "Ztomumu_mc16e" || nameOfSample == "Ztomumu_mc16d" || nameOfSample == "Ztomumu_mc16a" || nameOfSample == "TopQuark_mc16e_Sys" ){

 			hmdyTree->Branch( "Weight_indiv_SF_MU_ID_STAT_UP"         ,  &Weight_indiv_SF_MU_ID_STAT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_ID_STAT_DOWN"         ,  &Weight_indiv_SF_MU_ID_STAT_DOWN         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_ID_STAT_LOWPT_UP"         ,  &Weight_indiv_SF_MU_ID_STAT_LOWPT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN"         ,  &Weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_ID_SYST_LOWPT_UP"         ,  &Weight_indiv_SF_MU_ID_SYST_LOWPT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN"         ,  &Weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_TTVA_STAT_UP"         ,  &Weight_indiv_SF_MU_TTVA_STAT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_TTVA_STAT_DOWN"         ,  &Weight_indiv_SF_MU_TTVA_STAT_DOWN         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_Trigger_STAT_UP"         ,  &Weight_indiv_SF_MU_Trigger_STAT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_Trigger_STAT_DOWN"         ,  &Weight_indiv_SF_MU_Trigger_STAT_DOWN         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_Isol_STAT_UP"         ,  &Weight_indiv_SF_MU_Isol_STAT_UP         );
 			hmdyTree->Branch( "Weight_indiv_SF_MU_Isol_STAT_DOWN"         ,  &Weight_indiv_SF_MU_Isol_STAT_DOWN         );

		  	hmdyTree->Branch( "Weight_indiv_SF_MU_Trigger_UP"         ,  &Weight_indiv_SF_MU_Trigger_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_MU_Trigger_DOWN"         ,  &Weight_indiv_SF_MU_Trigger_DOWN         );
		  	hmdyTree->Branch( "Weight_indiv_SF_MU_ID_UP"         ,  &Weight_indiv_SF_MU_ID_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_MU_ID_DOWN"         ,  &Weight_indiv_SF_MU_ID_DOWN         );
		  	hmdyTree->Branch( "Weight_indiv_SF_MU_Isol_UP"         ,  &Weight_indiv_SF_MU_Isol_UP         );
		  	hmdyTree->Branch( "Weight_indiv_SF_MU_Isol_DOWN"         ,  &Weight_indiv_SF_MU_Isol_DOWN         );
		  	hmdyTree->Branch( "Weight_indiv_SF_MU_TTVA_UP"         ,  &Weight_indiv_SF_MU_TTVA_UP         );
		 	hmdyTree->Branch( "Weight_indiv_SF_MU_TTVA_DOWN"         ,  &Weight_indiv_SF_MU_TTVA_DOWN         );

		} // end of if muon sample

	} // end of if not a data sample


 	if( nameOfSample == "TopQuark_mc16e_Sys"  ){


		   	hmdyTree->Branch( "Weight_Top_ISR_RenFact_Up"         ,  &Weight_Top_ISR_RenFact_Up         ); 
		   	hmdyTree->Branch( "Weight_Top_ISR_RenFact_Down"         ,  &Weight_Top_ISR_RenFact_Down         ); 
		   	hmdyTree->Branch( "Weight_Top_ISR_Var3c_Up"         ,  &Weight_Top_ISR_Var3c_Up         ); 
		   	hmdyTree->Branch( "Weight_Top_ISR_Var3c_Down"         ,  &Weight_Top_ISR_Var3c_Down         ); 

		   	hmdyTree->Branch( "Weight_Top_FSR_RenFact_Up"         ,  &Weight_Top_FSR_RenFact_Up         ); 
		   	hmdyTree->Branch( "Weight_Top_FSR_RenFact_Down"         ,  &Weight_Top_FSR_RenFact_Down         ); 


		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_1"         ,  &Weight_Top_PDF4LHC_1         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_2"         ,  &Weight_Top_PDF4LHC_2         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_3"         ,  &Weight_Top_PDF4LHC_3         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_4"         ,  &Weight_Top_PDF4LHC_4         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_5"         ,  &Weight_Top_PDF4LHC_5         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_6"         ,  &Weight_Top_PDF4LHC_6         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_7"         ,  &Weight_Top_PDF4LHC_7         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_8"         ,  &Weight_Top_PDF4LHC_8         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_9"         ,  &Weight_Top_PDF4LHC_9         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_10"         ,  &Weight_Top_PDF4LHC_10         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_11"         ,  &Weight_Top_PDF4LHC_11         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_12"         ,  &Weight_Top_PDF4LHC_12         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_13"         ,  &Weight_Top_PDF4LHC_13         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_14"         ,  &Weight_Top_PDF4LHC_14         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_15"         ,  &Weight_Top_PDF4LHC_15         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_16"         ,  &Weight_Top_PDF4LHC_16         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_17"         ,  &Weight_Top_PDF4LHC_17         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_18"         ,  &Weight_Top_PDF4LHC_18         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_19"         ,  &Weight_Top_PDF4LHC_19         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_20"         ,  &Weight_Top_PDF4LHC_20         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_21"         ,  &Weight_Top_PDF4LHC_21         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_22"         ,  &Weight_Top_PDF4LHC_22         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_23"         ,  &Weight_Top_PDF4LHC_23         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_24"         ,  &Weight_Top_PDF4LHC_24         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_25"         ,  &Weight_Top_PDF4LHC_25         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_26"         ,  &Weight_Top_PDF4LHC_26         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_27"         ,  &Weight_Top_PDF4LHC_27         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_28"         ,  &Weight_Top_PDF4LHC_28         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_29"         ,  &Weight_Top_PDF4LHC_29         ); 
		   	hmdyTree->Branch( "Weight_Top_PDF4LHC_30"         ,  &Weight_Top_PDF4LHC_30         ); 

	} // end of if statement

  } // end of if !Unfolding

  if( nameOfTree == "nominal_Loose" ){
  	hmdyTree->Branch( "pass_ee_TT"         ,  &pass_ee_TT         );
  	hmdyTree->Branch( "pass_ee_TL"         ,  &pass_ee_TL         );
  	hmdyTree->Branch( "pass_ee_LT"         ,  &pass_ee_LT         );
  	hmdyTree->Branch( "pass_ee_LL"         ,  &pass_ee_LL         );

  	hmdyTree->Branch( "ele_isLHLoose"         ,  &ele_isLHLoose         );
  	hmdyTree->Branch( "ele_isLHMedium"         ,  &ele_isLHMedium         );
  	hmdyTree->Branch( "ele_isFixedCutTightIso"         ,  &ele_isFixedCutTightIso         );

  } // end of if statement

  if( Unfolding ){

  	hmdyTree->Branch( "TruthWeight"         ,  &TruthWeight         );

	if( doElectronChannel){

		  		/*hmdyTree->Branch( "RecoDilRapidity_00_08_ee"         ,  &RecoDilRapidity_00_08_ee        );
		  		hmdyTree->Branch( "RecoDilRapidity_08_16_ee"         ,  &RecoDilRapidity_08_16_ee         );
		  		hmdyTree->Branch( "RecoDilRapidity_16_24_ee"         ,  &RecoDilRapidity_16_24_ee         );
		  		hmdyTree->Branch( "RecoDilRapidity_00_16_ee"         ,  &RecoDilRapidity_00_16_ee         );
		  		hmdyTree->Branch( "RecoDilRapidity_24_ee"         ,  &RecoDilRapidity_24_ee         );

		  		hmdyTree->Branch( "RecoDilRapidity_00_08_Fwdee"         ,  &RecoDilRapidity_00_08_Fwdee        );
		  		hmdyTree->Branch( "RecoDilRapidity_08_16_Fwdee"         ,  &RecoDilRapidity_08_16_Fwdee         );
		  		hmdyTree->Branch( "RecoDilRapidity_16_24_Fwdee"         ,  &RecoDilRapidity_16_24_Fwdee         );
		  		hmdyTree->Branch( "RecoDilRapidity_00_16_Fwdee"         ,  &RecoDilRapidity_00_16_Fwdee         );
		  		hmdyTree->Branch( "RecoDilRapidity_24_Fwdee"         ,  &RecoDilRapidity_24_Fwdee         );*/

	} // end of if electron Channel

	if( doMuonChannel){

		  		/*hmdyTree->Branch( "RecoDilRapidity_00_08_mumu"         ,  &RecoDilRapidity_00_08_mumu        );
  				hmdyTree->Branch( "RecoDilRapidity_08_16_mumu"         ,  &RecoDilRapidity_08_16_mumu         );
  				hmdyTree->Branch( "RecoDilRapidity_16_24_mumu"         ,  &RecoDilRapidity_16_24_mumu         );
  				hmdyTree->Branch( "RecoDilRapidity_00_16_mumu"         ,  &RecoDilRapidity_00_16_mumu         );
  				hmdyTree->Branch( "RecoDilRapidity_24_mumu"         ,  &RecoDilRapidity_24_mumu         );*/


	} // end of if muon channel

	if( truthLevel == "Born" ){

			hmdyTree->Branch( "TruthCosThetaStar_Born"         ,  &TruthCosThetaStar_Born         );
			hmdyTree->Branch( "TruthPhiStar_Born"         ,  &TruthPhiStar_Born         );
			hmdyTree->Branch( "TruthDilPt_Born"         ,  &TruthDilPt_Born         );
			 
	  		hmdyTree->Branch( "BornTruthSelectionPassed"         ,  &BornTruthSelectionPassed         );
	  		hmdyTree->Branch( "BornTruthSelectionPassed_CF"         ,  &BornTruthSelectionPassed_CF         );
			/*hmdyTree->Branch( "Lepton1_Pt_Born"         ,  &Lepton1_Pt_Born         );
			hmdyTree->Branch( "Lepton2_Pt_Born"         ,  &Lepton2_Pt_Born         );
			hmdyTree->Branch( "Lepton1_Phi_Born"         ,  &Lepton1_Phi_Born         );
			hmdyTree->Branch( "Lepton2_Phi_Born"         ,  &Lepton2_Phi_Born         );
			hmdyTree->Branch( "Lepton1_Eta_Born"         ,  &Lepton1_Eta_Born         );
			hmdyTree->Branch( "Lepton2_Eta_Born"         ,  &Lepton2_Eta_Born         );*/

			if( doElectronChannel){

		  		/*hmdyTree->Branch( "TruthDilRapidity_00_08_Born_ee"         ,  &TruthDilRapidity_00_08_Born_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_08_16_Born_ee"         ,  &TruthDilRapidity_08_16_Born_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_00_16_Born_ee"         ,  &TruthDilRapidity_00_16_Born_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_16_24_Born_ee"         ,  &TruthDilRapidity_16_24_Born_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_24_Born_ee"         ,  &TruthDilRapidity_24_Born_ee         );*/

			} // end of if Electron Channel

			if( doMuonChannel){


		  		/*hmdyTree->Branch( "TruthDilRapidity_00_08_Born_mumu"         ,  &TruthDilRapidity_00_08_Born_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_08_16_Born_mumu"         ,  &TruthDilRapidity_08_16_Born_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_00_16_Born_mumu"         ,  &TruthDilRapidity_00_16_Born_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_16_24_Born_mumu"         ,  &TruthDilRapidity_16_24_Born_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_24_Born_mumu"         ,  &TruthDilRapidity_24_Born_mumu         );*/

			} // end of if Muon Channel

	} // end of if Born truth level
	if( truthLevel == "Bare" ){
	
	  		hmdyTree->Branch( "BareTruthSelectionPassed"         ,  &BareTruthSelectionPassed         );	
  			hmdyTree->Branch( "BareTruthSelectionPassed_CF"         ,  &BareTruthSelectionPassed_CF         );
			hmdyTree->Branch( "TruthDilPt_Bare"         ,  &TruthDilPt_Bare         );
			hmdyTree->Branch( "TruthDilMass_Bare"         ,  &TruthDilMass_Bare         );
			hmdyTree->Branch( "TruthDilRapidity_Bare"         ,  &TruthDilRapidity_Bare         );
			hmdyTree->Branch( "TruthCosThetaStar_Bare"         ,  &TruthCosThetaStar_Bare         );
			hmdyTree->Branch( "TruthPhiStar_Bare"         ,  &TruthPhiStar_Bare         );
			/*hmdyTree->Branch( "Lepton1_Phi_Bare"         ,  &Lepton1_Phi_Bare         );
			hmdyTree->Branch( "Lepton2_Phi_Bare"         ,  &Lepton2_Phi_Bare         );	
			hmdyTree->Branch( "Lepton1_Pt_Bare"         ,  &Lepton1_Pt_Bare         );
			hmdyTree->Branch( "Lepton2_Pt_Bare"         ,  &Lepton2_Pt_Bare         );
			hmdyTree->Branch( "Lepton1_Eta_Bare"         ,  &Lepton1_Eta_Bare         );
			hmdyTree->Branch( "Lepton2_Eta_Bare"         ,  &Lepton2_Eta_Bare         );*/

			if( doElectronChannel){

		  		/*hmdyTree->Branch( "TruthDilRapidity_00_08_Bare_ee"         ,  &TruthDilRapidity_00_08_Bare_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_08_16_Bare_ee"         ,  &TruthDilRapidity_08_16_Bare_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_00_16_Bare_ee"         ,  &TruthDilRapidity_00_16_Bare_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_16_24_Bare_ee"         ,  &TruthDilRapidity_16_24_Bare_ee         );
		  		hmdyTree->Branch( "TruthDilRapidity_24_Bare_ee"         ,  &TruthDilRapidity_24_Bare_ee         );*/


			} // end of if ElectronChannel

			if( doMuonChannel){

		  		/*hmdyTree->Branch( "TruthDilRapidity_00_08_Bare_mumu"         ,  &TruthDilRapidity_00_08_Bare_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_08_16_Bare_mumu"         ,  &TruthDilRapidity_08_16_Bare_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_00_16_Bare_mumu"         ,  &TruthDilRapidity_00_16_Bare_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_16_24_Bare_mumu"         ,  &TruthDilRapidity_16_24_Bare_mumu         );
		  		hmdyTree->Branch( "TruthDilRapidity_24_Bare_mumu"         ,  &TruthDilRapidity_24_Bare_mumu         );*/

			} // end of if MuonChannel

       } // end of if Bare truth Level
       if( truthLevel == "Dressed" ){
	
  			hmdyTree->Branch( "DressedTruthSelectionPassed"         ,  &DressedTruthSelectionPassed         );
	                hmdyTree->Branch( "DressedTruthSelectionPassed_CF"         ,  &DressedTruthSelectionPassed_CF         );
			/*hmdyTree->Branch( "Lepton1_Pt_Dressed"         ,  &Lepton1_Pt_Dressed         );
			hmdyTree->Branch( "Lepton2_Pt_Dressed"         ,  &Lepton2_Pt_Dressed         );
			hmdyTree->Branch( "Lepton1_Eta_Dressed"         ,  &Lepton1_Eta_Dressed         );
			hmdyTree->Branch( "Lepton2_Eta_Dressed"         ,  &Lepton2_Eta_Dressed         );
			hmdyTree->Branch( "Lepton1_Phi_Dressed"         ,  &Lepton1_Phi_Dressed         );
			hmdyTree->Branch( "Lepton2_Phi_Dressed"         ,  &Lepton2_Phi_Dressed         );*/
			hmdyTree->Branch( "TruthDilRapidity_Dressed"         ,  &TruthDilRapidity_Dressed         );
			hmdyTree->Branch( "TruthDilMass_Dressed"         ,  &TruthDilMass_Dressed         );
			hmdyTree->Branch( "TruthDilPt_Dressed"         ,  &TruthDilPt_Dressed         );
			hmdyTree->Branch( "TruthPhiStar_Dressed"         ,  &TruthPhiStar_Dressed         );
			hmdyTree->Branch( "TruthCosThetaStar_Dressed"         ,  &TruthCosThetaStar_Dressed         );

			if( doElectronChannel){

  				/*hmdyTree->Branch( "TruthDilRapidity_00_08_Dressed_ee"         ,  &TruthDilRapidity_00_08_Dressed_ee         );
  				hmdyTree->Branch( "TruthDilRapidity_08_16_Dressed_ee"         ,  &TruthDilRapidity_08_16_Dressed_ee         );
  				hmdyTree->Branch( "TruthDilRapidity_00_16_Dressed_ee"         ,  &TruthDilRapidity_00_16_Dressed_ee         );
  				hmdyTree->Branch( "TruthDilRapidity_16_24_Dressed_ee"         ,  &TruthDilRapidity_16_24_Dressed_ee         );
  				hmdyTree->Branch( "TruthDilRapidity_24_Dressed_ee"         ,  &TruthDilRapidity_24_Dressed_ee         );*/

			} // end of if ElectronChannel

			if( doMuonChannel){

  				/*hmdyTree->Branch( "TruthDilRapidity_00_08_Dressed_mumu"         ,  &TruthDilRapidity_00_08_Dressed_mumu         );
  				hmdyTree->Branch( "TruthDilRapidity_08_16_Dressed_mumu"         ,  &TruthDilRapidity_08_16_Dressed_mumu         );
  				hmdyTree->Branch( "TruthDilRapidity_00_16_Dressed_mumu"         ,  &TruthDilRapidity_00_16_Dressed_mumu         );
  				hmdyTree->Branch( "TruthDilRapidity_16_24_Dressed_mumu"         ,  &TruthDilRapidity_16_24_Dressed_mumu         );
  				hmdyTree->Branch( "TruthDilRapidity_24_Dressed_mumu"         ,  &TruthDilRapidity_24_Dressed_mumu         );*/

			} // end of if MuonChannel

	} // end of if Dressed truthLevel

    } // end of if unfolding

} // end of function 

void Comparison :: ResetVariables(){

ele_isLHLoose.clear();
ele_isLHMedium.clear();
ele_isFixedCutTightIso.clear();

Weight_TopQuarkError_LepSF_EL_ID_Up.clear();
Weight_TopQuarkError_LepSF_EL_ID_Down.clear();

TruthDilRapidity_00_08_Born_ee=false;
TruthDilRapidity_08_16_Born_ee=false;
TruthDilRapidity_00_16_Born_ee=false;
TruthDilRapidity_16_24_Born_ee=false;
TruthDilRapidity_24_Born_ee=false;

TruthDilRapidity_00_08_Bare_ee=false;
TruthDilRapidity_08_16_Bare_ee=false;
TruthDilRapidity_00_16_Bare_ee=false;
TruthDilRapidity_16_24_Bare_ee=false;
TruthDilRapidity_24_Bare_ee=false;

TruthDilRapidity_00_08_Dressed_ee=false;
TruthDilRapidity_08_16_Dressed_ee=false;
TruthDilRapidity_00_16_Dressed_ee=false;
TruthDilRapidity_16_24_Dressed_ee=false;
TruthDilRapidity_24_Dressed_ee=false;

TruthDilRapidity_00_08_Born_mumu=false;
TruthDilRapidity_08_16_Born_mumu=false;
TruthDilRapidity_00_16_Born_mumu=false;
TruthDilRapidity_16_24_Born_mumu=false;
TruthDilRapidity_24_Born_mumu=false;

TruthDilRapidity_00_08_Bare_mumu=false;
TruthDilRapidity_08_16_Bare_mumu=false;
TruthDilRapidity_00_16_Bare_mumu=false;
TruthDilRapidity_16_24_Bare_mumu=false;
TruthDilRapidity_24_Bare_mumu=false;

TruthDilRapidity_00_08_Dressed_mumu=false;
TruthDilRapidity_08_16_Dressed_mumu=false;
TruthDilRapidity_00_16_Dressed_mumu=false;
TruthDilRapidity_16_24_Dressed_mumu=false;
TruthDilRapidity_24_Dressed_mumu=false;

RecoDilRapidity_00_08_ee=false;
RecoDilRapidity_08_16_ee=false;
RecoDilRapidity_16_24_ee=false;
RecoDilRapidity_00_16_ee=false;
RecoDilRapidity_24_ee=false;

RecoDilRapidity_00_08_Fwdee=false;
RecoDilRapidity_08_16_Fwdee=false;
RecoDilRapidity_16_24_Fwdee=false;
RecoDilRapidity_00_16_Fwdee=false;
RecoDilRapidity_24_Fwdee=false;

RecoDilRapidity_00_08_mumu=false;
RecoDilRapidity_08_16_mumu=false;
RecoDilRapidity_16_24_mumu=false;
RecoDilRapidity_00_16_mumu=false;
 RecoDilRapidity_24_mumu=false;

LeptonSFWeight=1.0;
PileupWeight=1.0;
ChargeMisIDSFWeight=1.0;

Weight_TopQuarkError_Stat_emu_Up=1.0;
Weight_TopQuarkError_Stat_emu_Down=1.0;
Weight_TopQuarkError_Stat_ee_Up=1.0;
Weight_TopQuarkError_Stat_ee_Down=1.0;
Weight_TopQuarkError_Stat_mumu_Up=1.0;
Weight_TopQuarkError_Stat_mumu_Down=1.0;
Weight_TopQuarkError_Theory_Up_ee=1.0;
Weight_TopQuarkError_Theory_Down_ee=1.0;
Weight_TopQuarkError_Theory_Up_mumu=1.0;
Weight_TopQuarkError_Theory_Down_mumu=1.0;
Weight_TopQuarkError_LeptonSys_Up=1.0;
Weight_TopQuarkError_LeptonSys_Down=1.0;

 Weight_TopQuarkError_PDF4LHC_Total_Up=1.0;                                                                                                                                                                  
 Weight_TopQuarkError_PDF4LHC_Total_Down=1.0;                                                                                                                                                                
 Weight_TopQuarkError_ISR_Var3c_Up=1.0;                                                                                                                                                                           Weight_TopQuarkError_FSR_RenFact_Up=1.0;                                                                                                                                                                    
 Weight_TopQuarkError_ISR_Up=1.0;                                                                                                                                                                            
 Weight_TopQuarkError_HardScatter_Up=1.0;                                                                                                                                                                     Weight_TopQuarkError_FragHadModel_Up=1.0;                                                                                                                                                                        Weight_TopQuarkError_TopMass_Up=1.0;                                                                                                                                                                        
 Weight_TopQuarkError_ISR_RenFact_Up=1.0;                                                                                                                                                                    
 Weight_TopQuarkError_ISR_Var3c_Down=1.0;                                                                                                                                                                    Weight_TopQuarkError_FSR_RenFact_Down=1.0;                                                                                                                                                                   Weight_TopQuarkError_ISR_Down=1.0;                                                                                                                                                                           Weight_TopQuarkError_HardScatter_Down=1.0;                                                                                                                                                                    Weight_TopQuarkError_FragHadModel_Down=1.0;                                                                                                                                                                      Weight_TopQuarkError_TopMass_Down=1.0;                                                                                                                                                                      
 Weight_TopQuarkError_ISR_RenFact_Down=1.0;   


Weight_kFactor_QCDEW=1.0;
Weight_kFactor_Old=1.0; 
Weight_kFactor_New_CT18NNLO=1.0; 
Weight_kFactor_New_CT18ANNLO=1.0; 
Weight_kFactor_PowhegtoSherpa=1.0;
Weight_PDF_UP=1.0;
Weight_PDF_DOWN=1.0;
pTllReweight=1.0;



Weight_Lepton1_SF=1.0;
Weight_Lepton2_SF=1.0;

PhiRF=-999.0;

Weight_ME=1.0; 
Weight_Normalisation=1.0; 
Weight_NTrials=1.0; 
Weight_MUR05_MUF05=1.0;
Weight_MUR05_MUF1=1.0;
Weight_MUR1_MUF05=1.0;
Weight_MUR1_MUF1=1.0; 
Weight_MUR05_MUF2=1.0;
Weight_MUR2_MUF1=1.0; 
Weight_MUR2_MUF2=1.0; 

 EventNumber=0;
 RunNumber=0;
  RecoDilPhi=-999;
  FEle_Pt=0.0; 
  FEle_EtCone20=0.0; 
  FEle_EtCone30=0.0; 
  FEle_EtCone40=0.0;
  Mu=0.0;
  n_Bjets=0;
  HT=0;
  MET_Et=0.0;
  MET_Phi=0.0;
  DeltaPhi_ll =0.0;
  TruthWeight=1.0;
  RecoWeight=1.0;
  weight_MM=1.0;
  FwdeeChannel=false;
  mumuChannel=false;
  eeChannel=false;
  emuChannel=false;
  BornTruthSelectionPassed=false;
  BareTruthSelectionPassed=false;
  DressedTruthSelectionPassed=false;
  RecoSelectionPassed=false;
  BornTruthSelectionPassed_CF=false;
  BareTruthSelectionPassed_CF=false;
  DressedTruthSelectionPassed_CF=false;
  Lepton1_Pt=0.0;
  Lepton2_Pt=0.0;
  Lepton1_Phi=0.0;
  Lepton2_Phi=0.0;
  Lepton1_Eta=0.0;
  Lepton2_Eta=0.0;
  Lepton1_Pt_Born = 0.0;
  Lepton2_Pt_Born = 0.0;
  Lepton1_Pt_Bare = 0.0;
  Lepton2_Pt_Bare = 0.0;
  Lepton1_Pt_Dressed = 0.0;
  Lepton2_Pt_Dressed = 0.0;
  Lepton1_Eta_Born = 0.0;
  Lepton2_Eta_Born = 0.0;
  Lepton1_Eta_Bare = 0.0;
  Lepton2_Eta_Bare = 0.0;
  Lepton1_Eta_Dressed = 0.0;
  Lepton2_Eta_Dressed = 0.0;
  Lepton1_Phi_Born = 0.0;
  Lepton2_Phi_Born = 0.0;
  Lepton1_Phi_Bare = 0.0;
  Lepton2_Phi_Bare = 0.0;
  Lepton1_Phi_Dressed = 0.0;
  Lepton2_Phi_Dressed = 0.0;
  RecoDilRapidity        = 0.0;
  TruthDilRapidity_Born        = 0.0;
  TruthDilRapidity_Bare        = 0.0;
  TruthDilRapidity_Dressed        = 0.0;
  RecoDilMass        = 0.0;
  RecoPseudoDilMass = 0.0;
  RecoDilMass_ID        = 0.0;
  RecoDilMass_MS        = 0.0;
  TruthDilMass_Born        = 0.0;
  TruthDilMass_Bare        = 0.0;
  TruthDilMass_Dressed        = 0.0;
  RecoDilPt        = 0.0;
  TruthDilPt_Born        = 0.0;
  TruthDilPt_Bare        = 0.0;
  TruthDilPt_Dressed        = 0.0;
  RecoPhiStar        = 0.0;
  TruthPhiStar_Born        = 0.0;
  TruthPhiStar_Bare        = 0.0;
  TruthPhiStar_Dressed        = 0.0;
  RecoCosThetaStar        = -999;
  TruthCosThetaStar_Born        = -999;
  TruthCosThetaStar_Bare        = -999;
  TruthCosThetaStar_Dressed        = -999;
  pass_ee_TT=false;
  pass_ee_LT=false;
  pass_ee_TL=false;
  pass_ee_LL=false;

    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_1=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_1=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_2=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_2=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_3=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_3=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_4=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_4=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_5=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_5=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_6=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_6=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_7=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_7=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_8=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_8=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_9=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_9=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_10=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_10=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_11=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_11=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_12=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_12=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_13=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_13=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_14=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_14=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_15=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_15=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_16=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_16=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_17=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_17=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_18=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_18=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_19=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_19=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_20=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_20=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_21=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_21=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_22=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_22=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_23=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_23=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_24=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_24=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_25=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_25=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_26=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_26=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_27=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_27=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_28=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_28=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_29=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_29=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_30=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_30=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_31=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_31=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_32=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_32=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_33=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_33=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP_34=1.0;
    Weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN_34=1.0;


   Weight_indiv_SF_MU_ID_STAT_UP=1.0;
   Weight_indiv_SF_MU_ID_STAT_UP=1.0;
   Weight_indiv_SF_MU_ID_STAT_DOWN=1.0;
   Weight_indiv_SF_MU_ID_STAT_LOWPT_UP;//! 
   Weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN=1.0;
   Weight_indiv_SF_MU_ID_SYST_LOWPT_UP=1.0;
   Weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN=1.0;
   Weight_indiv_SF_MU_TTVA_STAT_UP=1.0;
   Weight_indiv_SF_MU_TTVA_STAT_DOWN=1.0;
   Weight_indiv_SF_MU_Trigger_STAT_UP=1.0;
   Weight_indiv_SF_MU_Trigger_STAT_DOWN=1.0;
   Weight_indiv_SF_MU_Isol_STAT_UP=1.0;
   Weight_indiv_SF_MU_Isol_STAT_DOWN=1.0;

   Weight_indiv_SF_EL_ChargeMisID_STAT_UP=1.0;
   Weight_indiv_SF_EL_ChargeMisID_STAT_DOWN=1.0;
   Weight_indiv_SF_EL_ChargeMisID_SYST_UP=1.0;
   Weight_indiv_SF_EL_ChargeMisID_SYST_DOWN=1.0;

   Weight_kFactor_HERAPDF20=1.0;
   Weight_kFactor_NNPDF30=1.0;
   Weight_kFactor_ct14nnlo_pdf_up=1.0;
   Weight_kFactor_ct14nnlo_pdf_down=1.0;
   Weight_kFactor_pi_mem0_spline=1.0;
   Weight_kFactor_pi_mem1_spline=1.0;
   Weight_kFactor_z_scaleup=1.0;
   Weight_kFactor_z_scaledown=1.0;
   Weight_kFactor_CT14nnlo_as_0118_alpha_s_up_fit=1.0;
   Weight_kFactor_CT14nnlo_as_0118_alpha_s_down_fit=1.0;
   Weight_kFactor_Zgamma_beamUp_spline=1.0;
   Weight_kFactor_Zgamma_beamDown_spline=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_1=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_2=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_3=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_4=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_5=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_6=1.0;
   Weight_kFactor_Zgamma_CT14nnlo_EWbun_EV_7=1.0;
   Weight_kFactor_PDF_EW__1up=1.0;
   Weight_kFactor_PDF_EW__1down=1.0;
   Weight_kFactor_PDF_REDCHOICE_NNPDF30=1.0;

   Weight_indiv_SF_EL_Trigger=1.0;
   Weight_indiv_SF_EL_Trigger_UP=1.0;
   Weight_indiv_SF_EL_Trigger_DOWN=1.0;
   Weight_indiv_SF_EL_Reco=1.0;
   Weight_indiv_SF_EL_Reco_UP=1.0;
   Weight_indiv_SF_EL_Reco_DOWN=1.0;
   Weight_indiv_SF_EL_ID=1.0;
   Weight_indiv_SF_EL_ID_UP=1.0;
   Weight_indiv_SF_EL_ID_DOWN=1.0;
   Weight_indiv_SF_EL_Isol=1.0;
   Weight_indiv_SF_EL_Isol_UP=1.0;
   Weight_indiv_SF_EL_Isol_DOWN=1.0;

   Weight_indiv_SF_MU_Trigger=1.0;
   Weight_indiv_SF_MU_Trigger_UP=1.0;
   Weight_indiv_SF_MU_Trigger_DOWN=1.0;
   Weight_indiv_SF_MU_TTVA=1.0;
   Weight_indiv_SF_MU_TTVA_UP=1.0;
   Weight_indiv_SF_MU_TTVA_DOWN=1.0;
   Weight_indiv_SF_MU_ID=1.0;
   Weight_indiv_SF_MU_ID_UP=1.0;
   Weight_indiv_SF_MU_ID_DOWN=1.0;
   Weight_indiv_SF_MU_Isol=1.0;
   Weight_indiv_SF_MU_Isol_UP=1.0;
   Weight_indiv_SF_MU_Isol_DOWN=1.0;

} //end of variables 


bool  Comparison::RFrame(TLorentzVector Z, TVector3 &RFAxis, TVector3 &xAxis,
			     TVector3 &yAxis,string frame){
//---------------------------------------------------------------------------
// boost event into the boson rest frame  sets the frame axis
// input TLorentzVector Z boson, string frame name 
// output RFaxis, xAxis, yAxis
// return false if sanity checks fail, true otherwise
 
  bool isGood = true;
  TLorentzVector p1, p2;
  double sign  = fabs(Z.Z())/Z.Z();
  
  double ProtonMass = 0.938272;
  double sqrts=13.0;
  static double BeamEnergy = sqrts*1000/2;
	
   
  p1.SetPxPyPzE(0, 0,sign*BeamEnergy, 
  		TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); 
  p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy, 
  		TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); 
    
  p1.Boost(-Z.BoostVector());
  p2.Boost(-Z.BoostVector());

  if(!strcmp(frame.c_str(),"CS"))       RFAxis =((p1.Vect()).Unit()-(p2.Vect()).Unit()).Unit();
  else if(!strcmp(frame.c_str(),"HX"))  RFAxis = Z.Vect().Unit();
  else if(!strcmp(frame.c_str(),"GJ1")) RFAxis = p1.Vect().Unit();
  else if(!strcmp(frame.c_str(),"GJ2")) RFAxis = p2.Vect().Unit();
  else if(!strcmp(frame.c_str(),"PX"))  RFAxis = ((p1.Vect()).Unit()+(p2.Vect()).Unit()).Unit();
  else {cerr<<"BRFFrame : unknown frame "<<frame<<endl; return !isGood;}
 
  yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(RFAxis);
  xAxis = xAxis.Unit();
  
  return isGood;
}

//---------------------------------------------------------------------------
double  Comparison::GetCosThetaRF(TLorentzVector lepM,TLorentzVector lepP,
				    string frame){
//----------------------------------------------------------------------------
// Calculates cos(Theta*) in the Boson Rest Frame.
// boost negative lepton in Z-rest frame, calls appropriative Frame(CS,HX,PX,GJ) 
// and calculates angle  between ZAxis and the boosted negative lepton.
// return cos(Theta*)
 
   TVector3 RFAxis, yAxis, xAxis;
   TLorentzVector boostedLep = lepM;
   TLorentzVector Z=lepM+lepP;

   boostedLep.Boost(-Z.BoostVector());

   if(!RFrame(Z, RFAxis, xAxis, yAxis,frame)) return 99999.;

   return cos(boostedLep.Angle(RFAxis));
}
//------------------------------------------------------------------------
double Comparison::GetPhiRF(TLorentzVector lepM,TLorentzVector lepP,
			       string frame){
//------------------------------------------------------------------------
// Calculates phi in the Boson rest Frame.
// boosts the negative lepton into to boson rest frame,
// calls appropriative Frame(CS,GJ,PX,HX)  and  calculates 
// the angle of the boosted lepton, projected onto the x,y plane.
// retrun phi

  TLorentzVector boostedLep = lepM;
  TVector3 RFAxis, xAxis, yAxis;
  TLorentzVector Z=lepM+lepP;
    
  boostedLep.Boost(-Z.BoostVector());
  if(!RFrame(Z, RFAxis, xAxis, yAxis,frame)) return 9999.;
  //atan2 Principal arc tangent of y/x, in the interval [-pi,+pi] radians.
  double phi = atan2((boostedLep.Vect()*yAxis),(boostedLep.Vect()*xAxis));

  bool doPhiModulus=true;

  //phi from -pi to pi -> from  0 to 2pi
  if(phi<0 && doPhiModulus) phi=phi+2*TMath::Pi();

  return phi; 
} // end of function

double Comparison::PhiStarEta(double phi_1, double eta_1, double phi_2, double eta_2){
 
  double phiStarEta;
  double deltaPhi;
  double phiAcop;
  double sinThetaStarEta;
  double tanAcop;
 
  double cosThetaStarEta;

  cosThetaStarEta = TMath::TanH((eta_1-eta_2)/2);
  

  sinThetaStarEta = sqrt(1-cosThetaStarEta*cosThetaStarEta);
  deltaPhi = fabs(phi_1-phi_2);
  phiAcop = fabs(TMath::Pi()-deltaPhi);
  tanAcop = TMath::Tan(phiAcop/2);

  phiStarEta = tanAcop*sinThetaStarEta;

  return phiStarEta;

} // end of function

double Comparison :: CalculateDeltaPhi( double Phi1, double Phi2 ){

      double Dphi = Phi1 - Phi2;
      if( fabs(Dphi)>TMath::Pi() ) {
      if( Dphi>0 ){
      	Dphi = 2*TMath::Pi()-Dphi;
      }
      else{
      	Dphi = 2*TMath::Pi()+Dphi;
    	}
      } // end of DeltaPhi calculation

      return Dphi;

} // end of function 

double Comparison :: MuonSmearing( double mu_pt, double mu_eta){

//mu_pt needs to be in GeV
//mu_eta is the absolute value |eta|

double sigma=0.0;
double newPt=0.0;

if(mu_eta<1.05){
    // calculate cat 0 scenario
    sigma=(pow(0.0594957,2)+pow(0.000212924*mu_pt,2)) - (pow(0.0546116,2)+pow(0.000117863*mu_pt,2));
    // calculate cat 3 scenario, and check cat 0 or 3 has larger sigma
    sigma=(pow(0.066265,2)+pow(0.000210047*mu_pt,2)) - (pow(0.100736,2)+pow(9.78226e-05*mu_pt,2)) > sigma ? (pow(0.066265,2)+pow(0.000210047*mu_pt,2)) - (pow(0.100736,2)+pow(9.78226e-05*mu_pt,2)) : sigma;
    sigma=sigma > 0 ? sigma : 0;
}else if(mu_eta<1.25){
    // calculate cat 1 scenario
    sigma=(pow(4.71779e-06,2)+pow(0.000402803*mu_pt,2)) - (pow(0.101355,2)+pow(0.000257201*mu_pt,2));
    sigma=sigma > 0 ? sigma : 0;
}else if(mu_eta<2){
    // calculate cat 1 scenario
    sigma=(pow(0.0841815,2)+pow(0.000333071*mu_pt,2)) - (pow(0.0914859,2)+pow(0.000284283*mu_pt,2));
    sigma=sigma > 0 ? sigma : 0;
}else{
    // calculate cat 2 scenario
    sigma=(pow(0.117913,2)+pow(0.000498831*mu_pt,2)) - (pow(0.0619713,2)+pow(0.000328653*mu_pt,2));
    sigma=sigma > 0 ? sigma : 0;
}
// calculate smeared pT, i.e. the new pT of the muon
newPt = mu_pt/(1 + gRandom->Gaus(0,sqrt(sigma)));

return newPt*1000.;

} // end of muon smearing

double Comparison::cosThetaStar( TLorentzVector LeptonPlus, TLorentzVector LeptonMinus, TLorentzVector Propagator ){

   double CosThetaStar = 2*((LeptonMinus.Pz()*LeptonPlus.E())-(LeptonPlus.Pz()*LeptonMinus.E()))*(1/((Propagator.Mag())*sqrt((Propagator.Mag()*Propagator.Mag()) + (Propagator.Pt()*Propagator.Pt()))));
   if (Propagator.Pz() < 0) CosThetaStar *= -1; 

  return CosThetaStar;
} // end of function

double Comparison::Zgamma_CT14nnlo_CT10(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 1.16376, 1.10724, 1.0664, 1.05432, 1.04982,
                        1.04639, 1.04096, 1.03119, 1.02177, 1.01925,
                        1.02076, 1.02921, 1.03461, 1.03704, 1.03894,
                        1.04113, 1.04331, 1.0435, 1.04427, 1.04468,
                        1.04422, 1.04439, 1.04381, 1.04306, 1.03941,
                        1.03452, 1.028, 1.02158, 1.01434, 1.00662,
                        0.99796, 0.988199, 0.977731, 0.966273, 0.952347,
                        0.9375, 0.921053, 0.903995, 0.867931, 0.830897,
                        0.796491, 0.764565, 0.736111, 0.708221, 0.675403,
                        0.630872, 0.56498, 0.468275, 0.333641 };
   const double fB[49] = { -0.0187344, -0.010019, -0.00124879, -0.000862026, -0.000278374,
                        -0.000403208, -0.000766901, -0.00109023, -0.000626743, 1.66477e-05,
                        0.000254302, 0.000318283, 0.000135366, 7.99829e-05, 6.44781e-05,
                        4.15938e-05, 3.13527e-05, -3.24053e-06, 1.03244e-05, -2.63095e-06,
                        -1.26508e-06, -9.01635e-07, -7.47064e-06, -9.10973e-06, -1.75214e-05,
                        -2.3261e-05, -2.6286e-05, -2.6892e-05, -3.00765e-05, -3.23832e-05,
                        -3.69782e-05, -4.07285e-05, -4.28575e-05, -5.09485e-05, -5.79558e-05,
                        -6.25087e-05, -6.75384e-05, -6.93981e-05, -7.43036e-05, -7.19739e-05,
                        -6.64383e-05, -6.0269e-05, -5.47663e-05, -5.87241e-05, -7.45862e-05,
                        -0.000107025, -0.000159851, -0.000229153, -0.000311568 };
   const double fC[49] = { 0.0012754, 0.000903448, -2.64227e-05, 6.50987e-05, -6.73341e-06,
                        -5.74997e-06, -3.06194e-05, -1.71345e-06, 4.80621e-05, 1.62769e-05,
                        7.48851e-06, -4.92929e-06, -2.38738e-06, 1.72061e-07, -7.9225e-07,
                        3.34563e-07, -5.39386e-07, 1.93453e-07, -5.78041e-08, -7.17494e-08,
                        8.54081e-08, -8.17736e-08, 1.60835e-08, -3.24744e-08, -1.17232e-09,
                        -2.17862e-08, 9.68616e-09, -1.21102e-08, -6.27411e-10, -8.59942e-09,
                        -9.78087e-09, -5.22004e-09, -3.29607e-09, -2.90681e-08, 1.03915e-09,
                        -1.92508e-08, -8.68025e-10, -6.57068e-09, -3.2404e-09, 7.89982e-09,
                        3.17132e-09, 9.16729e-09, 1.83803e-09, -9.75357e-09, -2.19705e-08,
                        -4.29075e-08, -6.27448e-08, -7.58585e-08, 500 };
   const double fD[49] = { -3.09957e-05, -3.09957e-05, 3.05071e-06, -2.3944e-06, 3.27815e-08,
                        -8.2898e-07, 9.63531e-07, 1.65919e-06, -1.05951e-06, -2.92947e-07,
                        -1.65571e-07, 3.38921e-08, 3.41259e-08, -1.28575e-08, 7.51209e-09,
                        -5.82633e-09, 2.4428e-09, -8.37525e-10, -4.64842e-11, 5.23858e-10,
                        -5.57272e-10, 3.2619e-10, -1.6186e-10, 4.17361e-11, -2.74851e-11,
                        4.19631e-11, -2.90619e-11, 1.53104e-11, -1.06293e-11, -1.57526e-12,
                        6.08111e-12, 2.56528e-12, -3.43627e-11, 4.0143e-11, -2.70533e-11,
                        2.45104e-11, -7.60355e-12, 2.22019e-12, 7.42681e-12, -3.15233e-12,
                        3.99731e-12, -4.88618e-12, -7.72773e-12, -8.14463e-12, -1.3958e-11,
                        -1.32249e-11, -8.74247e-12, -8.74247e-12, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
} // end of function


double Comparison::Zgamma_CT14nnlo_CT10_New(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 1.16376, 1.10724, 1.0664, 1.05432, 1.04982,
                        1.04639, 1.04096, 1.03119, 1.02177, 1.01925,
                        1.02076, 1.02921, 1.03461, 1.03704, 1.03894,
                        1.04113, 1.04331, 1.0435, 1.04427, 1.04468,
                        1.04422, 1.04439, 1.04381, 1.04306, 1.03941,
                        1.03452, 1.028, 1.02158, 1.01434, 1.00662,
                        0.99796, 0.988199, 0.977731, 0.966273, 0.952347,
                        0.9375, 0.921053, 0.903995, 0.867931, 0.830897,
                        0.796491, 0.764565, 0.736111, 0.708221, 0.675403,
                        0.630872, 0.56498, 0.468275, 0.333641 };
   const double fB[49] = { -0.0187344, -0.010019, -0.00124879, -0.000862026, -0.000278374,
                        -0.000403208, -0.000766901, -0.00109023, -0.000626743, 1.66477e-05,
                        0.000254302, 0.000318283, 0.000135366, 7.99829e-05, 6.44781e-05,
                        4.15938e-05, 3.13527e-05, -3.24053e-06, 1.03244e-05, -2.63095e-06,
                        -1.26508e-06, -9.01635e-07, -7.47064e-06, -9.10973e-06, -1.75214e-05,
                        -2.3261e-05, -2.6286e-05, -2.6892e-05, -3.00765e-05, -3.23832e-05,
                        -3.69782e-05, -4.07285e-05, -4.28575e-05, -5.09485e-05, -5.79558e-05,
                        -6.25087e-05, -6.75384e-05, -6.93981e-05, -7.43036e-05, -7.19739e-05,
                        -6.64383e-05, -6.0269e-05, -5.47663e-05, -5.87241e-05, -7.45862e-05,
                        -0.000107025, -0.000159851, -0.000229153, -0.000311568 };
   const double fC[49] = { 0.0012754, 0.000903448, -2.64227e-05, 6.50987e-05, -6.73341e-06,
                        -5.74997e-06, -3.06194e-05, -1.71345e-06, 4.80621e-05, 1.62769e-05,
                        7.48851e-06, -4.92929e-06, -2.38738e-06, 1.72061e-07, -7.9225e-07,
                        3.34563e-07, -5.39386e-07, 1.93453e-07, -5.78041e-08, -7.17494e-08,
                        8.54081e-08, -8.17736e-08, 1.60835e-08, -3.24744e-08, -1.17232e-09,
                        -2.17862e-08, 9.68616e-09, -1.21102e-08, -6.27411e-10, -8.59942e-09,
                        -9.78087e-09, -5.22004e-09, -3.29607e-09, -2.90681e-08, 1.03915e-09,
                        -1.92508e-08, -8.68025e-10, -6.57068e-09, -3.2404e-09, 7.89982e-09,
                        3.17132e-09, 9.16729e-09, 1.83803e-09, -9.75357e-09, -2.19705e-08,
                        -4.29075e-08, -6.27448e-08, -7.58585e-08, 500 };
   const double fD[49] = { -3.09957e-05, -3.09957e-05, 3.05071e-06, -2.3944e-06, 3.27815e-08,
                        -8.2898e-07, 9.63531e-07, 1.65919e-06, -1.05951e-06, -2.92947e-07,
                        -1.65571e-07, 3.38921e-08, 3.41259e-08, -1.28575e-08, 7.51209e-09,
                        -5.82633e-09, 2.4428e-09, -8.37525e-10, -4.64842e-11, 5.23858e-10,
                        -5.57272e-10, 3.2619e-10, -1.6186e-10, 4.17361e-11, -2.74851e-11,
                        4.19631e-11, -2.90619e-11, 1.53104e-11, -1.06293e-11, -1.57526e-12,
                        6.08111e-12, 2.56528e-12, -3.43627e-11, 4.0143e-11, -2.70533e-11,
                        2.45104e-11, -7.60355e-12, 2.22019e-12, 7.42681e-12, -3.15233e-12,
                        3.99731e-12, -4.88618e-12, -7.72773e-12, -8.14463e-12, -1.3958e-11,
                        -1.32249e-11, -8.74247e-12, -8.74247e-12, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT18NNLO_CT14nnlo_Central_spline(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 0.95424, 0.97409, 0.987923, 0.992336, 0.994558,
                        0.996338, 0.998363, 1.00111, 1.00356, 1.00429,
                        1.00376, 1.00211, 1.0014, 1.00118, 1.00119,
                        1.00151, 1.00202, 1.00344, 1.00497, 1.00637,
                        1.00768, 1.00879, 1.00978, 1.01058, 1.01207,
                        1.01288, 1.01304, 1.01256, 1.01135, 1.00935,
                        1.00651, 1.0029, 0.99859, 0.993569, 0.988085,
                        0.98226, 0.976255, 0.9702, 0.958522, 0.948359,
                        0.940648, 0.936246, 0.935625, 0.938784, 0.944825,
                        0.951018, 0.951497, 0.933296, 0.866253 };
   const double fB[49] = { 0.00661725, 0.00348807, 0.00040789, 0.000354207, 0.00016558,
                        0.000184009, 0.000239881, 0.000287327, 0.000168888, -7.91685e-06,
                        -7.49821e-05, -4.83603e-05, -1.50281e-05, -3.4795e-06, 3.1704e-06,
                        8.69927e-06, 1.18505e-05, 1.54768e-05, 1.46868e-05, 1.36568e-05,
                        1.20948e-05, 1.0496e-05, 8.99977e-06, 7.2775e-06, 4.60293e-06,
                        1.91813e-06, -6.17897e-07, -3.3355e-06, -6.37234e-06, -9.66502e-06,
                        -1.29842e-05, -1.58355e-05, -1.87458e-05, -2.11557e-05, -2.26976e-05,
                        -2.37617e-05, -2.42061e-05, -2.41342e-05, -2.21832e-05, -1.81785e-05,
                        -1.23472e-05, -5.11052e-06, 2.65057e-06, 9.73405e-06, 1.36149e-05,
                        9.2129e-06, -1.04343e-05, -7.38057e-05, -0.000205807 };
   const double fC[49] = { -0.000458902, -0.000323394, 1.53762e-05, -2.07445e-05, 1.88187e-06,
                        -3.90418e-08, 5.62624e-06, -8.81597e-07, -1.09623e-05, -6.71814e-06,
                        1.16172e-08, 1.05326e-06, 2.8003e-07, 1.81915e-07, 8.40812e-08,
                        2.64961e-08, 3.65276e-08, -2.64279e-10, -7.63582e-09, -2.66416e-09,
                        -1.29552e-08, -3.03366e-09, -1.19282e-08, -5.29442e-09, -5.40385e-09,
                        -5.33537e-09, -4.80872e-09, -6.06168e-09, -6.08571e-09, -7.08499e-09,
                        -6.19166e-09, -5.21373e-09, -6.42733e-09, -3.21212e-09, -2.95564e-09,
                        -1.30067e-09, -4.76912e-10, 7.64578e-10, 3.13731e-09, 4.87206e-09,
                        6.7905e-09, 7.68296e-09, 7.83922e-09, 6.32772e-09, 1.43392e-09,
                        -1.02379e-08, -2.90566e-08, -9.76861e-08, 500 };
   const double fD[49] = { 1.12923e-05, 1.12923e-05, -1.20402e-06, 7.54212e-07, -6.40303e-08,
                        1.88843e-07, -2.16928e-07, -3.36024e-07, 1.41473e-07, 2.24325e-07,
                        1.38885e-08, -1.03097e-08, -1.3082e-09, -1.30445e-09, -3.83901e-10,
                        6.68769e-11, -1.2264e-10, -2.45718e-11, 1.65722e-11, -3.43034e-11,
                        3.30718e-11, -2.96486e-11, 2.21127e-11, -1.45907e-13, 9.13016e-14,
                        7.02204e-13, -1.67062e-12, -3.20337e-14, -1.33238e-12, 1.19111e-12,
                        1.30392e-12, -1.61814e-12, 4.28695e-12, 3.41966e-13, 2.20663e-12,
                        1.09834e-12, 1.65532e-12, 1.58182e-12, 1.1565e-12, 1.27896e-12,
                        5.94976e-13, 1.04176e-13, -1.00767e-12, -3.26253e-12, -7.78119e-12,
                        -1.25458e-11, -4.5753e-11, -4.5753e-11, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT18ANNLO_CT14nnlo_Central_spline(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 0.951253, 0.969562, 0.981823, 0.985433, 0.98751,
                        0.990199, 0.995454, 1.00475, 1.01448, 1.01752,
                        1.01508, 1.00643, 1.00136, 0.998399, 0.996421,
                        0.993866, 0.992295, 0.990474, 0.989578, 0.989104,
                        0.988821, 0.988647, 0.988486, 0.988269, 0.987581,
                        0.986535, 0.985095, 0.98323, 0.980911, 0.978078,
                        0.974679, 0.970742, 0.966327, 0.961434, 0.956191,
                        0.950703, 0.945083, 0.93944, 0.928506, 0.918764,
                        0.910994, 0.905846, 0.903673, 0.904385, 0.906924,
                        0.908621, 0.903818, 0.880563, 0.81136 };
   const double fB[49] = { 0.00612811, 0.00319567, 0.000316529, 0.000299601, 0.000191396,
                        0.000364627, 0.000733288, 0.00106828, 0.000700621, -4.18583e-05,
                        -0.000353665, -0.000286831, -0.000145404, -9.50862e-05, -6.65148e-05,
                        -3.88833e-05, -2.54875e-05, -1.24423e-05, -6.26898e-06, -3.57209e-06,
                        -2.15913e-06, -1.50384e-06, -1.87253e-06, -2.35111e-06, -3.37232e-06,
                        -4.96895e-06, -6.5851e-06, -8.34211e-06, -1.02583e-05, -1.24518e-05,
                        -1.47171e-05, -1.67127e-05, -1.86547e-05, -2.03617e-05, -2.15305e-05,
                        -2.22925e-05, -2.25959e-05, -2.24817e-05, -2.09545e-05, -1.77566e-05,
                        -1.30916e-05, -7.38422e-06, -1.29699e-06, 3.80723e-06, 5.57819e-06,
                        -7.03402e-07, -2.14013e-05, -8.20383e-05, -0.000205196 };
   const double fC[49] = { -0.000430154, -0.000302955, 1.50414e-05, -1.67342e-05, 5.91376e-06,
                        1.14093e-05, 2.54568e-05, 8.04265e-06, -4.48087e-05, -2.94392e-05,
                        -1.74144e-06, 4.4148e-06, 1.24229e-06, 7.70412e-07, 3.72442e-07,
                        1.80188e-07, 8.77277e-08, 4.27242e-08, 1.90089e-08, 7.9601e-09,
                        6.1695e-09, 3.83382e-10, -4.07031e-09, -7.15462e-10, -3.36939e-09,
                        -3.01715e-09, -3.44744e-09, -3.58058e-09, -4.084e-09, -4.69039e-09,
                        -4.37062e-09, -3.61171e-09, -4.15623e-09, -2.67179e-09, -2.00364e-09,
                        -1.04408e-09, -1.69577e-10, 6.26132e-10, 2.42834e-09, 3.96739e-09,
                        5.36272e-09, 6.05198e-09, 6.12248e-09, 4.08597e-09, -5.44039e-10,
                        -1.20192e-08, -2.93766e-08, -9.18975e-08, 500 };
   const double fD[49] = { 1.05999e-05, 1.05999e-05, -1.05919e-06, 7.54933e-07, 1.83186e-07,
                        4.68248e-07, -5.80471e-07, -1.76171e-06, 5.12316e-07, 9.2326e-07,
                        8.20832e-08, -4.23002e-08, -6.29169e-09, -5.30626e-09, -1.28169e-09,
                        -6.16406e-10, -1.50011e-10, -7.90512e-11, -3.68292e-11, -5.96867e-12,
                        -1.92871e-11, -1.48456e-11, 1.11828e-11, -3.53857e-12, 4.69651e-13,
                        -5.73725e-13, -1.77519e-13, -6.71222e-13, -8.08523e-13, 4.26355e-13,
                        1.01188e-12, -7.26021e-13, 1.97925e-12, 8.9087e-13, 1.27941e-12,
                        1.16601e-12, 1.06095e-12, 1.20147e-12, 1.02603e-12, 9.30224e-13,
                        4.59507e-13, 4.69957e-14, -1.35767e-12, -3.08667e-12, -7.65008e-12,
                        -1.15716e-11, -4.16806e-11, -4.16806e-11, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
} // end of function

double Comparison::Zgamma_CT18ANNLO_CT14nnlo_Up_spline(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 1.09459, 1.08438, 1.0539, 1.04327, 1.03977,
                        1.03356, 1.03524, 1.04445, 1.04938, 1.05002,
                        1.04924, 1.03552, 1.02939, 1.02677, 1.02377,
                        1.02309, 1.01822, 1.02014, 1.02014, 1.02163,
                        1.02377, 1.02541, 1.02852, 1.03033, 1.03655,
                        1.04197, 1.05026, 1.0572, 1.06469, 1.07531,
                        1.08621, 1.09932, 1.11214, 1.12401, 1.13902,
                        1.15172, 1.1672, 1.18274, 1.21613, 1.25866,
                        1.31688, 1.3989, 1.51253, 1.67559, 1.90118,
                        2.20778, 2.62746, 3.15136, 3.73142 };
   const double fB[49] = { -0.00194629, -0.00302765, -0.002235, -0.000366227, -0.000541209,
                        -0.000380372, 0.000703517, 0.000832457, 0.000209942, -1.38072e-06,
                        -0.00024489, -0.000508193, -0.000105045, -0.000122601, -7.82786e-05,
                        -4.40273e-05, -7.89884e-05, 3.50504e-05, -3.63293e-06, 2.41479e-05,
                        1.6028e-05, 2.5038e-05, 2.63153e-05, 1.72138e-05, 2.38517e-05,
                        2.70713e-05, 3.24365e-05, 2.59181e-05, 3.70134e-05, 4.34127e-05,
                        4.76376e-05, 5.41662e-05, 4.67655e-05, 5.50168e-05, 5.57153e-05,
                        5.463e-05, 6.3919e-05, 6.1954e-05, 7.38505e-05, 9.81483e-05,
                        0.000138032, 0.000191211, 0.000271049, 0.00038469, 0.000522057,
                        0.000720229, 0.000954702, 0.00112243, 0.00117938 };
   const double fC[49] = { -0.000185115, -8.52269e-05, 0.000164492, 2.2385e-05, -3.98832e-05,
                        5.59668e-05, 5.24222e-05, -3.95282e-05, -2.27233e-05, 1.59102e-06,
                        -2.59419e-05, 1.54098e-05, 7.16125e-07, -1.41835e-06, 3.19125e-06,
                        -2.50622e-06, 1.807e-06, -6.66612e-07, 2.79779e-07, -1.9708e-09,
                        -7.9228e-08, 1.69327e-07, -1.56554e-07, 6.55395e-08, -3.8988e-08,
                        5.18663e-08, -3.04053e-08, 4.33156e-09, 4.00496e-08, -1.44523e-08,
                        3.13521e-08, -5.23774e-09, -2.43652e-08, 5.73705e-08, -5.45765e-08,
                        5.02353e-08, -1.30791e-08, 5.21902e-09, 1.8574e-08, 3.00217e-08,
                        4.9746e-08, 5.66121e-08, 1.03063e-07, 1.2422e-07, 1.50515e-07,
                        2.45828e-07, 2.23117e-07, 1.12337e-07, 500 };
   const double fD[49] = { 8.32397e-06, 8.32397e-06, -4.73691e-06, -2.07561e-06, 3.195e-06,
                        -1.18155e-07, -3.06501e-06, 5.60165e-07, 8.10476e-07, -9.17765e-07,
                        5.51357e-07, -1.95916e-07, -2.84597e-08, 6.14613e-08, -3.79831e-08,
                        2.87548e-08, -8.24537e-09, 3.15464e-09, -9.39166e-10, -2.57524e-10,
                        8.28518e-10, -1.08627e-09, 7.40312e-10, -1.3937e-10, 1.21139e-10,
                        -1.09695e-10, 4.63158e-11, 4.76241e-11, -7.26693e-11, 6.10725e-11,
                        -4.87864e-11, -2.55033e-11, 1.08981e-10, -1.49263e-10, 1.39749e-10,
                        -8.44192e-11, 2.43975e-11, 8.90329e-12, 7.63179e-12, 1.31496e-11,
                        4.57737e-12, 3.09671e-11, 1.41046e-11, 1.75306e-11, 6.35418e-11,
                        -1.51408e-11, -7.38533e-11, -7.38533e-11, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
} // end of function

double Comparison::Zgamma_CT18ANNLO_CT14nnlo_Down_spline(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -0.0262188, 0.803478, 0.930052, 0.938222, 0.944446,
                        0.948442, 0.9571, 0.971607, 0.978558, 0.979011,
                        0.980194, 0.973831, 0.97036, 0.967525, 0.965057,
                        0.962475, 0.955837, 0.954814, 0.95129, 0.948714,
                        0.94624, 0.943046, 0.941086, 0.937132, 0.928251,
                        0.918571, 0.910058, 0.898557, 0.885959, 0.873489,
                        0.859431, 0.846084, 0.83002, 0.81294, 0.798667,
                        0.781448, 0.765843, 0.750069, 0.716752, 0.683938,
                        0.64883, 0.610886, 0.564413, 0.513703, 0.454015,
                        0.382774, 0.302551, 0.209365, 0.116645 };
   const double fB[49] = { 0.304812, 0.121962, -0.0221115, 0.00690698, -0.00119833,
                        0.000952337, 0.00118523, 0.0012563, 0.000227126, 5.64174e-05,
                        3.77724e-05, -0.000282266, -8.87067e-05, -0.000119702, -6.89017e-05,
                        -9.4338e-05, -0.00010695, 3.04225e-06, -4.16207e-05, -1.9553e-05,
                        -3.16699e-05, -2.37939e-05, -2.77804e-05, -4.25151e-05, -3.60664e-05,
                        -3.59492e-05, -3.84594e-05, -5.03821e-05, -4.91949e-05, -5.36567e-05,
                        -5.45172e-05, -5.71299e-05, -6.98975e-05, -6.10068e-05, -6.23102e-05,
                        -6.76543e-05, -6.09528e-05, -6.50866e-05, -6.60606e-05, -6.74588e-05,
                        -7.16356e-05, -8.43095e-05, -9.76276e-05, -0.000108276, -0.000131655,
                        -0.000150679, -0.000174416, -0.000192117, -0.000172552 };
   const double fC[49] = { -0.0273284, -0.0183841, 0.0039767, -0.00107486, 0.000264324,
                        -4.92568e-05, 7.25464e-05, -6.54401e-05, -3.7477e-05, 2.04061e-05,
                        -2.22706e-05, 9.46912e-06, -1.72676e-06, 4.86943e-07, 1.54508e-06,
                        -2.0538e-06, 1.80157e-06, -7.01646e-07, 2.55016e-07, -3.43378e-08,
                        -8.68318e-08, 1.65592e-07, -2.05457e-07, 5.811e-08, -3.23151e-08,
                        3.27838e-08, -4.28245e-08, -4.86649e-09, 9.61525e-09, -2.74624e-08,
                        2.40205e-08, -3.44713e-08, -1.6599e-08, 5.21615e-08, -5.73749e-08,
                        3.59985e-08, -9.19256e-09, -7.34291e-09, 5.39508e-09, -8.19151e-09,
                        -1.62159e-10, -2.51856e-08, -1.45072e-09, -1.98464e-08, -2.69105e-08,
                        -1.11387e-08, -3.63343e-08, 9.31773e-10, 500 };
   const double fD[49] = { 0.000745359, 0.000745359, -0.000168385, 4.46393e-05, -1.04527e-05,
                        4.06011e-06, -4.59955e-06, 9.32102e-07, 1.92944e-06, -1.42256e-06,
                        4.23197e-07, -1.49278e-07, 2.95161e-08, 1.41084e-08, -2.39925e-08,
                        2.57024e-08, -8.34404e-09, 3.18887e-09, -9.64512e-10, -1.7498e-10,
                        8.41412e-10, -1.23683e-09, 8.78556e-10, -1.20567e-10, 8.67985e-11,
                        -1.00811e-10, 5.06107e-11, 1.9309e-11, -4.94368e-11, 6.86438e-11,
                        -7.7989e-11, 2.38298e-11, 9.16806e-11, -1.46048e-10, 1.24498e-10,
                        -6.02548e-11, 2.4662e-12, 8.49199e-12, -9.05773e-12, 5.3529e-12,
                        -1.66823e-11, 1.58232e-11, -1.22638e-11, -4.70937e-12, 1.05145e-11,
                        -1.6797e-11, 2.4844e-11, 2.4844e-11, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
} // end of function

double Comparison:: ew_z_spline(double x) {
   const int fNp = 40, fKstep = 0;
   const double fDelta = -1, fXmin = 9, fXmax = 9704;
   const double fX[40] = { 9, 14, 18, 24, 28,
                        34, 38, 44, 48, 54,
                        58, 64, 71, 81, 91,
                        106, 128, 154, 186, 225,
                        272, 328, 396, 478, 576,
                        696, 840, 1014, 1224, 1478,
                        1784, 2153, 2599, 3138, 3787,
                        4571, 5518, 6660, 8039, 9704 };
   const double fY[40] = { 1.0119, 1.0242, 1.0308, 1.0366, 1.0415,
                        1.0456, 1.0493, 1.0524, 1.0546, 1.0556,
                        1.0545, 1.0503, 1.0363, 1.01, 0.99577,
                        0.98928, 0.99052, 0.99511, 0.99753, 0.99772,
                        0.9968, 0.99311, 0.98831, 0.98457, 0.97896,
                        0.97158, 0.9626, 0.95198, 0.93982, 0.92616,
                        0.91084, 0.89381, 0.87491, 0.8539, 0.83065,
                        0.80546, 0.77913, 0.75309, 0.72915, 0.71127 };
   const double fB[40] = { 0.00282782, 0.00204652, 0.00122426, 0.00113392, 0.00101244,
                        0.000799439, 0.000801912, 0.000503781, 0.000469457, -0.000127954,
                        -0.000378124, -0.00125495, -0.00257074, -0.00218217, -0.000859562,
                        -0.000130429, 0.000169497, 0.000143971, 2.77654e-05, -7.55436e-06,
                        -4.12542e-05, -7.78298e-05, -5.70337e-05, -4.67198e-05, -6.19888e-05,
                        -6.17925e-05, -6.22099e-05, -5.97362e-05, -5.59572e-05, -5.19204e-05,
                        -4.82114e-05, -4.42668e-05, -4.06637e-05, -3.74677e-05, -3.41508e-05,
                        -3.01467e-05, -2.54682e-05, -2.0277e-05, -1.44061e-05, -7.01641e-06 };
   const double fC[40] = { -6.44344e-05, -9.18261e-05, -0.000113739, 9.86823e-05, -0.000129052,
                        9.35525e-05, -9.29342e-05, 4.32456e-05, -5.18265e-05, -4.77419e-05,
                        -1.48005e-05, -0.000131337, -5.66338e-05, 9.54909e-05, 3.67702e-05,
                        1.18387e-05, 1.79427e-06, -2.77602e-06, -8.5541e-07, -5.02238e-08,
                        -6.66793e-07, 1.36563e-08, 2.92169e-07, -1.6639e-07, 1.05842e-08,
                        -8.94791e-09, 6.04903e-09, 8.16782e-09, 9.82737e-09, 6.06561e-09,
                        6.05513e-09, 4.63494e-09, 3.44382e-09, 2.48563e-09, 2.62506e-09,
                        2.48231e-09, 2.45802e-09, 2.08768e-09, 2.16965e-09, 1665 };
   const double fD[40] = { -1.82611e-06, -1.82611e-06, 1.18012e-05, -1.89779e-05, 1.23669e-05,
                        -1.55406e-05, 7.56554e-06, -7.92267e-06, 2.26922e-07, 2.74512e-06,
                        -6.47424e-06, 3.55729e-06, 5.07083e-06, -1.95736e-06, -5.54033e-07,
                        -1.52188e-07, -5.85934e-08, 2.00063e-08, 6.88194e-09, -4.37283e-09,
                        4.05029e-09, 1.36526e-09, -1.86406e-09, 6.01955e-10, -5.42559e-11,
                        3.47151e-11, 4.05898e-12, 2.6342e-12, -4.93669e-12, -1.14174e-14,
                        -1.28292e-12, -8.90221e-13, -5.92576e-13, 7.16144e-14, -6.06954e-14,
                        -8.55e-15, -1.08096e-13, 1.98141e-14, 1.98141e-14, 640.501 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}


double Comparison::z_all_pi_ave_spline(double x) {
   const int fNp = 28, fKstep = 0;
   const double fDelta = -1, fXmin = 64, fXmax = 9704;
   const double fX[28] = { 64, 71, 81, 91, 106,
                        128, 154, 186, 225, 272,
                        328, 396, 478, 576, 696,
                        840, 1014, 1224, 1478, 1784,
                        2397, 3138, 3787, 4571, 5518,
                        6660, 8039, 9704 };
   const double fY[28] = { -0.25035, 0.26341, 0.0951, -0.0058996, 0.112237,
                        0.70107, 1.31431, 1.88632, 2.41409, 2.88836,
                        3.3023, 3.65166, 3.94552, 4.21875, 4.48816,
                        4.77903, 5.11665, 5.53411, 6.07457, 6.79179,
                        8.16411, 11.3991, 14.3567, 19.1715, 28.3837,
                        49.8721, 118.526, 522.289 };
   const double fB[28] = { 0.142374, 0.0175202, -0.0244358, -0.000569987, 0.0176811,
                        0.0285581, 0.0201396, 0.0157104, 0.0116941, 0.00869104,
                        0.00621906, 0.00424521, 0.00310885, 0.00249093, 0.00208846,
                        0.00196356, 0.00194716, 0.00202503, 0.00229281, 0.00217215,
                        0.00316197, 0.00462166, 0.00503407, 0.00712567, 0.0138913,
                        0.0217625, 0.104664, 0.419486 };
   const double fC[28] = { -0.0117265, -0.00610976, 0.00191417, 0.000472413, 0.000744326,
                        -0.000249918, -7.38673e-05, -6.45458e-05, -3.84354e-05, -2.54604e-05,
                        -1.86821e-05, -1.0345e-05, -3.51309e-06, -2.79219e-06, -5.61751e-07,
                        -3.05554e-07, 2.11276e-07, 1.59523e-07, 8.94748e-07, -1.28907e-06,
                        2.90378e-06, -9.33878e-07, 1.56933e-06, 1.09853e-06, 6.04579e-06,
                        8.46679e-07, 5.92706e-05, 1665 };
   const double fD[28] = { 0.000267464, 0.000267464, -4.80584e-05, 6.04249e-06, -1.50643e-05,
                        2.25706e-06, 9.70988e-08, 2.23166e-07, 9.20211e-08, 4.03471e-08,
                        4.08682e-08, 2.77721e-08, 2.45205e-09, 6.19566e-09, 5.93047e-10,
                        9.90097e-10, -8.21481e-11, 9.64863e-10, -2.37888e-09, 2.27996e-09,
                        -1.72634e-09, 1.28567e-09, -2.0017e-10, 1.74138e-09, -1.51755e-09,
                        1.41223e-08, 1.41223e-08, 640.5 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT10nnlo_CT10nnlo(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 1.43722, 1.28309, 1.18981, 1.16425, 1.1536,
                        1.14944, 1.1463, 1.14286, 1.14096, 1.1421,
                        1.14274, 1.14808, 1.152, 1.15565, 1.15924,
                        1.16504, 1.17239, 1.18145, 1.19101, 1.1994,
                        1.20738, 1.21468, 1.22187, 1.22864, 1.24605,
                        1.26258, 1.27929, 1.29578, 1.31247, 1.32941,
                        1.34556, 1.36272, 1.38113, 1.40039, 1.41934,
                        1.44049, 1.463, 1.48459, 1.53471, 1.58914,
                        1.64902, 1.71068, 1.776, 1.8393, 1.90196,
                        1.96498, 2.02435, 2.07322, 2.0872 };
   const double fB[49] = { -0.0521994, -0.0263848, -0.00177992, -0.00214878, -0.00048811,
                        -0.000343171, -0.000328294, -0.000317046, -5.47055e-06, 0.000112669,
                        8.73371e-05, 0.000222832, 0.000133542, 0.00015126, 0.0001292,
                        0.000131301, 0.000134845, 8.24348e-05, 9.40102e-05, 8.01234e-05,
                        7.65015e-05, 7.21623e-05, 6.97824e-05, 6.76581e-05, 6.84254e-05,
                        6.59483e-05, 6.66344e-05, 6.59053e-05, 6.79387e-05, 6.58757e-05,
                        6.56569e-05, 7.12238e-05, 7.62536e-05, 7.58138e-05, 7.89944e-05,
                        8.93417e-05, 8.75406e-05, 8.96729e-05, 0.000105776, 0.000114573,
                        0.000121771, 0.000127586, 0.0001298, 0.00012489, 0.000126394,
                        0.000123661, 0.000113315, 7.24994e-05, -2.62571e-05 };
   const double fC[49] = { 0.00379727, 0.00265637, -0.000195883, 0.000158997, 7.07053e-06,
                        7.42338e-06, -5.9357e-06, 7.06056e-06, 2.4097e-05, -1.2283e-05,
                        9.74973e-06, -4.32993e-06, 7.58313e-07, -4.9597e-08, -8.32804e-07,
                        8.74831e-07, -8.03948e-07, 2.79844e-07, -1.6409e-07, 2.52222e-08,
                        -6.14412e-08, 1.80497e-08, -4.18491e-08, 2.0607e-08, -1.75379e-08,
                        7.62952e-09, -4.88535e-09, 1.96923e-09, 6.16412e-09, -1.44159e-08,
                        1.35404e-08, 8.72725e-09, 1.13922e-08, -1.31514e-08, 2.58737e-08,
                        1.55156e-08, -2.272e-08, 3.12491e-08, 9.56937e-10, 1.66372e-08,
                        -2.2419e-09, 1.3872e-08, -9.44403e-09, -3.75533e-10, 3.38327e-09,
                        -8.84785e-09, -1.18451e-08, -6.9786e-08, 500 };
   const double fD[49] = { -9.50752e-05, -9.50752e-05, 1.18293e-05, -5.06421e-06, 1.17619e-08,
                        -4.45303e-07, 4.33209e-07, 5.67881e-07, -1.21266e-06, 7.34423e-07,
                        -1.87729e-07, 6.78432e-08, -1.07721e-08, -1.04428e-08, 1.13842e-08,
                        -1.11919e-08, 3.61264e-09, -1.47978e-09, 6.31042e-10, -2.88878e-10,
                        2.6497e-10, -1.99663e-10, 2.08187e-10, -5.08598e-11, 3.35565e-11,
                        -1.66865e-11, 9.13943e-12, 5.5932e-12, -2.744e-11, 3.7275e-11,
                        -6.41755e-12, 3.5532e-12, -3.27248e-11, 5.20335e-11, -1.38109e-11,
                        -5.09808e-11, 7.19588e-11, -2.01948e-11, 1.04535e-11, -1.2586e-11,
                        1.07426e-11, -1.5544e-11, 6.04566e-12, 2.50587e-12, -8.15408e-12,
                        -1.99815e-12, -3.86273e-11, -3.86273e-11, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
} 

double Comparison::Zgamma_CT14nnlo_EWbun_EV_7(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 0.19067, -6.98113, 1.1208, 1.19289, 0.697837,
                        0.860024, 0.7087, 0.621118, 0.386255, 0.196773,
                        0.227125, 0.280816, 0.361664, 0.440176, 0.339047,
                        0.4158, 0.316641, 0.340281, 0.327907, 0.299646,
                        0.338792, 0.27978, 0.288795, 0.262123, 0.359066,
                        0.522345, 1.00358, 1.50671, 2.19159, 2.99489,
                        4.04498, 5.118, 6.18221, 7.25552, 8.3648,
                        9.40972, 10.4762, 11.4877, 13.3001, 14.8562,
                        15.9912, 16.9364, 17.2759, 17.1286, 16.3104,
                        14.0831, 10.0654, 2.51908, -11.826 };
   const double fB[49] = { -3.2836, -0.5157, 0.802346, -0.241477, 0.0366754,
                        -0.00508515, -0.0130758, -0.0142832, -0.026525, -0.00692045,
                        0.00646795, 0.0012326, 0.00474625, -0.00109437, -0.00308279,
                        0.00101969, -0.00234031, 0.000812577, -0.000572016, 0.00025644,
                        -0.000127211, -0.000343557, 1.53111e-06, -0.000192286, 0.000505128,
                        0.00129443, 0.00205135, 0.0023126, 0.00295428, 0.00372834,
                        0.00437302, 0.00425693, 0.00424611, 0.00440889, 0.00430942,
                        0.00420384, 0.00421186, 0.00388491, 0.00341819, 0.00265304,
                        0.00211611, 0.00136361, 0.000137975, -0.00076188, -0.00288349,
                        -0.00597754, -0.0106766, -0.0207, -0.0378718 };
   const double fC[49] = { 0.426012, 0.265963, -0.134159, 0.0297762, -0.00196096,
                        -0.0022151, 0.00141603, -0.00153677, 0.000312587, 0.00164786,
                        -0.000309025, 9.96113e-05, 4.09345e-05, -0.000274559, 0.000195022,
                        -0.000112972, 4.57723e-05, -1.42434e-05, 3.97502e-07, 7.88705e-06,
                        -1.17236e-05, 9.56011e-06, -6.10923e-06, 4.17105e-06, -1.3814e-06,
                        4.53862e-06, -1.51095e-06, 2.55595e-06, 1.07666e-08, 3.08549e-06,
                        -5.06799e-07, 4.24314e-08, -8.56806e-08, 7.36785e-07, -1.13465e-06,
                        7.12325e-07, -6.80264e-07, -6.27503e-07, -3.05945e-07, -1.22436e-06,
                        1.50513e-07, -1.65552e-06, -7.95752e-07, -1.00396e-06, -3.23926e-06,
                        -2.94884e-06, -6.44928e-06, -1.35976e-05, 500 };
   const double fD[49] = { -0.0133374, -0.0133374, 0.00546449, -0.00105791, -8.47141e-06,
                        0.000121038, -9.84265e-05, 6.16451e-05, 4.45093e-05, -6.52297e-05,
                        5.44849e-06, -7.82358e-07, -4.20658e-06, 6.26108e-06, -2.0533e-06,
                        1.0583e-06, -2.00052e-07, 4.88031e-08, 2.49652e-08, -6.53687e-08,
                        7.09455e-08, -5.22311e-08, 3.42676e-08, -7.40327e-09, 7.89336e-09,
                        -8.0661e-09, 5.42253e-09, -3.39358e-09, 4.09963e-09, -4.78972e-09,
                        7.32307e-10, -1.70816e-10, 1.09662e-09, -2.49525e-09, 2.46264e-09,
                        -1.85679e-09, 7.03483e-11, 2.14372e-10, -6.12279e-10, 9.16584e-10,
                        -1.20402e-09, 5.73176e-10, -1.38804e-10, -1.4902e-09, 1.93611e-10,
                        -2.33362e-09, -4.76555e-09, -4.76555e-09, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_EWbun_EV_6(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -6.38109, -2.7673, -1.24533, -1.06599, -0.628053,
                        -0.548636, -0.268817, 0.270051, 0.45285, 0.590319,
                        0.573221, 0.206917, -0.0904159, -0.260104, -0.414391,
                        -0.623701, -0.903014, -1.14845, -1.39361, -1.57995,
                        -1.69396, -1.78629, -1.80978, -1.83486, -1.76365,
                        -1.68311, -1.43369, -1.29381, -1.13114, -1.02264,
                        -0.934579, -0.881433, -0.867679, -0.966724, -1.0574,
                        -1.21528, -1.39683, -1.59648, -2.18475, -2.94042,
                        -3.78855, -4.71328, -5.7783, -6.87041, -8.02034,
                        -9.06788, -9.86636, -10.1718, -8.94282 };
   const double fB[49] = { 1.2593, 0.587937, -0.031376, 0.0479585, 0.0247252,
                        0.00834701, 0.0496575, 0.0386291, 0.0123265, 0.0081452,
                        -0.00879615, -0.0155699, -0.00856065, -0.00622999, -0.00539634,
                        -0.00474939, -0.00492352, -0.0018407, -0.00243143, -0.00137867,
                        -0.00106447, -0.000553661, -0.000195559, -0.000121252, 0.000311072,
                        0.000697982, 0.000856495, 0.000547655, 0.000583485, 0.000372386,
                        0.00028572, 0.00017927, -0.000199994, -0.000402791, -0.000465516,
                        -0.000717784, -0.000736429, -0.000910963, -0.00138272, -0.00162179,
                        -0.00175294, -0.00200359, -0.00217125, -0.00225423, -0.00226403,
                        -0.00187448, -0.00131421, 0.000508091, 0.00482308 };
   const double fC[49] = { -0.099051, -0.0687909, 0.00685955, 0.00107389, -0.00339722,
                        0.0017594, 0.00237165, -0.00347449, 0.000844235, -0.00126236,
                        -0.000431771, 0.000160821, 0.000119549, -2.63228e-05, 5.96691e-05,
                        -4.67301e-05, 4.32474e-05, -1.24192e-05, 6.51182e-06, 4.01585e-06,
                        -8.73829e-07, 5.98188e-06, -2.40086e-06, 3.14393e-06, -1.41464e-06,
                        2.96228e-06, -2.32823e-06, 1.09287e-06, -9.49545e-07, 1.05147e-07,
                        -4.51811e-07, 2.60111e-08, -1.54307e-06, 7.31875e-07, -9.82777e-07,
                        -2.62945e-08, -4.82845e-08, -6.49852e-07, -2.9366e-07, -1.84476e-07,
                        -7.78262e-08, -4.23472e-07, 8.81539e-08, -2.54125e-07, 2.34528e-07,
                        5.44574e-07, 5.7596e-07, 3.06865e-06, 500 };
   const double fD[49] = { 0.00252168, 0.00252168, -0.000192855, -0.000149037, 0.000171888,
                        2.04081e-05, -0.000194871, 0.000143958, -7.022e-05, 2.76864e-05,
                        7.90122e-06, -5.50288e-07, -1.94496e-06, 1.14656e-06, -7.09329e-07,
                        5.9985e-07, -1.85555e-07, 6.31033e-08, -8.31991e-09, -1.62989e-08,
                        2.28524e-08, -2.79425e-08, 1.84826e-08, -6.07809e-09, 5.83588e-09,
                        -7.054e-09, 4.56146e-09, -2.72321e-09, 1.40626e-09, -7.4261e-10,
                        6.37096e-10, -2.0921e-09, 3.03325e-09, -2.2862e-09, 1.27531e-09,
                        -2.932e-11, -8.0209e-10, 2.37461e-10, 7.27898e-11, 7.10995e-11,
                        -2.3043e-10, 3.41084e-10, -2.28186e-10, 3.25769e-10, 2.06698e-10,
                        2.09242e-11, 1.66179e-09, 1.66179e-09, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_EWbun_EV_5(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -1.37282, 0.314465, 0.933998, 0.913706, 1.11654,
                        0.874852, 0.684262, 0.270051, -0.492808, -0.669028,
                        -0.43262, 0.103458, 0.406872, 0.640256, 0.772273,
                        0.831601, 0.820922, 0.935772, 0.901745, 0.871697,
                        0.846979, 0.731734, 0.693107, 0.589777, 0.40131,
                        0.319211, 0.501792, 0.720603, 1.09579, 1.60701,
                        2.20502, 2.84333, 3.36226, 3.6939, 4.1352,
                        4.27083, 4.25397, 4.00278, 2.64469, 0.115562,
                        -4.05286, -10.055, -18.9269, -31.7934, -50.8819,
                        -80.3951, -128.226, -210.84, -365.841 };
   const double fB[49] = { 0.591416, 0.271307, -0.0281617, 0.021112, -0.00152387,
                        -0.0266727, -0.0214685, -0.0688934, -0.0560788, 0.0114848,
                        0.0281959, 0.0155524, 0.0103335, 0.00752942, 0.00339707,
                        -0.00019746, 0.000311664, 0.000688954, -0.000642785, -4.00598e-05,
                        -0.00083995, -0.000799045, -0.00058002, -0.00113957, -0.000584357,
                        0.000230196, 0.000869364, 0.00110905, 0.00182244, 0.00223809,
                        0.00253596, 0.0024539, 0.00153521, 0.00161211, 0.00129164,
                        0.000144461, -0.000444227, -0.00158424, -0.00378324, -0.00660608,
                        -0.00997776, -0.0145062, -0.0212417, -0.0309577, -0.0466576,
                        -0.0740221, -0.121317, -0.223377, -0.410868 };
   const double fC[49] = { -0.0471679, -0.0328593, 0.00291239, 0.00201498, -0.00427857,
                        0.00176369, -0.00124328, -0.00349921, 0.00478067, 0.0019757,
                        -0.000304592, -0.000201147, -7.60927e-06, -0.000104553, -6.07409e-05,
                        -1.11498e-05, 2.13322e-05, -1.75593e-05, 4.24195e-06, 1.7853e-06,
                        -9.78421e-06, 1.01933e-05, -8.003e-06, 2.40752e-06, -1.86676e-07,
                        3.44489e-06, -8.88214e-07, 1.84697e-06, 1.0066e-06, 6.55975e-07,
                        5.35522e-07, -8.63754e-07, -2.81101e-06, 3.11858e-06, -4.40043e-06,
                        -1.88307e-07, -2.16645e-06, -2.39359e-06, -2.00442e-06, -3.64126e-06,
                        -3.1021e-06, -5.95474e-06, -7.51621e-06, -1.19159e-05, -1.94838e-05,
                        -3.52451e-05, -5.93448e-05, -0.000144775, 500 };
   const double fD[49] = { 0.00119239, 0.00119239, -2.99134e-05, -0.000209785, 0.000201409,
                        -0.000100232, -7.51978e-05, 0.000275996, -9.34991e-05, -7.60096e-05,
                        1.37926e-06, 2.58051e-06, -1.29258e-06, 5.84161e-07, 3.30608e-07,
                        2.16547e-07, -1.29639e-07, 7.2671e-08, -8.18883e-09, -3.8565e-08,
                        6.65916e-08, -6.06542e-08, 3.47017e-08, -3.45893e-09, 4.84208e-09,
                        -5.77747e-09, 3.64691e-09, -1.12049e-09, -4.67501e-10, -1.60604e-10,
                        -1.8657e-09, -2.59633e-09, 7.90611e-09, -1.00253e-08, 5.61616e-09,
                        -2.63752e-09, -3.02863e-10, 2.59449e-10, -1.09123e-09, 3.59444e-10,
                        -1.90177e-09, -1.04098e-09, -2.93315e-09, -5.04527e-09, -1.05075e-08,
                        -1.60665e-08, -5.69537e-08, -5.69537e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_EWbun_EV_4(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 0.203381, 0, -0.809465, -0.812183, -0.837404,
                        -0.874852, -0.928641, -0.918174, -1.07885, -0.905155,
                        -0.854424, -0.753769, -0.678119, -0.640256, -0.546242,
                        -0.363825, -0.234549, 0.255211, 0.679236, 1.11686,
                        1.58103, 2.00151, 2.46438, 2.85059, 3.85468,
                        4.7011, 5.44803, 6.04324, 6.50407, 6.79328,
                        7.12617, 7.3358, 7.37527, 7.25552, 7.17523,
                        7.01389, 6.73016, 6.38593, 5.28938, 3.86492,
                        2.02643, -0.455617, -3.89151, -8.4814, -15.0861,
                        -24.924, -40.3753, -66.3931, -114.072 };
   const double fB[49] = { -0.019085, -0.0759884, -0.0445474, 0.0105233, -0.0059276,
                        -0.00561358, 0.00101075, -0.0114263, -0.000368144, 0.0168045,
                        0.00047775, 0.00477194, 0.00159101, 0.00248557, 0.00429197,
                        0.00278538, 0.00326812, 0.00502649, 0.00403948, 0.00466513,
                        0.00435373, 0.00445929, 0.00430975, 0.00377422, 0.0038208,
                        0.00314872, 0.00270446, 0.00213906, 0.00141175, 0.00121446,
                        0.00119564, 0.000513208, -0.000259233, -0.0004396, -0.000382901,
                        -0.000928377, -0.00124441, -0.00162947, -0.00257507, -0.00319632,
                        -0.00421734, -0.00585754, -0.00786016, -0.0108565, -0.0158814,
                        -0.0242737, -0.0387591, -0.0695046, -0.125405 };
   const double fC[49] = { -0.00959435, -0.0046315, 0.00777561, -0.00226854, 0.000623448,
                        -0.000592046, 0.00125448, -0.00249818, 0.00360399, -0.00188672,
                        0.000254045, -8.22776e-05, -4.49597e-05, 8.07422e-05, -8.48612e-06,
                        -2.16458e-05, 3.13006e-05, -1.37169e-05, 3.84674e-06, 2.4098e-06,
                        -5.52382e-06, 6.5794e-06, -8.07481e-06, 2.71953e-06, -2.5332e-06,
                        -1.55138e-07, -1.62192e-06, -6.39676e-07, -2.26955e-06, 1.4804e-06,
                        -1.55571e-06, -1.174e-06, -1.91576e-06, 1.1943e-06, -9.67498e-07,
                        -1.21441e-06, -4.97118e-08, -1.49056e-06, -4.00628e-07, -8.41885e-07,
                        -1.20015e-06, -2.08024e-06, -1.92501e-06, -4.06773e-06, -5.98192e-06,
                        -1.08027e-05, -1.81681e-05, -4.33228e-05, 500 };
   const double fD[49] = { 0.00041357, 0.00041357, -0.000334805, 9.63995e-05, -4.05164e-05,
                        6.15508e-05, -0.000125089, 0.000203406, -0.000183024, 7.13589e-05,
                        -4.4843e-06, 4.97572e-07, 1.67602e-06, -1.18971e-06, -8.77313e-08,
                        3.52976e-07, -1.50058e-07, 5.85454e-08, -4.7898e-09, -2.64454e-08,
                        4.03441e-08, -4.88474e-08, 3.59811e-08, -7.00364e-09, 3.17075e-09,
                        -1.95571e-09, 1.30966e-09, -2.17317e-09, 4.99993e-09, -4.04815e-09,
                        5.08946e-10, -9.8902e-10, 4.14675e-09, -2.88239e-09, -3.29213e-10,
                        1.55293e-09, -1.92113e-09, 7.26621e-10, -2.94171e-10, -2.38841e-10,
                        -5.86732e-10, 1.03492e-10, -1.42848e-09, -1.27613e-09, -3.21387e-09,
                        -4.91027e-09, -1.67698e-08, -1.67698e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_EWbun_EV_3(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -5.504, -0.377358, 0.311333, 0.126904, 0.279135,
                        0.207592, 0.0733138, -0.162031, -0.61268, -0.747737,
                        -0.540774, -0.251256, -0.045208, 0.240096, 0.263703,
                        0.467775, 0.469098, 0.723097, 0.878323, 1.03514,
                        1.18577, 1.29129, 1.46323, 1.6055, 2.03823,
                        2.52467, 3.08244, 3.58664, 4.10039, 4.52885,
                        5.05257, 5.48763, 5.85683, 6.13616, 6.43882,
                        6.66667, 6.85714, 6.9528, 7.01418, 6.7283,
                        5.94714, 4.52474, 1.76887, -2.81924, -10.5416,
                        -23.9615, -47.7396, -91.7805, -178.938 };
   const double fB[49] = { 1.8832, 0.752987, -0.159844, 0.0376681, -0.000487469,
                        -0.0115117, -0.015212, -0.038527, -0.0364782, 0.0087278,
                        0.0231386, 0.00617442, 0.0116317, 0.00626108, 0.000393312,
                        0.00302801, -0.000181663, 0.00281263, 0.0012079, 0.0017171,
                        0.00114712, 0.00137903, 0.00166043, 0.00140553, 0.00187371,
                        0.00212955, 0.00213857, 0.00205982, 0.00183758, 0.00189647,
                        0.00200271, 0.00159801, 0.0012564, 0.00115869, 0.00109271,
                        0.000836604, 0.000580727, 0.000274085, -0.000141908, -0.00105345,
                        -0.00204656, -0.00398163, -0.00709654, -0.0116961, -0.0199819,
                        -0.0352298, -0.0622867, -0.122538, -0.234752 };
   const double fC[49] = { -0.168601, -0.113952, 0.0226692, -0.00291794, -0.00089762,
                        -0.000204802, -0.000165233, -0.00216626, 0.00237114, 0.00214946,
                        -0.000708379, 2.98109e-05, 0.00018848, -0.000403305, 0.000168594,
                        -0.0001159, 5.17065e-05, -2.17636e-05, 5.71625e-06, -6.24166e-07,
                        -5.07564e-06, 7.39472e-06, -4.58069e-06, 2.03169e-06, -1.58974e-07,
                        1.18233e-06, -1.14627e-06, 8.3127e-07, -1.72023e-06, 1.95579e-06,
                        -1.53081e-06, -8.8003e-08, -1.27845e-06, 8.87623e-07, -1.15154e-06,
                        1.27118e-07, -1.15063e-06, -7.59387e-08, -7.56048e-07, -1.06703e-06,
                        -9.19194e-07, -2.95093e-06, -3.2789e-06, -5.9202e-06, -1.06515e-05,
                        -1.98442e-05, -3.42695e-05, -8.62328e-05, 500 };
   const double fD[49] = { 0.00455405, 0.00455405, -0.000852903, 6.73439e-05, 2.30939e-05,
                        1.319e-06, -6.67009e-05, 0.000151247, -7.38924e-06, -9.52613e-05,
                        9.84253e-06, 2.11559e-06, -7.89047e-06, 7.62532e-06, -1.89663e-06,
                        1.11738e-06, -2.449e-07, 9.15995e-08, -2.11347e-08, -1.48382e-08,
                        4.15679e-08, -3.9918e-08, 2.20413e-08, -2.92089e-09, 1.7884e-09,
                        -3.10479e-09, 2.63671e-09, -3.40199e-09, 4.90135e-09, -4.6488e-09,
                        1.92375e-09, -1.58727e-09, 2.8881e-09, -2.71889e-09, 1.70488e-09,
                        -1.70366e-09, 1.43292e-09, -4.53406e-10, -2.07324e-10, 9.85601e-11,
                        -1.35449e-09, -2.18648e-10, -1.76086e-09, -3.15418e-09, -6.1285e-09,
                        -9.61687e-09, -3.46422e-08, -3.46422e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_EWbun_EV_2(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 2.55498, 1.57233, 0.249066, -0.152284, -0.348918,
                        -0.474496, -0.391007, -0.162031, -0.0799148, 0.0393546,
                        0.0865239, -0.280816, -0.406872, -0.35014, -0.395555,
                        -0.363825, -0.457371, -0.255211, -0.199087, -0.0817216,
                        0.0564653, 0.150651, 0.288795, 0.42595, 0.707572,
                        1.01567, 1.43369, 1.76875, 2.29763, 2.92184,
                        3.85514, 4.97583, 6.12798, 7.5608, 9.15785,
                        10.9722, 13.0794, 15.2938, 20.4293, 26.6307,
                        33.8767, 42.7023, 53.5377, 67.1879, 85.2044,
                        110.233, 147.341, 206.679, 313.748 };
   const double fB[49] = { -0.286243, -0.207424, -0.0718682, -0.0224863, -0.017582,
                        -0.00384915, 0.0203521, 0.0161804, 0.00825398, 0.0112193,
                        -0.00319952, -0.0143554, 0.00141349, 0.0003825, -0.00158546,
                        -0.000247995, -0.00113152, 0.00212446, 0.000382189, 0.00155145,
                        0.00107857, 0.00110546, 0.00146948, 0.00127559, 0.00106328,
                        0.00154792, 0.00145849, 0.00165512, 0.00228832, 0.00302867,
                        0.00428712, 0.00447075, 0.005104, 0.00613288, 0.00672296,
                        0.00791234, 0.00868582, 0.00920386, 0.0113653, 0.013356,
                        0.0158949, 0.0194937, 0.0240969, 0.0310324, 0.0417736,
                        0.060144, 0.0904729, 0.156643, 0.281396 };
   const double fC[49] = { 0.0107307, 0.00897387, 0.00458175, 0.000356435, 0.000133992,
                        0.00123929, 0.00118083, -0.001598, 0.000805355, -0.000508825,
                        -0.000933055, 0.000486821, 0.000143932, -0.000185172, 0.000106454,
                        -7.97042e-05, 6.20338e-05, -2.9474e-05, 1.20512e-05, -3.58604e-07,
                        -4.37022e-06, 4.63913e-06, -9.98903e-07, -9.40008e-07, 9.0781e-08,
                        1.84775e-06, -2.20547e-06, 2.992e-06, -4.59188e-07, 3.42057e-06,
                        1.61325e-06, -8.78743e-07, 3.41178e-06, 7.03707e-07, 1.65662e-06,
                        3.10089e-06, -6.9532e-09, 2.07912e-06, 2.24383e-06, 1.73745e-06,
                        3.34044e-06, 3.85704e-06, 5.34937e-06, 8.5216e-06, 1.2961e-05,
                        2.37797e-05, 3.6878e-05, 9.54617e-05, 500 };
   const double fD[49] = { -0.000146404, -0.000146404, -0.000140844, -7.41474e-06, 3.68434e-05,
                        -1.94883e-06, -9.26275e-05, 8.01117e-05, -4.3806e-05, -1.4141e-05,
                        1.89317e-05, -4.57185e-06, -4.38806e-06, 3.88834e-06, -1.24105e-06,
                        9.4492e-07, -3.05026e-07, 1.38417e-07, -4.1366e-08, -1.3372e-08,
                        3.00311e-08, -1.87934e-08, 1.96317e-10, 1.37439e-09, 2.34263e-09,
                        -5.40429e-09, 6.92996e-09, -4.60158e-09, 5.17301e-09, -2.40976e-09,
                        -3.32265e-09, 5.72069e-09, -3.61076e-09, 1.27055e-09, 1.92569e-09,
                        -4.14379e-09, 2.78143e-09, 1.09808e-10, -3.37587e-10, 1.06866e-09,
                        3.44402e-10, 9.94884e-10, 2.11482e-09, 2.95958e-09, 7.21251e-09,
                        8.73216e-09, 3.90558e-08, 3.90558e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_EWbun_EV_1(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -0.686412, 1.50943, 2.49066, 2.48731, 2.65178,
                        2.71352, 2.73705, 2.91655, 3.09004, 3.0303,
                        3.06078, 2.91162, 2.93852, 2.84114, 2.82539,
                        2.85863, 2.77941, 2.89239, 2.97459, 3.07818,
                        3.21852, 3.28204, 3.38853, 3.47313, 3.63291,
                        3.71445, 3.79928, 3.79954, 3.74691, 3.6523,
                        3.6653, 3.61103, 3.47072, 3.28686, 3.20997,
                        2.88194, 2.66667, 2.32531, 1.57148, 0.539291,
                        -0.792952, -2.35664, -4.54009, -7.29685, -11.3198,
                        -17.3759, -26.9548, -43.9313, -76.4294 };
   const double fB[49] = { 0.75907, 0.362081, -0.020661, 0.013926, 0.013293,
                        0.000766264, 0.00922251, 0.023253, 0.00366236, -0.00377776,
                        0.0026724, -0.00430233, -0.00013499, -0.00361537, 0.0010213,
                        -0.000681793, -0.00105318, 0.00156553, 0.000646483, 0.00142236,
                        0.000982074, 0.000765141, 0.0010575, 0.00073762, 0.000455742,
                        0.000335241, 0.000199805, -0.00011338, -0.000374798, -0.000154313,
                        1.28098e-05, -0.000392152, -0.000779256, -0.000380857, -0.000826268,
                        -0.00117309, -0.00100101, -0.00150245, -0.00169877, -0.00241861,
                        -0.00281341, -0.00370331, -0.00485619, -0.00651321, -0.00976907,
                        -0.0148847, -0.0245021, -0.0464392, -0.0865884 };
   const double fC[49] = { -0.0583342, -0.0409132, 0.00263908, 0.000819608, -0.000882908,
                        -0.000369761, 0.00121539, 0.000187661, -0.00214672, 0.00140271,
                        -0.000757696, 0.000478707, -0.000312014, 0.000172798, 1.26683e-05,
                        -4.67301e-05, 3.93023e-05, -1.31151e-05, 3.92467e-06, 3.83412e-06,
                        -8.23699e-06, 6.06767e-06, -3.14407e-06, -5.47316e-08, -1.07278e-06,
                        5.90775e-07, -1.13252e-06, -1.20219e-07, -9.25455e-07, 1.8074e-06,
                        -1.13891e-06, -4.80941e-07, -1.06747e-06, 2.66107e-06, -4.44271e-06,
                        3.05543e-06, -2.36712e-06, 3.61378e-07, -7.54019e-07, -6.85659e-07,
                        -1.03954e-07, -1.67585e-06, -6.29897e-07, -2.68415e-06, -3.82759e-06,
                        -6.40371e-06, -1.28311e-05, -3.10432e-05, 500 };
   const double fD[49] = { 0.00145174, 0.00145174, -6.06492e-05, -5.67505e-05, 1.71049e-05,
                        5.28382e-05, -3.42575e-05, -7.78128e-05, 0.000118315, -7.20136e-05,
                        1.64854e-05, -1.05429e-05, 6.46416e-06, -2.13507e-06, -3.95989e-07,
                        5.73549e-07, -1.74725e-07, 5.67993e-08, -3.01839e-10, -4.0237e-08,
                        4.76822e-08, -3.07058e-08, 1.02978e-08, -1.3574e-09, 2.21807e-09,
                        -2.29772e-09, 1.34973e-09, -1.07365e-09, 3.6438e-09, -3.9284e-09,
                        8.77287e-10, -7.82043e-10, 4.97139e-09, -9.47171e-09, 9.99752e-09,
                        -7.23007e-09, 3.638e-09, -7.43598e-10, 4.55736e-11, 3.87803e-10,
                        -1.04793e-09, 6.97301e-10, -1.3695e-09, -7.62294e-10, -1.71742e-09,
                        -4.28492e-09, -1.21414e-08, -1.21414e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_beamUp_spline(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 0.413092, 0.440252, 0.529101, 0.545616, 0.592954,
                        0.607948, 0.623243, 0.661536, 0.679275, 0.688841,
                        0.708376, 0.731545, 0.7461, 0.755415, 0.800603,
                        0.831601, 0.867832, 0.935772, 0.989519, 1.06238,
                        1.12931, 1.18369, 1.26119, 1.32722, 1.50483,
                        1.69737, 1.89896, 2.11233, 2.33298, 2.55661,
                        2.81042, 3.06993, 3.33424, 3.6359, 3.92601,
                        4.24978, 4.56853, 4.94134, 5.74273, 6.63334,
                        7.67202, 8.84173, 10.1849, 11.8125, 13.7241,
                        16.003, 18.841, 22.4037, 27.0364 };
   const double fB[49] = { 0.00405278, 0.00891606, 0.00503482, 0.00255399, 0.00390534,
                        0.000524173, 0.00308467, 0.00321373, 0.000870053, 0.00149738,
                        0.00187065, 0.000593813, 0.000280952, 0.00114682, 0.00167211,
                        0.000378561, 0.000847376, 0.000544524, 0.000625157, 0.000753111,
                        0.000555983, 0.000662131, 0.000752083, 0.000635421, 0.000755047,
                        0.000786286, 0.000829365, 0.000875725, 0.000875961, 0.000951796,
                        0.00104619, 0.00102325, 0.00114658, 0.00118214, 0.00122615,
                        0.00127983, 0.00136474, 0.00155989, 0.00166698, 0.0019242,
                        0.00221197, 0.00247824, 0.00295247, 0.00353656, 0.00413628,
                        0.00506133, 0.00631987, 0.00806312, 0.0106001 };
   const double fC[49] = { 0.000837044, 0.000378775, -0.000766898, 0.000518815, -0.000383681,
                        4.55638e-05, 0.000210486, -0.00019758, -3.67881e-05, 9.95205e-05,
                        -6.21934e-05, 1.112e-05, -2.36344e-05, 5.82692e-05, -3.72578e-05,
                        1.13869e-05, -2.01064e-06, -1.01788e-06, 1.8242e-06, -5.4466e-07,
                        -1.42662e-06, 2.48811e-06, -1.58859e-06, 4.21972e-07, 5.65342e-08,
                        6.84193e-08, 1.03898e-07, 8.15413e-08, -8.05962e-08, 3.83935e-07,
                        -6.37725e-09, -8.5377e-08, 5.7871e-07, -4.3649e-07, 6.12566e-07,
                        -3.97857e-07, 7.37489e-07, 4.31167e-08, 1.71059e-07, 3.4338e-07,
                        2.32167e-07, 3.00361e-07, 6.48116e-07, 5.20055e-07, 6.79378e-07,
                        1.17072e-06, 1.34636e-06, 2.14013e-06, 500 };
   const double fD[49] = { -3.81891e-05, -3.81891e-05, 4.28571e-05, -3.00832e-05, 1.43081e-05,
                        5.4974e-06, -1.36022e-05, 5.35971e-06, 4.54362e-06, -5.39046e-06,
                        9.77512e-07, -4.63392e-07, 1.09205e-06, -1.27369e-06, 3.24298e-07,
                        -8.93172e-08, 3.30922e-09, 9.47359e-09, -7.89621e-09, -2.93988e-09,
                        1.30491e-08, -1.3589e-08, 6.70189e-09, -4.87251e-10, 1.58468e-11,
                        4.73053e-11, -2.98092e-11, -2.16183e-10, 6.19375e-10, -5.20416e-10,
                        -1.05333e-10, 8.85449e-10, -1.3536e-09, 1.39874e-09, -1.34723e-09,
                        1.51379e-09, -9.2583e-10, 8.52947e-11, 1.14881e-10, -7.4142e-11,
                        4.54623e-11, 2.31837e-10, -8.53735e-11, 1.06215e-10, 3.27563e-10,
                        1.17094e-10, 5.29178e-10, 5.29178e-10, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_beamDown_spline(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -0.413092, -0.440252, -0.529101, -0.545616, -0.592954,
                        -0.607948, -0.623243, -0.661536, -0.679275, -0.688841,
                        -0.708376, -0.731545, -0.7461, -0.755415, -0.800603,
                        -0.831601, -0.867832, -0.935772, -0.989519, -1.06238,
                        -1.12931, -1.18369, -1.26119, -1.32722, -1.50483,
                        -1.69737, -1.89896, -2.11233, -2.33298, -2.55661,
                        -2.81042, -3.06993, -3.33424, -3.6359, -3.92601,
                        -4.24978, -4.56853, -4.94134, -5.74273, -6.63334,
                        -7.67202, -8.84173, -10.1849, -11.8125, -13.7241,
                        -16.003, -18.841, -22.4037, -27.0364 };
   const double fB[49] = { -0.00405278, -0.00891606, -0.00503482, -0.00255399, -0.00390534,
                        -0.000524173, -0.00308467, -0.00321373, -0.000870053, -0.00149738,
                        -0.00187065, -0.000593813, -0.000280952, -0.00114682, -0.00167211,
                        -0.000378561, -0.000847376, -0.000544524, -0.000625157, -0.000753111,
                        -0.000555983, -0.000662131, -0.000752083, -0.000635421, -0.000755047,
                        -0.000786286, -0.000829365, -0.000875725, -0.000875961, -0.000951796,
                        -0.00104619, -0.00102325, -0.00114658, -0.00118214, -0.00122615,
                        -0.00127983, -0.00136474, -0.00155989, -0.00166698, -0.0019242,
                        -0.00221197, -0.00247824, -0.00295247, -0.00353656, -0.00413628,
                        -0.00506133, -0.00631987, -0.00806312, -0.0106001 };
   const double fC[49] = { -0.000837044, -0.000378775, 0.000766898, -0.000518815, 0.000383681,
                        -4.55638e-05, -0.000210486, 0.00019758, 3.67881e-05, -9.95205e-05,
                        6.21934e-05, -1.112e-05, 2.36344e-05, -5.82692e-05, 3.72578e-05,
                        -1.13869e-05, 2.01064e-06, 1.01788e-06, -1.8242e-06, 5.4466e-07,
                        1.42662e-06, -2.48811e-06, 1.58859e-06, -4.21972e-07, -5.65342e-08,
                        -6.84193e-08, -1.03898e-07, -8.15413e-08, 8.05962e-08, -3.83935e-07,
                        6.37725e-09, 8.5377e-08, -5.7871e-07, 4.3649e-07, -6.12566e-07,
                        3.97857e-07, -7.37489e-07, -4.31167e-08, -1.71059e-07, -3.4338e-07,
                        -2.32167e-07, -3.00361e-07, -6.48116e-07, -5.20055e-07, -6.79378e-07,
                        -1.17072e-06, -1.34636e-06, -2.14013e-06, 500 };
   const double fD[49] = { 3.81891e-05, 3.81891e-05, -4.28571e-05, 3.00832e-05, -1.43081e-05,
                        -5.4974e-06, 1.36022e-05, -5.35971e-06, -4.54362e-06, 5.39046e-06,
                        -9.77512e-07, 4.63392e-07, -1.09205e-06, 1.27369e-06, -3.24298e-07,
                        8.93172e-08, -3.30922e-09, -9.47359e-09, 7.89621e-09, 2.93988e-09,
                        -1.30491e-08, 1.3589e-08, -6.70189e-09, 4.87251e-10, -1.58468e-11,
                        -4.73053e-11, 2.98092e-11, 2.16183e-10, -6.19375e-10, 5.20416e-10,
                        1.05333e-10, -8.85449e-10, 1.3536e-09, -1.39874e-09, 1.34723e-09,
                        -1.51379e-09, 9.2583e-10, -8.52947e-11, -1.14881e-10, 7.4142e-11,
                        -4.54623e-11, -2.31837e-10, 8.53735e-11, -1.06215e-10, -3.27563e-10,
                        -1.17094e-10, -5.29178e-10, -5.29178e-10, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 6.68344, 5.71411, 4.5364, 3.92704, 3.5363,
                        3.24618, 3.03919, 2.891, 2.78144, 2.66378,
                        2.5613, 2.31077, 2.11987, 1.97224, 1.83434,
                        1.61626, 1.42569, 1.1225, 0.852309, 0.610022,
                        0.395145, 0.192575, 0.0288745, 0.21294, 0.604934,
                        0.942876, 1.23934, 1.51604, 1.76703, 1.9943,
                        2.20451, 2.38231, 2.52101, 2.63011, 2.6926,
                        2.71481, 2.69206, 2.63219, 2.38515, 1.99247,
                        1.47279, 1.53493, 2.55593, 3.52601, 4.3358,
                        4.82307, 4.77765, 3.86508, 1.87319 };
   const double fB[49] = { -0.29577, -0.193996, -0.0734154, -0.0484626, -0.032764,
                        -0.024741, -0.0174075, -0.0121816, -0.0111905, -0.0112224,
                        -0.00996111, -0.00913892, -0.00645516, -0.00566472, -0.00514906,
                        -0.00395572, -0.00354728, -0.00276859, -0.00257982, -0.00228639,
                        -0.00198955, -0.00227883, 0.000116746, 0.00242281, 0.0012573,
                        0.00130724, 0.00112667, 0.00106411, 0.000949183, 0.000878249,
                        0.000787511, 0.000627757, 0.000499464, 0.000347991, 0.00016765,
                        -2.18918e-06, -0.00016531, -0.000327995, -0.000620638, -0.00102775,
                        -0.00074252, 0.00125257, 0.00223105, 0.0017697, 0.00136939,
                        0.000535116, -0.00085877, -0.00284799, -0.00517601 };
   const double fC[49] = { 0.014634, 0.0108096, 0.00124845, 0.00124684, 0.00032302,
                        0.00047928, 0.000254064, 0.000268529, -0.000169416, 0.000166225,
                        -4.00999e-05, 7.29878e-05, 3.43624e-05, -2.74475e-06, 2.33712e-05,
                        4.95619e-07, 7.67322e-06, 1.13682e-07, 1.77399e-06, 1.16032e-06,
                        1.80806e-06, -4.70088e-06, 2.86567e-05, -5.59597e-06, 9.33893e-07,
                        -7.34126e-07, 1.1853e-08, -2.62094e-07, -1.97606e-07, -8.61315e-08,
                        -2.76819e-07, -3.62198e-07, -1.50973e-07, -4.54918e-07, -2.66446e-07,
                        -4.12911e-07, -2.39571e-07, -4.11172e-07, -1.74113e-07, -6.4011e-07,
                        1.21057e-06, 2.77962e-06, -8.22671e-07, -1.00017e-07, -7.00607e-07,
                        -9.67943e-07, -1.81983e-06, -2.15862e-06, 500 };
   const double fD[49] = { -0.000318704, -0.000318704, -5.35693e-08, -3.07939e-05, 5.20865e-06,
                        -7.50718e-06, 4.8216e-07, -1.45982e-05, 1.1188e-05, -6.8775e-06,
                        1.50784e-06, -5.15006e-07, -4.94762e-07, 3.48212e-07, -1.52504e-07,
                        4.78507e-08, -2.51985e-08, 5.53436e-09, -2.04557e-09, 2.15915e-09,
                        -2.16965e-08, 1.11192e-07, -1.14175e-07, 8.70649e-09, -2.22403e-09,
                        9.94639e-10, -3.65263e-10, 8.59842e-11, 1.48633e-10, -2.5425e-10,
                        -1.13839e-10, 2.81633e-10, -4.05259e-10, 2.51296e-10, -1.95286e-10,
                        2.31119e-10, -2.28801e-10, 1.58039e-10, -3.10665e-10, 1.23379e-09,
                        1.04603e-09, -2.40153e-09, 4.8177e-10, -4.00393e-10, -1.78225e-10,
                        -5.67923e-10, -2.25862e-10, -2.25862e-10, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_CT14nnlo_as_0118_alpha_s_down_fit(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -6.19552, -5.41866, -4.45551, -3.95241, -3.62,
                        -3.38254, -3.22242, -3.12854, -3.06904, -2.98249,
                        -2.87052, -2.59297, -2.38655, -2.21327, -2.05657,
                        -1.81374, -1.58983, -1.22879, -0.914359, -0.639978,
                        -0.3895, -0.171058, -0.00577489, -0.14742, -0.499361,
                        -0.789115, -1.04592, -1.2721, -1.48431, -1.67288,
                        -1.82346, -1.94166, -2.02223, -2.04914, -2.02606,
                        -1.94758, -1.81587, -1.62212, -1.07754, -0.593502,
                        -0.194021, 0.0629069, -0.100348, -0.65727, -1.49274,
                        -2.376, -3.31528, -4.32644, -5.45449 };
   const double fB[49] = { 0.235921, 0.156436, 0.0607167, 0.040572, 0.0276461,
                        0.0198029, 0.0124184, 0.00672409, 0.00669952, 0.0102935,
                        0.011682, 0.00977724, 0.00728543, 0.00664559, 0.00572908,
                        0.00451021, 0.00423459, 0.00327218, 0.00294089, 0.00262872,
                        0.00229001, 0.00227886, 0.000106311, -0.00199498, -0.00114764,
                        -0.00111481, -0.000951816, -0.000873771, -0.000813762, -0.000680469,
                        -0.000534229, -0.000408082, -0.000218613, -7.203e-06, 0.000201475,
                        0.000420074, 0.000640441, 0.00092364, 0.00109481, 0.000868822,
                        0.000730995, 0.000145653, -0.000751574, -0.00146042, -0.00176108,
                        -0.0018076, -0.00194375, -0.00212004, -0.00241133 };
   const double fC[49] = { -0.011407, -0.00846428, -0.0011076, -0.000906875, -0.00038572,
                        -0.000398601, -0.000339846, -0.000229585, 0.000227128, 0.000132268,
                        6.58297e-06, -8.27729e-05, -1.68997e-05, -8.69377e-06, -2.79668e-05,
                        3.58948e-06, -9.10195e-06, -5.22171e-07, -2.79069e-06, -3.31072e-07,
                        -3.05601e-06, 2.94455e-06, -2.467e-05, 3.65718e-06, -2.67823e-07,
                        3.99115e-07, 2.52876e-07, 5.93013e-08, 1.80737e-07, 3.52434e-07,
                        2.32526e-07, 2.7206e-07, 4.85816e-07, 3.59823e-07, 4.74889e-07,
                        3.99508e-07, 4.81961e-07, 6.50835e-07, -3.08488e-07, -1.43497e-07,
                        -1.32156e-07, -1.03853e-06, -7.55926e-07, -6.61771e-07, 6.04492e-08,
                        -1.5349e-07, -1.18799e-07, -2.3379e-07, 500 };
   const double fD[49] = { 0.000245223, 0.000245223, 6.69079e-06, 1.73718e-05, -4.29382e-07,
                        1.9585e-06, 3.67538e-06, 1.52237e-05, -3.16199e-06, -4.1895e-06,
                        -1.19141e-06, 8.78309e-07, 1.09413e-07, -2.56973e-07, 2.10375e-07,
                        -8.46095e-08, 2.85992e-08, -7.56173e-09, 8.19873e-09, -9.08313e-09,
                        2.00019e-08, -9.20486e-08, 9.44241e-08, -5.23333e-09, 8.89251e-10,
                        -1.94985e-10, -2.581e-10, 1.61915e-10, 2.28929e-10, -1.59877e-10,
                        5.27121e-11, 2.85008e-10, -1.67991e-10, 1.53421e-10, -1.00508e-10,
                        1.09937e-10, 2.25166e-10, -6.39548e-10, 1.09994e-10, 7.5606e-12,
                        -6.04248e-10, 1.88401e-10, 6.27701e-11, 4.8148e-10, -1.42626e-10,
                        2.31271e-11, -7.66605e-11, -7.66605e-11, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::z_scalelow(double x) {
   const int fNp = 48, fKstep = 0;
   const double fDelta = -1, fXmin = 10, fXmax = 10000;
   const double fX[48] = { 10, 20, 30, 40, 50,
                        60, 70, 80, 90, 100,
                        125, 150, 175, 200, 250,
                        300, 400, 500, 600, 700,
                        800, 900, 1000, 1250, 1500,
                        1750, 2000, 2250, 2500, 2750,
                        3000, 3250, 3500, 3750, 4000,
                        4250, 4500, 5000, 5500, 6000,
                        6500, 7000, 7500, 8000, 8500,
                        9000, 9500, 10000 };
   const double fY[48] = { -6.09, -2.49, -1.48, -1.19, -0.89,
                        -0.77, -0.65, -0.57, -0.55, -0.43,
                        -0.37, -0.3, -0.23, -0.19, -0.08,
                        -0.07, 0.06, 0.12, 0.17, 0.23,
                        0.26, 0.31, 0.35, 0.42, 0.5,
                        0.59, 0.66, 0.71, 0.8, 0.86,
                        0.94, 1.05, 1.11, 1.23, 1.31,
                        1.42, 1.56, 1.81, 2.16, 2.47,
                        2.9, 3.34, 3.76, 4.2, 4.66,
                        5.12, 5.52, 5.72 };
   const double fB[48] = { 0.559541, 0.195479, 0.041541, 0.0283564, 0.0220335,
                        0.00950959, 0.0119281, 0.00277789, 0.0069603, 0.0113809,
                        0.000132848, 0.00368769, 0.00191639, 0.00184676, 0.00128666,
                        0.000206601, 0.00128708, 0.000345091, 0.00063256, 0.00042467,
                        0.00036876, 0.000500289, 0.000330085, 0.000278683, 0.000355184,
                        0.00034058, 0.000202495, 0.000289441, 0.000319741, 0.000231596,
                        0.000433874, 0.000312909, 0.00035449, 0.00042913, 0.000328989,
                        0.000534915, 0.000531349, 0.000602072, 0.000660362, 0.000716479,
                        0.00091372, 0.000848641, 0.000851718, 0.000904488, 0.00093033,
                        0.000894191, 0.000652904, 9.41914e-05 };
   const double fC[48] = { -0.0234562, -0.01295, -0.00244384, 0.00112538, -0.00175766,
                        0.000505269, -0.000263415, -0.000651608, 0.00106985, -0.000627787,
                        0.000177864, -3.56708e-05, -3.51814e-05, 3.23964e-05, -4.35984e-05,
                        2.19972e-05, -1.11925e-05, 1.77259e-06, 1.1021e-06, -3.181e-06,
                        2.62191e-06, -1.30662e-06, -3.95411e-07, 1.89802e-07, 1.16205e-07,
                        -1.74621e-07, -3.77722e-07, 7.25508e-07, -6.04311e-07, 2.51734e-07,
                        5.57375e-07, -1.04123e-06, 1.20756e-06, -9.08997e-07, 5.0843e-07,
                        3.15278e-07, -3.29542e-07, 4.70987e-07, -3.54407e-07, 4.66642e-07,
                        -7.21611e-08, -5.79979e-08, 6.41525e-08, 4.13877e-08, 1.02966e-08,
                        -8.25742e-08, -4e-07, 500 };
   const double fD[48] = { 0.000350205, 0.000350205, 0.000118974, -9.61013e-05, 7.5431e-05,
                        -2.56228e-05, -1.29398e-05, 5.73819e-05, -5.65879e-05, 1.0742e-05,
                        -2.84714e-06, 6.52497e-09, 9.01037e-07, -5.06632e-07, 4.37304e-07,
                        -1.10632e-07, 4.32168e-08, -2.23494e-09, -1.4277e-08, 1.9343e-08,
                        -1.30951e-08, 3.03737e-09, 7.80284e-10, -9.81292e-11, -3.87767e-10,
                        -2.70801e-10, 1.47097e-09, -1.77309e-09, 1.14139e-09, 4.07521e-10,
                        -2.13148e-09, 2.99839e-09, -2.82207e-09, 1.8899e-09, -2.57535e-10,
                        -8.5976e-10, 5.33686e-10, -5.50263e-10, 5.47366e-10, -3.59202e-10,
                        9.44213e-12, 8.14336e-11, -1.51766e-11, -2.07274e-11, -6.19138e-11,
                        -2.11617e-10, -2.11617e-10, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::z_scaleup(double x) {
   const int fNp = 48, fKstep = 0;
   const double fDelta = -1, fXmin = 10, fXmax = 10000;
   const double fX[48] = { 10, 20, 30, 40, 50,
                        60, 70, 80, 90, 100,
                        125, 150, 175, 200, 250,
                        300, 400, 500, 600, 700,
                        800, 900, 1000, 1250, 1500,
                        1750, 2000, 2250, 2500, 2750,
                        3000, 3250, 3500, 3750, 4000,
                        4250, 4500, 5000, 5500, 6000,
                        6500, 7000, 7500, 8000, 8500,
                        9000, 9500, 10000 };
   const double fY[48] = { 4.37, 2.49, 1.89, 1.5, 1.27,
                        1.13, 0.98, 0.9, 0.82, 0.78,
                        0.62, 0.5, 0.44, 0.34, 0.19,
                        0.07, -0.04, -0.15, -0.24, -0.32,
                        -0.38, -0.46, -0.51, -0.64, -0.76,
                        -0.88, -1, -1.12, -1.22, -1.33,
                        -1.45, -1.56, -1.69, -1.84, -1.97,
                        -2.09, -2.26, -2.6, -2.98, -3.41,
                        -3.85, -4.33, -4.78, -5.25, -5.7,
                        -6.15, -6.53, -6.71 };
   const double fB[48] = { -0.296766, -0.101617, -0.040766, -0.0323189, -0.0159583,
                        -0.0148479, -0.01165, -0.00755196, -0.00614214, -0.00387947,
                        -0.00668833, -0.00296719, -0.00304289, -0.00406125, -0.00254674,
                        -0.00195179, -0.000895754, -0.00106519, -0.000843482, -0.000660881,
                        -0.000712994, -0.000687142, -0.000438437, -0.000523083, -0.000469231,
                        -0.000479993, -0.000490795, -0.000436826, -0.000401902, -0.000475566,
                        -0.000455833, -0.000461102, -0.000579761, -0.000579855, -0.00046082,
                        -0.000576866, -0.000711716, -0.000695969, -0.000824406, -0.000866407,
                        -0.000929965, -0.000933733, -0.000915105, -0.000925848, -0.000901502,
                        -0.000868142, -0.000605929, -6.81422e-05 };
   const double fC[48] = { 0.0131149, 0.0064, -0.000314903, 0.00115961, 0.00047645,
                        -0.000365413, 0.000685202, -0.000275394, 0.000416376, -0.000190109,
                        7.77544e-05, 7.10911e-05, -7.41189e-05, 3.33847e-05, -3.09452e-06,
                        1.49934e-05, -4.43302e-06, 2.73864e-06, -5.21551e-07, 2.34756e-06,
                        -2.86869e-06, 3.12722e-06, -6.40169e-07, 3.01587e-07, -8.61789e-08,
                        4.31286e-08, -8.63356e-08, 3.02214e-07, -1.62519e-07, -1.32138e-07,
                        2.11071e-07, -2.32144e-07, -2.42493e-07, 2.42118e-07, 2.34022e-07,
                        -6.98207e-07, 1.58805e-07, -1.27311e-07, -1.29562e-07, 4.55592e-08,
                        -1.72675e-07, 1.6514e-07, -1.27884e-07, 1.06398e-07, -5.7706e-08,
                        1.24427e-07, 4e-07, 500 };
   const double fD[48] = { -0.00022383, -0.00022383, 4.91506e-05, -2.27721e-05, -2.80621e-05,
                        3.50205e-05, -3.20199e-05, 2.3059e-05, -2.02162e-05, 3.57151e-06,
                        -8.8844e-08, -1.93613e-06, 1.43338e-06, -2.43195e-07, 1.20586e-07,
                        -6.47548e-08, 2.39055e-08, -1.08673e-08, 9.56371e-09, -1.73875e-08,
                        1.99864e-08, -1.25579e-08, 1.25567e-09, -5.17021e-10, 1.7241e-10,
                        -1.72619e-10, 5.18066e-10, -6.19643e-10, 4.0508e-11, 4.57611e-10,
                        -5.90953e-10, -1.37988e-11, 6.46148e-10, -1.07941e-11, -1.24297e-09,
                        1.14268e-09, -1.90744e-10, -1.50101e-12, 1.16748e-10, -1.45489e-10,
                        2.2521e-10, -1.95349e-10, 1.56188e-10, -1.09402e-10, 1.21422e-10,
                        1.83716e-10, 1.83716e-10, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::z_all_pi_mem0_spline(double x) {
   const int fNp = 28, fKstep = 0;
   const double fDelta = -1, fXmin = 64, fXmax = 9704;
   const double fX[28] = { 64, 71, 81, 91, 106,
                        128, 154, 186, 225, 272,
                        328, 396, 478, 576, 696,
                        840, 1014, 1224, 1478, 1784,
                        2397, 3138, 3787, 4571, 5518,
                        6660, 8039, 9704 };
   const double fY[28] = { -0.0338, 0.4807, 0.15098, -0.005288, 0.16585,
                        1.00511, 1.86706, 2.66568, 3.3991, 4.0584,
                        4.63433, 5.13159, 5.5567, 5.97159, 6.38994,
                        6.85378, 7.39851, 8.07163, 8.94237, 10.0932,
                        12.294, 17.4818, 22.2847, 30.1231, 45.1366,
                        80.268, 192.897, 845.71 };
   const double fB[28] = { 0.157914, 0.00579227, -0.0376415, -0.00102257, 0.0254821,
                        0.0404329, 0.0281587, 0.021883, 0.0162411, 0.0120733,
                        0.00872668, 0.00609563, 0.00459157, 0.0038451, 0.00328156,
                        0.00316086, 0.00314274, 0.00326482, 0.00368652, 0.0034842,
                        0.00506058, 0.00745518, 0.00819509, 0.0116178, 0.0225902,
                        0.0359698, 0.170378, 0.676466 };
   const double fC[28] = { -0.0144458, -0.0072859, 0.00294252, 0.000719373, 0.00104761,
                        -0.000368028, -0.000104057, -9.20578e-05, -5.26071e-05, -3.60695e-05,
                        -2.36917e-05, -1.50002e-05, -3.34201e-06, -4.27503e-06, -4.21137e-07,
                        -4.17037e-07, 3.12888e-07, 2.68427e-07, 1.39183e-06, -2.053e-06,
                        4.62458e-06, -1.393e-06, 2.53307e-06, 1.83261e-06, 9.75391e-06,
                        1.96199e-06, 9.5506e-05, 1665 };
   const double fD[28] = { 0.000340947, 0.000340947, -7.4105e-05, 7.29409e-06, -2.1449e-05,
                        3.38425e-06, 1.24988e-07, 3.37186e-07, 1.17288e-07, 7.36772e-08,
                        4.26056e-08, 4.73909e-08, -3.17352e-09, 1.07052e-08, 9.49029e-12,
                        1.39832e-09, -7.0573e-11, 1.47428e-09, -3.75253e-09, 3.6311e-09,
                        -2.70697e-09, 2.01648e-09, -2.97817e-10, 2.78821e-09, -2.27435e-09,
                        2.26116e-08, 2.26116e-08, 640.5 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::z_all_pi_mem1_spline(double x) {
   const int fNp = 28, fKstep = 0;
   const double fDelta = -1, fXmin = 64, fXmax = 9704;
   const double fX[28] = { 64, 71, 81, 91, 106,
                        128, 154, 186, 225, 272,
                        328, 396, 478, 576, 696,
                        840, 1014, 1224, 1478, 1784,
                        2397, 3138, 3787, 4571, 5518,
                        6660, 8039, 9704 };
   const double fY[28] = { -0.4669, 0.04612, 0.03922, -0.0065112, 0.058624,
                        0.39703, 0.76155, 1.10695, 1.42907, 1.71832,
                        1.97027, 2.17173, 2.33434, 2.46592, 2.58639,
                        2.70427, 2.83479, 2.99659, 3.20677, 3.49033,
                        4.0342, 5.31648, 6.4287, 8.22, 11.6307,
                        19.4762, 44.156, 198.868 };
   const double fB[28] = { 0.126834, 0.0292481, -0.01123, -0.000117403, 0.00988005,
                        0.0166832, 0.0121206, 0.0095378, 0.00714719, 0.00530878,
                        0.00371144, 0.0023948, 0.00162612, 0.00113676, 0.000895354,
                        0.000766267, 0.00075158, 0.000785239, 0.000899105, 0.000860102,
                        0.00126336, 0.00178814, 0.00187306, 0.00263357, 0.00519248,
                        0.00755532, 0.0389504, 0.162506 };
   const double fC[28] = { -0.00900723, -0.00493362, 0.000885809, 0.000225453, 0.000441043,
                        -0.000131808, -4.3678e-05, -3.70339e-05, -2.42637e-05, -1.48514e-05,
                        -1.36725e-05, -5.68987e-06, -3.68417e-06, -1.30935e-06, -7.02364e-07,
                        -1.94072e-07, 1.09664e-07, 5.06189e-08, 3.97671e-07, -5.25131e-07,
                        1.18297e-06, -4.74752e-07, 6.05588e-07, 3.64452e-07, 2.33767e-06,
                        -2.68631e-07, 2.30352e-05, 1665 };
   const double fD[28] = { 0.000193981, 0.000193981, -2.20118e-05, 4.79089e-06, -8.67957e-06,
                        1.12988e-06, 6.921e-08, 1.09146e-07, 6.67541e-08, 7.01695e-09,
                        3.91308e-08, 8.15323e-09, 8.07763e-09, 1.68607e-09, 1.1766e-09,
                        5.8187e-10, -9.37231e-11, 4.5545e-10, -1.00523e-09, 9.2882e-10,
                        -7.45713e-10, 5.54874e-10, -1.02524e-10, 6.9455e-10, -7.60742e-10,
                        5.63303e-09, 5.63303e-09, 640.5 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_HERAPDF20_CT14nnlo(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 0.948519, 0.991195, 1.03176, 1.04543, 1.05164,
                        1.05442, 1.05523, 1.0532, 1.04915, 1.04604,
                        1.04586, 1.04744, 1.04837, 1.04942, 1.04992,
                        1.05042, 1.0509, 1.05147, 1.05106, 1.05012,
                        1.04856, 1.04713, 1.04544, 1.04391, 1.04055,
                        1.03743, 1.03513, 1.03144, 1.02722, 1.02191,
                        1.01562, 1.00711, 0.997831, 0.987076, 0.974698,
                        0.961111, 0.945397, 0.928621, 0.889996, 0.845788,
                        0.794714, 0.737942, 0.675118, 0.607676, 0.536211,
                        0.460993, 0.382997, 0.304198, 0.227887 };
   const double fB[49] = { 0.0135515, 0.0080702, 0.0018154, 0.000939135, 0.000393256,
                        0.000184021, -5.24016e-05, -0.000340005, -0.000412219, -0.000157692,
                        5.60151e-05, 5.19793e-05, 3.7849e-05, 3.38249e-05, 1.19783e-05,
                        9.42721e-06, 9.2276e-06, 6.51247e-07, -6.95181e-06, -1.31904e-05,
                        -1.52779e-05, -1.54079e-05, -1.67833e-05, -1.42573e-05, -1.33233e-05,
                        -1.01006e-05, -1.14094e-05, -1.61445e-05, -1.8901e-05, -2.26197e-05,
                        -2.97374e-05, -3.60965e-05, -3.94071e-05, -4.66579e-05, -5.15565e-05,
                        -5.87001e-05, -6.5256e-05, -7.01571e-05, -8.29139e-05, -9.51829e-05,
                        -0.00010805, -0.000119698, -0.000130734, -0.000138961, -0.000146864,
                        -0.000153681, -0.000157695, -0.000156304, -0.000147747 };
   const double fC[49] = { -0.000791563, -0.000578752, -4.67275e-05, -4.08993e-05, -1.36886e-05,
                        -7.2349e-06, -1.64074e-05, -1.2353e-05, 5.13164e-06, 2.0321e-05,
                        1.04968e-06, -1.21111e-06, 6.45901e-07, -8.06866e-07, -6.70002e-08,
                        1.59792e-08, -1.99714e-08, -6.57922e-08, -1.02384e-08, -5.21473e-08,
                        3.12718e-08, -3.25713e-08, 1.8817e-08, 6.44341e-09, -2.70754e-09,
                        1.55983e-08, -2.08334e-08, 1.89319e-09, -1.29193e-08, -1.95552e-09,
                        -2.65152e-08, 1.07891e-09, -1.43217e-08, -1.46811e-08, -4.91327e-09,
                        -2.36612e-08, -2.56263e-09, -1.70418e-08, -8.47185e-09, -1.6066e-08,
                        -9.66756e-09, -1.36283e-08, -8.44497e-09, -8.00865e-09, -7.79708e-09,
                        -5.83722e-09, -2.19139e-09, 4.97441e-09, 500 };
   const double fD[49] = { 1.77342e-05, 1.77342e-05, 1.94271e-07, 9.07025e-07, 2.15123e-07,
                        -3.0575e-07, 1.35147e-07, 5.82821e-07, 5.06313e-07, -6.42378e-07,
                        -3.01439e-08, 2.47602e-08, -1.93702e-08, 9.86488e-09, 5.53196e-10,
                        -2.39671e-10, -1.52736e-10, 1.85179e-10, -1.39697e-10, 2.78064e-10,
                        -2.1281e-10, 1.71294e-10, -4.12453e-11, -1.22013e-11, 2.44077e-11,
                        -4.85755e-11, 3.03021e-11, -1.975e-11, 1.46184e-11, -3.27462e-11,
                        3.67921e-11, -2.05341e-11, -4.79302e-13, 1.30238e-11, -2.49972e-11,
                        2.81314e-11, -1.93055e-11, 5.71329e-12, -5.06279e-12, 4.26565e-12,
                        -2.64052e-12, 3.45558e-12, 2.90878e-13, 1.41047e-13, 1.30658e-12,
                        2.43055e-12, 4.7772e-12, 4.7772e-12, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_NNPDF30_CT14nnlo(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 1.26007, 1.07987, 0.976961, 0.961168, 0.961619,
                        0.965302, 0.970919, 0.978126, 0.985083, 0.988194,
                        0.988427, 0.986994, 0.986438, 0.986895, 0.98738,
                        0.988565, 0.990032, 0.992769, 0.995667, 0.998638,
                        1.00169, 1.00463, 1.0077, 1.01081, 1.01912,
                        1.02757, 1.03728, 1.04602, 1.05585, 1.06647,
                        1.07973, 1.09468, 1.1128, 1.13463, 1.16258,
                        1.19653, 1.2381, 1.28991, 1.4335, 1.65639,
                        2.00485, 2.56716, 3.52241, 5.27363, 8.80577,
                        16.7224, 36.3378, 89.1794, 240.593 };
   const double fB[49] = { -0.0611857, -0.0306868, -0.000975737, -0.00102222, 0.000461885,
                        0.00041517, 0.000667399, 0.000762239, 0.000532757, 0.000127067,
                        -3.75686e-05, -5.13697e-05, 4.26961e-06, 2.24071e-05, 1.91789e-05,
                        2.76853e-05, 2.91844e-05, 2.75853e-05, 2.95328e-05, 3.03517e-05,
                        2.98708e-05, 2.98398e-05, 3.09869e-05, 3.1776e-05, 3.3084e-05,
                        3.69554e-05, 3.70263e-05, 3.63647e-05, 4.04046e-05, 4.74357e-05,
                        5.64267e-05, 6.53906e-05, 7.88145e-05, 9.87047e-05, 0.000123694,
                        0.000149304, 0.000185326, 0.000230002, 0.000354461, 0.000551049,
                        0.000869423, 0.00143588, 0.00249243, 0.00483323, 0.00987485,
                        0.0243599, 0.0578775, 0.178872, 0.452165 };
   const double fC[49] = { 0.00447716, 0.00314756, -0.000176447, 0.000171799, -2.33888e-05,
                        1.87173e-05, 6.50559e-06, 2.97845e-06, -2.59267e-05, -1.46424e-05,
                        -1.82117e-06, 1.26913e-06, 9.56443e-07, -2.30943e-07, 1.01817e-07,
                        6.83104e-08, -3.83292e-08, 2.23382e-08, -2.86307e-09, 1.10524e-08,
                        -1.58612e-08, 1.55511e-08, -4.08087e-09, 1.19723e-08, -6.74037e-09,
                        2.22258e-08, -2.19419e-08, 1.92953e-08, -3.13559e-09, 3.12601e-08,
                        4.70383e-09, 3.11516e-08, 2.25443e-08, 5.70163e-08, 4.29405e-08,
                        5.95003e-08, 8.45883e-08, 9.41166e-08, 1.548e-07, 2.38376e-07,
                        3.98373e-07, 7.34536e-07, 1.37856e-06, 3.30304e-06, 6.7802e-06,
                        2.219e-05, 4.48451e-05, 0.000197144, 500 };
   const double fD[49] = { -0.0001108, -0.0001108, 1.16082e-05, -6.50626e-06, 1.40354e-06,
                        -4.07057e-07, -1.17571e-07, -9.63505e-07, 3.76145e-07, 4.27373e-07,
                        4.1204e-08, -4.16916e-09, -1.58318e-08, 4.4368e-09, -2.23378e-10,
                        -7.10931e-10, 2.02225e-10, -8.40042e-11, 4.6385e-11, -8.97122e-11,
                        1.04708e-10, -6.54399e-11, 5.35107e-11, -2.49503e-11, 3.86216e-11,
                        -5.88903e-11, 5.49829e-11, -2.99078e-11, 4.58609e-11, -3.54083e-11,
                        3.52637e-11, -1.14764e-11, 4.59627e-11, -1.87678e-11, 2.20798e-11,
                        3.34506e-11, 1.27044e-11, 4.04555e-11, 5.57172e-11, 1.06665e-10,
                        2.24109e-10, 4.29349e-10, 1.28299e-09, 2.3181e-09, 1.02732e-08,
                        1.51034e-08, 1.01532e-07, 1.01532e-07, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_ct14nnlo_pdf_down(double x) {
   const int fNp = 40, fKstep = 0;
   const double fDelta = -1, fXmin = 9, fXmax = 9703;
   const double fX[40] = { 9, 14, 18, 23, 28,
                        33, 38, 43, 48, 53,
                        58, 63, 71, 81, 91,
                        106, 128, 154, 186, 225,
                        271, 328, 396, 477, 576,
                        696, 840, 1014, 1224, 1478,
                        1784, 2153, 2599, 3137, 3787,
                        4571, 5517, 6660, 8039, 9703 };
   const double fY[40] = { -7.41, -6.56, -6.23, -5.95, -5.74,
                        -5.57, -5.41, -5.26, -5.11, -4.93,
                        -4.73, -4.52, -4.28, -4.35, -4.35,
                        -4.16, -3.82, -3.72, -3.69, -3.67,
                        -3.67, -3.7, -3.77, -3.95, -4.26,
                        -4.7, -5.25, -5.89, -6.62, -7.43,
                        -8.38, -9.69, -11.66, -14.63, -18.78,
                        -23.83, -31.88, -44.43, -62.2, -90.53 };
   const double fB[40] = { 0.245333, 0.109513, 0.0649883, 0.048037, 0.0368639,
                        0.0325075, 0.0311061, 0.0290679, 0.0326221, 0.0384436,
                        0.0416034, 0.0411428, 0.0110919, -0.009842, 0.00727613,
                        0.0163824, 0.0106057, 0.000691503, 0.000875358, 0.000239024,
                        -0.00025978, -0.000711769, -0.00154019, -0.00274782, -0.0034471,
                        -0.0038051, -0.00377937, -0.00358994, -0.00333949, -0.00308105,
                        -0.00323035, -0.00391475, -0.00492673, -0.0060841, -0.00634095,
                        -0.00710989, -0.00981646, -0.0118396, -0.0143085, -0.0202891 };
   const double fC[40] = { -0.0180356, -0.00912841, -0.0020027, -0.00138756, -0.000847053,
                        -2.42257e-05, -0.000256044, -0.000151597, 0.000862431, 0.000301872,
                        0.00033008, -0.000422192, -0.00333418, 0.00124079, 0.000471026,
                        0.000136056, -0.000398632, 1.73168e-05, -1.15713e-05, -4.74498e-06,
                        -6.09858e-06, -1.83106e-06, -1.03515e-05, -4.55756e-06, -2.50584e-06,
                        -4.77524e-07, 6.56256e-07, 4.32387e-07, 7.60237e-07, 2.5725e-07,
                        -7.45152e-07, -1.1096e-06, -1.15942e-06, -9.91831e-07, 5.9669e-07,
                        -1.57749e-06, -1.28358e-06, -4.86445e-07, -1.30389e-06, 1664 };
   const double fD[40] = { 0.00059381, 0.00059381, 4.10089e-05, 3.6034e-05, 5.48552e-05,
                        -1.54546e-05, 6.96318e-06, 6.76019e-05, -3.73706e-05, 1.88051e-06,
                        -5.01515e-05, -0.000121333, 0.000152499, -2.56587e-05, -7.44377e-06,
                        -8.10133e-06, 5.33267e-06, -3.00917e-07, 5.83443e-08, -9.80868e-09,
                        2.49563e-08, -4.1767e-08, 2.38435e-08, 6.90812e-09, 5.63422e-09,
                        2.62449e-09, -4.28868e-10, 5.20396e-10, -6.60087e-10, -1.09194e-09,
                        -3.29224e-10, -3.72281e-11, 1.03831e-10, 8.14626e-10, -9.24395e-10,
                        1.03562e-10, 2.32468e-10, -1.97593e-10, -1.97593e-10, 640.495 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}

double Comparison::Zgamma_ct14nnlo_pdf_up(double x) {
   const int fNp = 40, fKstep = 0;
   const double fDelta = -1, fXmin = 9, fXmax = 9703;
   const double fX[40] = { 9, 14, 18, 23, 28,
                        33, 38, 43, 48, 53,
                        58, 63, 71, 81, 91,
                        106, 128, 154, 186, 225,
                        271, 328, 396, 477, 576,
                        696, 840, 1014, 1224, 1478,
                        1784, 2153, 2599, 3137, 3787,
                        4571, 5517, 6660, 8039, 9703 };
   const double fY[40] = { 27.74, 19.06, 14.88, 11.56, 9.33,
                        7.85, 6.85, 6.13, 5.57, 5.14,
                        4.77, 4.45, 4.12, 3.96, 3.87,
                        3.64, 3.21, 2.99, 2.9, 2.89,
                        2.97, 3.11, 3.3, 3.53, 3.8,
                        4.12, 4.49, 4.94, 5.48, 6.12,
                        6.87, 7.76, 8.93, 10.75, 14.12,
                        20.08, 29.78, 48.11, 106.29, 377.66 };
   const double fB[40] = { -2.24045, -1.29853, -0.834336, -0.533077, -0.363355,
                        -0.239504, -0.16663, -0.125975, -0.0974695, -0.078147,
                        -0.0699426, -0.0560824, -0.027413, -0.00922577, -0.0106839,
                        -0.0192419, -0.0155066, -0.00427661, -0.00151482, 0.000881693,
                        0.00225353, 0.00264904, 0.00286574, 0.00278921, 0.00269427,
                        0.00261637, 0.00256317, 0.00259073, 0.00254969, 0.0024889,
                        0.00241318, 0.00245485, 0.00285497, 0.00408309, 0.00633358,
                        0.0086959, 0.0122601, 0.0206239, 0.07881, 0.269275 };
   const double fC[40] = { 0.114285, 0.0740987, 0.0419498, 0.0183019, 0.0156426,
                        0.00912756, 0.00544713, 0.00268392, 0.00301719, 0.000847315,
                        0.000793548, 0.00197849, 0.00160518, 0.000213542, -0.000359354,
                        -0.00021118, 0.000380968, 5.09543e-05, 3.53516e-05, 2.60974e-05,
                        3.72509e-06, 3.21372e-06, -2.68899e-08, -9.17954e-07, -4.09979e-08,
                        -6.08197e-07, 2.38757e-07, -8.03565e-08, -1.15071e-07, -1.24289e-07,
                        -1.23143e-07, 2.36062e-07, 6.61079e-07, 1.62167e-06, 1.84063e-06,
                        1.17254e-06, 2.59508e-06, 4.72236e-06, 3.74721e-05, 1664 };
   const double fD[40] = { -0.00267908, -0.00267908, -0.00157652, -0.000177284, -0.000434338,
                        -0.000245362, -0.000184214, 2.22181e-05, -0.000144658, -3.58445e-06,
                        7.89962e-05, -1.55545e-05, -4.63881e-05, -1.90966e-05, 3.29276e-06,
                        8.97194e-06, -4.23094e-06, -1.62528e-07, -7.90963e-08, -1.62118e-07,
                        -2.99045e-09, -1.58853e-08, -3.66693e-09, 2.95271e-09, -1.57555e-09,
                        1.96054e-09, -6.11329e-10, -5.5103e-11, -1.2097e-11, 1.24855e-12,
                        3.24485e-10, 3.17651e-10, 5.95159e-10, 1.12289e-10, -2.84053e-10,
                        5.0125e-10, 6.20378e-10, 7.91629e-09, 7.91629e-09, 640.495 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
} 


int Comparison::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Comparison::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Comparison::GetHistograms( double Mass ){

        	if( Mass > 116 && Mass < 150 ) TopSys_ee = TopSys_ee_116_150;
        	if( Mass > 150 && Mass < 200 ) TopSys_ee = TopSys_ee_150_200;
        	if( Mass > 200 && Mass < 300 ) TopSys_ee = TopSys_ee_200_300;
        	if( Mass > 300 && Mass < 1500 ) TopSys_ee = TopSys_ee_300_1500;
        	
        	//if(Debug) cout<<"Getting histograms after picking up the mass "<<Mass<<endl;

        	   Top_Stat_emu  = (TH1F*) TopSys_ee->Get("StatError_emu");
        	   Top_Stat_ee  = (TH1F*) TopSys_ee->Get("StatError_ee");
        	   Top_Sys_Theory_Up_ee  = (TH1F*) TopSys_ee->Get("SysRatio_Theory_Up_ee");	
        	   Top_Sys_Theory_Down_ee  = (TH1F*) TopSys_ee->Get("SysRatio_Theory_Down_ee");	

		   //if(Debug) cout<<"Theory histos"<<endl;

        	   SysRatioTop_ELRES_MATERIALCALO_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALCALO_Up_ee");
        	   SysRatioTop_ELRES_MATERIALCRYO_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALCRYO_Up_ee");
        	   SysRatioTop_ELRES_MATERIALGAP_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALGAP_Up_ee");
        	   SysRatioTop_ELRES_MATERIALIBL_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALIBL_Up_ee");
        	   SysRatioTop_ELRES_MATERIALPP0_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALPP0_Up_ee");
        	   SysRatioTop_ELRES_PILEUP_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_PILEUP_Up_ee");
        	   SysRatioTop_ELRES_SAMPLINGTERM_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_SAMPLINGTERM_Up_ee");
                   SysRatioTop_ELRES_ZSMEARING_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_ZSMEARING_Up_ee"); 
        	   SysRatioTop_ELSCALE_E4SCINTILLATOR_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_E4SCINTILLATOR_Up_ee");
        	   SysRatioTop_ELSCALE_G4_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_G4_Up_ee");
        	   SysRatioTop_ELSCALE_L1GAIN_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_L1GAIN_Up_ee");
        	   SysRatioTop_ELSCALE_L2GAIN_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_L2GAIN_Up_ee");
        	   SysRatioTop_ELSCALE_LARCALIB_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARCALIB_Up_ee");
        	   SysRatioTop_ELSCALE_LARELECALIB_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARELECALIB_Up_ee");
        	   SysRatioTop_ELSCALE_LARELECUNCONV_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARELECUNCONV_Up_ee");
        	   SysRatioTop_ELSCALE_MATCALO_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATCALO_Up_ee");
        	   SysRatioTop_ELSCALE_MATCRYO_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATCRYO_Up_ee");
        	   SysRatioTop_ELSCALE_MATID_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATID_Up_ee");
        	   SysRatioTop_ELSCALE_MATPP0_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATPP0_Up_ee");
        	   SysRatioTop_ELSCALE_PEDESTAL_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_PEDESTAL_Up_ee");
        	   SysRatioTop_ELSCALE_PS_BARREL_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_PS_BARREL_Up_ee");
        	   SysRatioTop_ELSCALE_ELSCALE_PS_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_PS_Up_ee");
        	   SysRatioTop_ELSCALE_S12_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_S12_Up_ee");
        	   SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_TOPOCLUSTER_THRES_Up_ee");
        	   SysRatioTop_ELSCALE_WTOTS1_THRES_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_WTOTS1_THRES_Up_ee");
        	   SysRatioTop_ELSCALE_ZEESYST_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_ZEESYST_Up_ee");
        	   SysRatioTop_ELSCALE_LARUNCONVCALIB_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARUNCONVCALIB_Up_ee");
        	
        	   SysRatioTop_ELRES_MATERIALCALO_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALCALO_Down_ee");
        	   SysRatioTop_ELRES_MATERIALCRYO_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALCRYO_Down_ee");
        	   SysRatioTop_ELRES_MATERIALGAP_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALGAP_Down_ee");
        	   SysRatioTop_ELRES_MATERIALIBL_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALIBL_Down_ee");
        	   SysRatioTop_ELRES_MATERIALPP0_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_MATERIALPP0_Down_ee");
        	   SysRatioTop_ELRES_PILEUP_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_PILEUP_Down_ee");
        	   SysRatioTop_ELRES_SAMPLINGTERM_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_SAMPLINGTERM_Down_ee");
                   SysRatioTop_ELRES_ZSMEARING_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELRES_ZSMEARING_Down_ee");
        	   SysRatioTop_ELSCALE_E4SCINTILLATOR_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_E4SCINTILLATOR_Down_ee");
        	   SysRatioTop_ELSCALE_G4_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_G4_Down_ee");
        	   SysRatioTop_ELSCALE_L1GAIN_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_L1GAIN_Down_ee");
        	   SysRatioTop_ELSCALE_L2GAIN_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_L2GAIN_Down_ee");
        	   SysRatioTop_ELSCALE_LARCALIB_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARCALIB_Down_ee");
        	   SysRatioTop_ELSCALE_LARELECALIB_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARELECALIB_Down_ee");
        	   SysRatioTop_ELSCALE_LARELECUNCONV_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARELECUNCONV_Down_ee");
        	   SysRatioTop_ELSCALE_MATCALO_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATCALO_Down_ee");
        	   SysRatioTop_ELSCALE_MATCRYO_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATCRYO_Down_ee");
        	   SysRatioTop_ELSCALE_MATID_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATID_Down_ee");
        	   SysRatioTop_ELSCALE_MATPP0_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_MATPP0_Down_ee");
        	   SysRatioTop_ELSCALE_PEDESTAL_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_PEDESTAL_Down_ee");
        	   SysRatioTop_ELSCALE_PS_BARREL_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_PS_BARREL_Down_ee");
        	   SysRatioTop_ELSCALE_ELSCALE_PS_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_PS_Down_ee");
        	   SysRatioTop_ELSCALE_S12_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_S12_Down_ee");
        	   SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_TOPOCLUSTER_THRES_Down_ee");
        	   SysRatioTop_ELSCALE_WTOTS1_THRES_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_WTOTS1_THRES_Down_ee");
        	   SysRatioTop_ELSCALE_ZEESYST_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_ZEESYST_Down_ee");
        	   SysRatioTop_ELSCALE_LARUNCONVCALIB_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ELSCALE_LARUNCONVCALIB_Down_ee");       	
        	
        	   SysRatioTop_MU_ID_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_ID_Up_ee");
        	   SysRatioTop_MU_ID_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_ID_Down_ee");
        	   SysRatioTop_MU_MS_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_MS_Up_ee");
        	   SysRatioTop_MU_MS_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_MS_Down_ee");
        	   SysRatioTop_MU_SCALE_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_SCALE_Up_ee");
        	   SysRatioTop_MU_SCALE_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_SCALE_Down_ee");
        	   SysRatioTop_MU_SAGHITTARES_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_SAGITTA_RES_BIAS_Up_ee");
        	   SysRatioTop_MU_SAGHITTARES_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_SAGITTA_RES_BIAS_Down_ee");
        	   SysRatioTop_MU_SAGHITTARHO_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_SAGITTA_RHO_Up_ee");
        	   SysRatioTop_MU_SAGHITTARHO_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_MU_SAGITTA_RHO_Down_ee");
        	
        	   SysRatioTop_LepSF_EL_ID_1_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_1_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_2_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_2_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_3_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_3_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_4_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_4_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_5_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_5_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_6_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_6_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_7_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_7_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_8_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_8_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_9_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_9_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_10_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_10_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_11_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_11_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_12_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_12_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_13_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_13_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_14_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_14_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_15_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_15_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_16_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_16_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_17_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_17_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_18_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_18_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_19_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_19_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_20_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_20_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_21_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_21_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_22_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_22_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_23_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_23_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_24_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_24_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_25_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_25_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_26_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_26_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_27_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_27_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_28_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_28_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_29_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_29_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_30_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_30_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_31_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_31_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_32_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_32_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_33_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_33_Up_ee");
        	   SysRatioTop_LepSF_EL_ID_34_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_34_Up_ee");

        	   SysRatioTop_LepSF_EL_ID_1_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_1_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_2_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_2_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_3_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_3_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_4_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_4_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_5_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_5_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_6_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_6_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_7_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_7_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_8_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_8_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_9_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_9_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_10_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_10_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_11_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_11_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_12_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_12_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_13_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_13_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_14_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_14_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_15_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_15_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_16_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_16_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_17_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_17_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_18_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_18_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_19_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_19_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_20_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_20_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_21_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_21_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_22_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_22_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_23_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_23_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_24_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_24_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_25_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_25_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_26_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_26_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_27_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_27_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_28_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_28_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_29_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_29_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_30_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_30_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_31_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_31_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_32_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_32_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_33_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_33_Down_ee");
        	   SysRatioTop_LepSF_EL_ID_34_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_ID_34_Down_ee");

        	   SysRatioTop_LepSF_EL_Reco_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_Reco_Up_ee");
        	   SysRatioTop_LepSF_EL_Iso_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_Iso_Up_ee");
        	   SysRatioTop_LepSF_EL_Trigger_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_Trigger_Up_ee");

        	   SysRatioTop_LepSF_EL_Reco_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_Reco_Down_ee");
        	   SysRatioTop_LepSF_EL_Iso_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_Iso_Down_ee");
        	   SysRatioTop_LepSF_EL_Trigger_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_EL_Trigger_Down_ee");

        	   SysRatioTop_LepSF_MU_Reco_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Reco_Up_ee");
        	   SysRatioTop_LepSF_MU_Reco_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Reco_Down_ee");

        	   SysRatioTop_LepSF_MU_TTVA_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_TTVA_Up_ee"); 
        	   SysRatioTop_LepSF_MU_Trigger_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Trigger_Up_ee");
        	   SysRatioTop_LepSF_MU_Iso_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Iso_Up_ee");

        	   SysRatioTop_LepSF_MU_TTVA_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_TTVA_Down_ee"); 
        	   SysRatioTop_LepSF_MU_Trigger_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Trigger_Down_ee");
        	   SysRatioTop_LepSF_MU_Iso_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Iso_Down_ee");
        	
        	   SysRatioTop_LepSF_MU_TTVA_STAT_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_TTVA_STAT_Up_ee");
        	   SysRatioTop_LepSF_MU_TTVA_STAT_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_TTVA_STAT_Down_ee");
        	   SysRatioTop_LepSF_MU_Iso_STAT_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Isol_STAT_Up_ee");
        	   SysRatioTop_LepSF_MU_Iso_STAT_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Isol_STAT_Down_ee");
        	   SysRatioTop_LepSF_MU_Trigger_STAT_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Trigger_STAT_Up_ee");
        	   SysRatioTop_LepSF_MU_Trigger_STAT_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Trigger_STAT_Down_ee");
        	   SysRatioTop_LepSF_MU_Reco_STAT_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Reco_STAT_Up_ee");
        	   SysRatioTop_LepSF_MU_Reco_STAT_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_LepSF_MU_Reco_STAT_Down_ee");     

		   SysRatio_PDF4LHC_Total_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_PDF4LHC_Total_Up_ee");
                   SysRatio_PDF4LHC_Total_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_PDF4LHC_Total_Down_ee");
		   SysRatio_ISR_Var3c_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ISR_Var3c_Up_ee");                                                                                                                                                                    
		   SysRatio_FSR_RenFact_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_FSR_RenFact_Up_ee");                                                                                                                                                                  
		   SysRatio_ISR_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ISR_Up_ee");                                                                                                                                                                          
		   SysRatio_HardScatter_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_HardScatter_Up_ee");                                                                                                                                                                  
		   SysRatio_FragHadModel_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_FragHadModel_Up_ee");                                                                                                                                                                 
		   SysRatio_TopMass_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_TopMass_Up_ee");                                                                                                                                                                     
		   SysRatio_ISR_RenFact_Up_ee = (TH1F*) TopSys_ee->Get("SysRatio_ISR_RenFact_Up_ee");                                                                                                                                                                  
		   SysRatio_ISR_Var3c_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_ISR_Var3c_Down_ee");                                                                                                                                                                  
		   SysRatio_FSR_RenFact_Down_ee = (TH1F*) TopSys_ee->Get("SysRatio_FSR_RenFact_Down_ee");                                                                                                                                                                
		   SysRatio_ISR_Down_ee= (TH1F*) TopSys_ee->Get("SysRatio_ISR_Down_ee");                                                                                                                                                                        
		   SysRatio_HardScatter_Down_ee= (TH1F*) TopSys_ee->Get("SysRatio_HardScatter_Down_ee");                                                                                                                                                                
		   SysRatio_FragHadModel_Down_ee= (TH1F*) TopSys_ee->Get("SysRatio_FragHadModel_Down_ee");                                                                                                                                                               
		   SysRatio_TopMass_Down_ee= (TH1F*) TopSys_ee->Get("SysRatio_TopMass_Down_ee");                                                                                                                                                                   
		   SysRatio_ISR_RenFact_Down_ee= (TH1F*) TopSys_ee->Get("SysRatio_ISR_RenFact_Down_ee");   

        	if( Mass > 116 && Mass < 150 ) TopSys_mumu = TopSys_mumu_116_150;
        	if( Mass > 150 && Mass < 200 ) TopSys_mumu = TopSys_mumu_150_200;
        	if( Mass > 200 && Mass < 300 ) TopSys_mumu = TopSys_mumu_200_300;
        	if( Mass > 300 && Mass < 1500 ) TopSys_mumu = TopSys_mumu_300_1500;
		
		   Top_Stat_mumu  = (TH1F*) TopSys_mumu->Get("StatError_mumu");
        	   Top_Sys_Theory_Up_mumu  = (TH1F*) TopSys_mumu->Get("SysRatio_Theory_Up_mumu");	
        	   Top_Sys_Theory_Down_mumu  = (TH1F*) TopSys_mumu->Get("SysRatio_Theory_Down_mumu");

        	   SysRatioTop_ELRES_MATERIALCALO_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALCALO_Up_mumu");
        	   SysRatioTop_ELRES_MATERIALCRYO_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALCRYO_Up_mumu");
        	   SysRatioTop_ELRES_MATERIALGAP_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALGAP_Up_mumu");
        	   SysRatioTop_ELRES_MATERIALIBL_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALIBL_Up_mumu");
        	   SysRatioTop_ELRES_MATERIALPP0_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALPP0_Up_mumu");
        	   SysRatioTop_ELRES_PILEUP_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_PILEUP_Up_mumu");
        	   SysRatioTop_ELRES_SAMPLINGTERM_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_SAMPLINGTERM_Up_mumu");
                   SysRatioTop_ELRES_ZSMEARING_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_ZSMEARING_Up_mumu");
        	   SysRatioTop_ELSCALE_E4SCINTILLATOR_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_E4SCINTILLATOR_Up_mumu");
        	   SysRatioTop_ELSCALE_G4_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_G4_Up_mumu");
        	   SysRatioTop_ELSCALE_L1GAIN_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_L1GAIN_Up_mumu");
        	   SysRatioTop_ELSCALE_L2GAIN_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_L2GAIN_Up_mumu");
        	   SysRatioTop_ELSCALE_LARCALIB_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARCALIB_Up_mumu");
        	   SysRatioTop_ELSCALE_LARELECALIB_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARELECALIB_Up_mumu");
        	   SysRatioTop_ELSCALE_LARELECUNCONV_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARELECUNCONV_Up_mumu");
        	   SysRatioTop_ELSCALE_MATCALO_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATCALO_Up_mumu");
        	   SysRatioTop_ELSCALE_MATCRYO_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATCRYO_Up_mumu");
        	   SysRatioTop_ELSCALE_MATID_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATID_Up_mumu");
        	   SysRatioTop_ELSCALE_MATPP0_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATPP0_Up_mumu");
        	   SysRatioTop_ELSCALE_PEDESTAL_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_PEDESTAL_Up_mumu");
        	   SysRatioTop_ELSCALE_PS_BARREL_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_PS_BARREL_Up_mumu");
        	   SysRatioTop_ELSCALE_ELSCALE_PS_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_PS_Up_mumu");
        	   SysRatioTop_ELSCALE_S12_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_S12_Up_mumu");
        	   SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_TOPOCLUSTER_THRES_Up_mumu");
        	   SysRatioTop_ELSCALE_WTOTS1_THRES_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_WTOTS1_THRES_Up_mumu");
        	   SysRatioTop_ELSCALE_ZEESYST_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_ZEESYST_Up_mumu");
        	   SysRatioTop_ELSCALE_LARUNCONVCALIB_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARUNCONVCALIB_Up_mumu");
        	
        	   SysRatioTop_ELRES_MATERIALCALO_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALCALO_Down_mumu");
        	   SysRatioTop_ELRES_MATERIALCRYO_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALCRYO_Down_mumu");
        	   SysRatioTop_ELRES_MATERIALGAP_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALGAP_Down_mumu");
        	   SysRatioTop_ELRES_MATERIALIBL_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALIBL_Down_mumu");
        	   SysRatioTop_ELRES_MATERIALPP0_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_MATERIALPP0_Down_mumu");
        	   SysRatioTop_ELRES_PILEUP_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_PILEUP_Down_mumu");
        	   SysRatioTop_ELRES_SAMPLINGTERM_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_SAMPLINGTERM_Down_mumu");
                   SysRatioTop_ELRES_ZSMEARING_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELRES_ZSMEARING_Down_mumu");
        	   SysRatioTop_ELSCALE_E4SCINTILLATOR_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_E4SCINTILLATOR_Down_mumu");
        	   SysRatioTop_ELSCALE_G4_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_G4_Down_mumu");
        	   SysRatioTop_ELSCALE_L1GAIN_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_L1GAIN_Down_mumu");
        	   SysRatioTop_ELSCALE_L2GAIN_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_L2GAIN_Down_mumu");
        	   SysRatioTop_ELSCALE_LARCALIB_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARCALIB_Down_mumu");
        	   SysRatioTop_ELSCALE_LARELECALIB_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARELECALIB_Down_mumu");
        	   SysRatioTop_ELSCALE_LARELECUNCONV_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARELECUNCONV_Down_mumu");
        	   SysRatioTop_ELSCALE_MATCALO_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATCALO_Down_mumu");
        	   SysRatioTop_ELSCALE_MATCRYO_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATCRYO_Down_mumu");
        	   SysRatioTop_ELSCALE_MATID_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATID_Down_mumu");
        	   SysRatioTop_ELSCALE_MATPP0_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_MATPP0_Down_mumu");
        	   SysRatioTop_ELSCALE_PEDESTAL_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_PEDESTAL_Down_mumu");
        	   SysRatioTop_ELSCALE_PS_BARREL_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_PS_BARREL_Down_mumu");
        	   SysRatioTop_ELSCALE_ELSCALE_PS_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_PS_Down_mumu");
        	   SysRatioTop_ELSCALE_S12_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_S12_Down_mumu");
        	   SysRatioTop_ELSCALE_TOPOCLUSTER_THRES_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_TOPOCLUSTER_THRES_Down_mumu");
        	   SysRatioTop_ELSCALE_WTOTS1_THRES_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_WTOTS1_THRES_Down_mumu");
        	   SysRatioTop_ELSCALE_ZEESYST_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_ZEESYST_Down_mumu");
        	   SysRatioTop_ELSCALE_LARUNCONVCALIB_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ELSCALE_LARUNCONVCALIB_Down_mumu");       	
        	
        	   SysRatioTop_MU_ID_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_ID_Up_mumu");
        	   SysRatioTop_MU_ID_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_ID_Down_mumu");
        	   SysRatioTop_MU_MS_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_MS_Up_mumu");
        	   SysRatioTop_MU_MS_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_MS_Down_mumu");
        	   SysRatioTop_MU_SCALE_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_SCALE_Up_mumu");
        	   SysRatioTop_MU_SCALE_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_SCALE_Down_mumu");
        	   SysRatioTop_MU_SAGHITTARES_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_SAGITTA_RES_BIAS_Up_mumu");
        	   SysRatioTop_MU_SAGHITTARES_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_SAGITTA_RES_BIAS_Down_mumu");
        	   SysRatioTop_MU_SAGHITTARHO_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_SAGITTA_RHO_Up_mumu");
        	   SysRatioTop_MU_SAGHITTARHO_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_MU_SAGITTA_RHO_Down_mumu");
        	
        	   SysRatioTop_LepSF_EL_ID_1_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_1_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_2_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_2_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_3_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_3_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_4_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_4_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_5_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_5_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_6_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_6_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_7_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_7_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_8_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_8_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_9_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_9_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_10_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_10_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_11_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_11_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_12_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_12_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_13_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_13_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_14_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_14_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_15_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_15_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_16_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_16_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_17_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_17_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_18_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_18_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_19_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_19_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_20_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_20_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_21_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_21_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_22_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_22_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_23_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_23_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_24_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_24_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_25_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_25_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_26_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_26_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_27_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_27_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_28_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_28_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_29_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_29_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_30_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_30_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_31_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_31_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_32_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_32_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_33_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_33_Up_mumu");
        	   SysRatioTop_LepSF_EL_ID_34_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_34_Up_mumu");

        	   SysRatioTop_LepSF_EL_ID_1_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_1_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_2_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_2_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_3_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_3_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_4_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_4_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_5_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_5_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_6_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_6_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_7_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_7_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_8_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_8_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_9_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_9_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_10_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_10_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_11_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_11_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_12_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_12_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_13_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_13_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_14_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_14_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_15_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_15_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_16_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_16_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_17_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_17_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_18_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_18_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_19_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_19_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_20_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_20_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_21_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_21_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_22_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_22_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_23_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_23_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_24_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_24_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_25_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_25_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_26_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_26_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_27_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_27_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_28_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_28_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_29_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_29_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_30_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_30_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_31_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_31_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_32_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_32_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_33_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_33_Down_mumu");
        	   SysRatioTop_LepSF_EL_ID_34_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_ID_34_Down_mumu");

        	   SysRatioTop_LepSF_EL_Reco_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_Reco_Up_mumu");
        	   SysRatioTop_LepSF_EL_Iso_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_Iso_Up_mumu");
        	   SysRatioTop_LepSF_EL_Trigger_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_Trigger_Up_mumu");

        	   SysRatioTop_LepSF_EL_Reco_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_Reco_Down_mumu");
        	   SysRatioTop_LepSF_EL_Iso_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_Iso_Down_mumu");
        	   SysRatioTop_LepSF_EL_Trigger_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_EL_Trigger_Down_mumu");

        	   SysRatioTop_LepSF_MU_Reco_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Reco_Up_mumu");
        	   SysRatioTop_LepSF_MU_Reco_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Reco_Down_mumu");

        	   SysRatioTop_LepSF_MU_TTVA_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_TTVA_Up_mumu"); 
        	   SysRatioTop_LepSF_MU_Trigger_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Trigger_Up_mumu");
        	   SysRatioTop_LepSF_MU_Iso_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Iso_Up_mumu");

        	   SysRatioTop_LepSF_MU_TTVA_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_TTVA_Down_mumu"); 
        	   SysRatioTop_LepSF_MU_Trigger_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Trigger_Down_mumu");
        	   SysRatioTop_LepSF_MU_Iso_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Iso_Down_mumu");
        	
        	   SysRatioTop_LepSF_MU_TTVA_STAT_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_TTVA_STAT_Up_mumu");
        	   SysRatioTop_LepSF_MU_TTVA_STAT_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_TTVA_STAT_Down_mumu");
        	   SysRatioTop_LepSF_MU_Iso_STAT_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Isol_STAT_Up_mumu");
        	   SysRatioTop_LepSF_MU_Iso_STAT_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Isol_STAT_Down_mumu");
        	   SysRatioTop_LepSF_MU_Trigger_STAT_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Trigger_STAT_Up_mumu");
        	   SysRatioTop_LepSF_MU_Trigger_STAT_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Trigger_STAT_Down_mumu");
        	   SysRatioTop_LepSF_MU_Reco_STAT_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Reco_STAT_Up_mumu");
        	   SysRatioTop_LepSF_MU_Reco_STAT_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_LepSF_MU_Reco_STAT_Down_mumu");

		   SysRatio_PDF4LHC_Total_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_PDF4LHC_Total_Up_mumu");                                                                                                                                                                
		   SysRatio_PDF4LHC_Total_Down_mumu= (TH1F*) TopSys_mumu->Get("SysRatio_PDF4LHC_Total_Down_mumu");                                                                                                                                                              

		   SysRatio_ISR_Var3c_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ISR_Var3c_Up_mumu");                                                                                                                                                                    
		   SysRatio_FSR_RenFact_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_FSR_RenFact_Up_mumu");                                                                                                                                                                  
		   SysRatio_ISR_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ISR_Up_mumu");                                                                                                                                                                          
		   SysRatio_HardScatter_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_HardScatter_Up_mumu");                                                                                                                                                                  
		   SysRatio_FragHadModel_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_FragHadModel_Up_mumu");                                                                                                                                                                 
		   SysRatio_TopMass_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_TopMass_Up_mumu");                                                                                                                                                                     
		   SysRatio_ISR_RenFact_Up_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ISR_RenFact_Up_mumu");                                                                                                                                                                  

		   SysRatio_ISR_Var3c_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_ISR_Var3c_Down_mumu");                                                                                                                                                                  
		   SysRatio_FSR_RenFact_Down_mumu = (TH1F*) TopSys_mumu->Get("SysRatio_FSR_RenFact_Down_mumu");                                                                                                                                                                
		   SysRatio_ISR_Down_mumu= (TH1F*) TopSys_mumu->Get("SysRatio_ISR_Down_mumu");                                                                                                                                                                        
		   SysRatio_HardScatter_Down_mumu= (TH1F*) TopSys_mumu->Get("SysRatio_HardScatter_Down_mumu");                                                                                                                                                                
		   SysRatio_FragHadModel_Down_mumu= (TH1F*) TopSys_mumu->Get("SysRatio_FragHadModel_Down_mumu");                                                                                                                                                               
		   SysRatio_TopMass_Down_mumu= (TH1F*) TopSys_mumu->Get("SysRatio_TopMass_Down_mumu");                                                                                                                                                                   
		   SysRatio_ISR_RenFact_Down_mumu= (TH1F*) TopSys_mumu->Get("SysRatio_ISR_RenFact_Down_mumu");                                                                                                                                                               


} // end of if GetHistograms


void Comparison::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   if( nameOfTree != "SumWeights" ){
    // Set object pointer
  // Set object pointer
   mc_generator_weights = 0;
   weight_leptonSF_EL_SF_SIMPLIFIED_Reco_UP = 0;
   weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN = 0;
   weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP = 0;
   weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN = 0;
   weight_leptonSF_EL_SF_SIMPLIFIED_Iso_UP = 0;
   weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_jvt = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
   el_trigMatch_HLT_e140_lhloose_nod0 = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
   mu_trigMatch_HLT_mu50 = 0;
   mu_trigMatch_HLT_mu22_mu8noL1 = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   mu_isMedium = 0;
   mu_isHighPt = 0;
   mu_PtID = 0;
   mu_PtMS = 0;
   mu_isolation_FixedCutTight = 0;
   mu_isolation_FixedCutLoose = 0;
   mu_isolation_FixedCutPflowTight = 0;
   mu_isolation_FixedCutPflowLoose = 0;
   mu_isolation_FCTightTrackOnly_FixedRad = 0;
   el_isolation_FixedCutLoose = 0;
   el_isolation_FixedCutTight = 0;
   el_isolation_PflowTight = 0;
   el_isolation_TightTrackOnly = 0;
   el_isolation_TightTrackOnly_FixedRad = 0;
   el_isolation_FCHighPtCaloOnly=0;
   el_isElTight = 0;
   fwdel_isTight = 0;
   fwdel_isMedium = 0;
   fwdel_isLoose = 0;
   el_isElMedium = 0;
   el_isElLoose = 0;
   el_DFCommonElectronsECIDS = 0;
   el_DFCommonElectronsECIDSResult = 0;
   el_isPromptLepton = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF", &weight_globalLeptonTriggerSF, &b_weight_globalLeptonTriggerSF);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_Reco_UP", &weight_leptonSF_EL_SF_SIMPLIFIED_Reco_UP, &b_weight_leptonSF_EL_SF_SIMPLIFIED_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN", &weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN, &b_weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP", &weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP, &b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN", &weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN, &b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_Iso_UP", &weight_leptonSF_EL_SF_SIMPLIFIED_Iso_UP, &b_weight_leptonSF_EL_SF_SIMPLIFIED_Iso_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN", &weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN, &b_weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_UP", &weight_globalLeptonTriggerSF_EL_Trigger_UP, &b_weight_globalLeptonTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &weight_globalLeptonTriggerSF_EL_Trigger_DOWN, &b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID", &weight_indiv_SF_EL_ChargeID, &b_weight_indiv_SF_EL_ChargeID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weight_indiv_SF_EL_ChargeID_UP, &b_weight_indiv_SF_EL_ChargeID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weight_indiv_SF_EL_ChargeID_DOWN, &b_weight_indiv_SF_EL_ChargeID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID", &weight_indiv_SF_EL_ChargeMisID, &b_weight_indiv_SF_EL_ChargeMisID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weight_indiv_SF_EL_ChargeMisID_STAT_UP, &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weight_indiv_SF_EL_ChargeMisID_STAT_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weight_indiv_SF_EL_ChargeMisID_SYST_UP, &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weight_indiv_SF_EL_ChargeMisID_SYST_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("mumuSelection", &mumuSelection, &b_mumuSelection);
   fChain->SetBranchAddress("emu", &emu, &b_emu);
   fChain->SetBranchAddress("eeSelection", &eeSelection, &b_eeSelection);
   fChain->SetBranchAddress("eeSelection_withmuon", &eeSelection_withmuon, &b_eeSelection_withmuon);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_2mu14", &HLT_2mu14, &b_HLT_2mu14);
   fChain->SetBranchAddress("HLT_2e24_lhvloose_nod0", &HLT_2e24_lhvloose_nod0, &b_HLT_2e24_lhvloose_nod0);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_mu22_mu8noL1", &HLT_mu22_mu8noL1, &b_HLT_mu22_mu8noL1);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu22_mu8noL1", &mu_trigMatch_HLT_mu22_mu8noL1, &b_mu_trigMatch_HLT_mu22_mu8noL1);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_2e17_lhvloose_nod0", &HLT_2e17_lhvloose_nod0, &b_HLT_2e17_lhvloose_nod0);
   fChain->SetBranchAddress("HLT_2e12_lhloose_L12EM10VH", &HLT_2e12_lhloose_L12EM10VH, &b_HLT_2e12_lhloose_L12EM10VH);
   fChain->SetBranchAddress("weight_KFactor", &weight_KFactor, &b_weight_KFactor);
   fChain->SetBranchAddress("mu_isMedium", &mu_isMedium, &b_mu_isMedium);
   fChain->SetBranchAddress("mu_isHighPt", &mu_isHighPt, &b_mu_isHighPt);
   fChain->SetBranchAddress("mu_PtID", &mu_PtID, &b_mu_PtID);
   fChain->SetBranchAddress("mu_PtMS", &mu_PtMS, &b_mu_PtMS);
   fChain->SetBranchAddress("mu_isolation_FixedCutTight", &mu_isolation_FixedCutTight, &b_mu_isolation_FixedCutTight);
   fChain->SetBranchAddress("mu_isolation_FixedCutLoose", &mu_isolation_FixedCutLoose, &b_mu_isolation_FixedCutLoose);
   fChain->SetBranchAddress("mu_isolation_FixedCutPflowTight", &mu_isolation_FixedCutPflowTight, &b_mu_isolation_FixedCutPflowTight);
   fChain->SetBranchAddress("mu_isolation_FixedCutPflowLoose", &mu_isolation_FixedCutPflowLoose, &b_mu_isolation_FixedCutPflowLoose);
   fChain->SetBranchAddress("mu_isolation_FCTightTrackOnly_FixedRad", &mu_isolation_FCTightTrackOnly_FixedRad, &b_mu_isolation_FCTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("el_isolation_FixedCutLoose", &el_isolation_FixedCutLoose, &b_el_isolation_FixedCutLoose);
   fChain->SetBranchAddress("el_isolation_FixedCutTight", &el_isolation_FixedCutTight, &b_el_isolation_FixedCutTight);
   fChain->SetBranchAddress("el_isolation_PflowTight", &el_isolation_PflowTight, &b_el_isolation_PflowTight);
   fChain->SetBranchAddress("el_isolation_TightTrackOnly", &el_isolation_TightTrackOnly, &b_el_isolation_TightTrackOnly);
   fChain->SetBranchAddress("el_isolation_TightTrackOnly_FixedRad", &el_isolation_TightTrackOnly_FixedRad, &b_el_isolation_TightTrackOnly_FixedRad);
   fChain->SetBranchAddress("el_isolation_FCHighPtCaloOnly", &el_isolation_FCHighPtCaloOnly, &b_el_isolation_FCHighPtCaloOnly);
   fChain->SetBranchAddress("el_isElTight", &el_isElTight, &b_el_isElTight);
   fChain->SetBranchAddress("fwdel_isTight", &fwdel_isTight, &b_fwdel_isTight);
   fChain->SetBranchAddress("fwdel_isMedium", &fwdel_isMedium, &b_fwdel_isMedium);
   fChain->SetBranchAddress("fwdel_isLoose", &fwdel_isLoose, &b_fwdel_isLoose);
   fChain->SetBranchAddress("el_isElMedium", &el_isElMedium, &b_el_isElMedium);
   fChain->SetBranchAddress("el_isElLoose", &el_isElLoose, &b_el_isElLoose);
   fChain->SetBranchAddress("el_DFCommonElectronsECIDS", &el_DFCommonElectronsECIDS, &b_el_DFCommonElectronsECIDS);
   fChain->SetBranchAddress("el_DFCommonElectronsECIDSResult", &el_DFCommonElectronsECIDSResult, &b_el_DFCommonElectronsECIDSResult);
   fChain->SetBranchAddress("el_isPromptLepton", &el_isPromptLepton, &b_el_isPromptLepton);
   fChain->SetBranchAddress("Quark2_Px", &Quark2_Px, &b_Quark2_Px);
   fChain->SetBranchAddress("Quark2_Py", &Quark2_Py, &b_Quark2_Py);
   fChain->SetBranchAddress("Quark2_Pz", &Quark2_Pz, &b_Quark2_Pz);
   fChain->SetBranchAddress("Quark2_E", &Quark2_E, &b_Quark2_E);
   fChain->SetBranchAddress("Quark2_PDGID", &Quark2_PDGID, &b_Quark2_PDGID);
   fChain->SetBranchAddress("Quark1_Px", &Quark1_Px, &b_Quark1_Px);
   fChain->SetBranchAddress("Quark1_Py", &Quark1_Py, &b_Quark1_Py);
   fChain->SetBranchAddress("Quark1_Pz", &Quark1_Pz, &b_Quark1_Pz);
   fChain->SetBranchAddress("Quark1_E", &Quark1_E, &b_Quark1_E);
   fChain->SetBranchAddress("Quark1_PDGID", &Quark1_PDGID, &b_Quark1_PDGID);
   fChain->SetBranchAddress("Dilepton_Pt_born", &Dilepton_Pt_born, &b_Dilepton_Pt_born);
   fChain->SetBranchAddress("Dilepton_Rapidity_born", &Dilepton_Rapidity_born, &b_Dilepton_Rapidity_born);
   fChain->SetBranchAddress("Dilepton_Eta_born", &Dilepton_Eta_born, &b_Dilepton_Eta_born);
   fChain->SetBranchAddress("Dilepton_Phi_born", &Dilepton_Phi_born, &b_Dilepton_Phi_born);
   fChain->SetBranchAddress("Dilepton_Mass_born", &Dilepton_Mass_born, &b_Dilepton_Mass_born);
   fChain->SetBranchAddress("Dilepton_m_born", &Dilepton_m_born, &b_Dilepton_m_born);
   fChain->SetBranchAddress("Lepton_Pt_born", &Lepton_Pt_born, &b_Lepton_Pt_born);
   fChain->SetBranchAddress("AntiLepton_Pt_born", &AntiLepton_Pt_born, &b_AntiLepton_Pt_born);
   fChain->SetBranchAddress("Lepton_Eta_born", &Lepton_Eta_born, &b_Lepton_Eta_born);
   fChain->SetBranchAddress("AntiLepton_Eta_born", &AntiLepton_Eta_born, &b_AntiLepton_Eta_born);
   fChain->SetBranchAddress("Lepton_Phi_born", &Lepton_Phi_born, &b_Lepton_Phi_born);
   fChain->SetBranchAddress("AntiLepton_Phi_born", &AntiLepton_Phi_born, &b_AntiLepton_Phi_born);
   fChain->SetBranchAddress("Lepton_m_born", &Lepton_m_born, &b_Lepton_m_born);
   fChain->SetBranchAddress("AntiLepton_m_born", &AntiLepton_m_born, &b_AntiLepton_m_born);
   fChain->SetBranchAddress("Lepton_Pt_bare", &Lepton_Pt_bare, &b_Lepton_Pt_bare);
   fChain->SetBranchAddress("AntiLepton_Pt_bare", &AntiLepton_Pt_bare, &b_AntiLepton_Pt_bare);
   fChain->SetBranchAddress("Lepton_Eta_bare", &Lepton_Eta_bare, &b_Lepton_Eta_bare);
   fChain->SetBranchAddress("AntiLepton_Eta_bare", &AntiLepton_Eta_bare, &b_AntiLepton_Eta_bare);
   fChain->SetBranchAddress("Lepton_Phi_bare", &Lepton_Phi_bare, &b_Lepton_Phi_bare);
   fChain->SetBranchAddress("AntiLepton_Phi_bare", &AntiLepton_Phi_bare, &b_AntiLepton_Phi_bare);
   fChain->SetBranchAddress("Lepton_Pt_dressed", &Lepton_Pt_dressed, &b_Lepton_Pt_dressed);
   fChain->SetBranchAddress("Lepton_Eta_dressed", &Lepton_Eta_dressed, &b_Lepton_Eta_dressed);
   fChain->SetBranchAddress("Lepton_Phi_dressed", &Lepton_Phi_dressed, &b_Lepton_Phi_dressed);
   fChain->SetBranchAddress("Lepton_Charge_dressed", &Lepton_Charge_dressed, &b_Lepton_Charge_dressed);
   fChain->SetBranchAddress("AntiLepton_Pt_dressed", &AntiLepton_Pt_dressed, &b_AntiLepton_Pt_dressed);
   fChain->SetBranchAddress("AntiLepton_Eta_dressed", &AntiLepton_Eta_dressed, &b_AntiLepton_Eta_dressed);
   fChain->SetBranchAddress("AntiLepton_Phi_dressed", &AntiLepton_Phi_dressed, &b_AntiLepton_Phi_dressed);
   fChain->SetBranchAddress("AntiLepton_Charge_dressed", &AntiLepton_Charge_dressed, &b_AntiLepton_Charge_dressed);
   /*fChain->SetBranchAddress("elTruth_Pt_dressed", &elTruth_Pt_dressed, &b_elTruth_Pt_dressed);
   fChain->SetBranchAddress("elTruth_Eta_dressed", &elTruth_Eta_dressed, &b_elTruth_Eta_dressed);
   fChain->SetBranchAddress("elTruth_Phi_dressed", &elTruth_Phi_dressed, &b_elTruth_Phi_dressed);
   fChain->SetBranchAddress("elTruth_Charge", &elTruth_Charge, &b_elTruth_Charge);
   fChain->SetBranchAddress("muTruth_Pt_dressed", &muTruth_Pt_dressed, &b_muTruth_Pt_dressed);
   fChain->SetBranchAddress("muTruth_Eta_dressed", &muTruth_Eta_dressed, &b_muTruth_Eta_dressed);
   fChain->SetBranchAddress("muTruth_Phi_dressed", &muTruth_Phi_dressed, &b_muTruth_Phi_dressed);
   fChain->SetBranchAddress("muTruth_Charge", &muTruth_Charge, &b_muTruth_Charge);*/
   Notify();

   } // end of if SumWeights
   if( nameOfTree == "SumWeights" ){



   // Set object pointer
   generators = 0;
   AMITag = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("dsid", &dsid, &b_dsid);
   fChain->SetBranchAddress("isAFII", &isAFII, &b_isAFII);
   fChain->SetBranchAddress("generators", &generators, &b_generators);
   fChain->SetBranchAddress("AMITag", &AMITag, &b_AMITag);
   fChain->SetBranchAddress("totalEventsWeighted", &totalEventsWeighted, &b_totalEventsWeighted);
   fChain->SetBranchAddress("totalEvents", &totalEvents, &b_totalEvents);
   }//end of if Sumweights
   Notify();
} // end of function

Bool_t Comparison::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Comparison::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Comparison::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Comparison_cxx)
